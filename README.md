<!-- TOC -->
* [About](#about)
* [Quick Start](#quick-start)
* [Tech Stack](#tech-stack)
* [Scraped Files](#scraped-files)
* [Current Project Structure](#current-project-structure)
* [SQL Database](#sql-database)
* [Intro to the django database framework](#intro-to-the-django-database-framework)
* [WebScraping](#webscraping)
  * [Database Schema](#database-schema)
  * [Agent Caveats](#agent-caveats)
  * [Add new files to parse](#add-new-files-to-parse)
    * [About ScrapeTargetCollections and how to implement them](#about-scrapetargetcollections-and-how-to-implement-them)
    * [About ParserFactory and how to implement them](#about-parserfactory-and-how-to-implement-them)
    * [About Parser and how to implement them](#about-parser-and-how-to-implement-them)
      * [Parser Caveats](#parser-caveats)
    * [About Django Models and Loading them](#about-django-models-and-loading-them)
      * [Database Caveats](#database-caveats)
  * [Changing django models, adding new models, migrations](#changing-django-models-adding-new-models-migrations)
  * [Testing](#testing)
* [API](#api)
  * [Adding new endpoints](#adding-new-endpoints)
  * [Serialization](#serialization)
  * [Api Caveats](#api-caveats)
  * [Admin Page](#admin-page)
  * [Logs of Scraping](#logs-of-scraping)
  * [API documentation - Swagger](#api-documentation---swagger)
  * [API documentation - python examples](#api-documentation---python-examples)
  * [Running the server and deployment](#running-the-server-and-deployment)
    * [Running locally](#running-locally)
    * [Deployment to remote server](#deployment-to-remote-server)
    * [WSGI Server](#wsgi-server)
* [Frontend](#frontend)
  * [Angular and its Components](#angular-and-its-components)
  * [High level structure](#high-level-structure)
  * [General Tab](#general-tab)
  * [VLBI Sessions](#vlbi-sessions)
  * [Query Tabs - Earth Orientation Parameters, Celestial Reference Frame](#query-tabs---earth-orientation-parameters-celestial-reference-frame)
  * [Api Examples](#api-examples)
  * [Source, Station and Session Pages](#source-station-and-session-pages)
  * [Plotting in general, query-view -> chart-handler -> chart-wrapper](#plotting-in-general-query-view---chart-handler---chart-wrapper)
  * [Special Components and Services](#special-components-and-services)
    * [Short intro services](#short-intro-services-)
    * [Api Service](#api-service)
    * [Reusable table](#reusable-table)
    * [App Root: app.component](#app-root-appcomponent)
    * [Router Service](#router-service)
  * [On different types of URLs](#on-different-types-of-urls)
  * [Adding a new page](#adding-a-new-page)
    * [Displaying a standalone HTML file that is returned by the backend](#displaying-a-standalone-html-file-that-is-returned-by-the-backend)
    * [Displaying HTML content with Angular](#displaying-html-content-with-angular)
  * [Building the Angular Project](#building-the-angular-project)
* [Developer Flow for API and Frontend](#developer-flow-for-api-and-frontend)
  * [New endpoint](#new-endpoint)
  * [New Frontend Component Design](#new-frontend-component-design)
* [Troubleshooting](#troubleshooting)
  * [Import Errors In Python](#import-errors-in-python)
  * [Api URL not found](#api-url-not-found)
  * [Deployment Errors](#deployment-errors)
    * [Import fixes and database file](#import-fixes-and-database-file)
    * [Static files](#static-files)
  * [Finding Angular Components on UI](#finding-angular-components-on-ui)
<!-- TOC -->
# About
The glovdh database is an open database to make it easy for the research community to query, analyze and visualize data. 
It has three main components, the scraper, the api and the frontend. The scraper is run periodically, keeps track
of the files on the internet, parses updated files and keeps error states upon failure. The api is a RESTful
interface to the database that enables researchers to query the database to their own need.
The frontend is a web application that helps visualize and query data from the database.

# Quick Start

- Clone the repo
  ```
  git clone https://gitlab.ethz.ch/spacegeodesy_public/glovdh_ord.git
  cd glovdh_ord
  ```
- Create virtual environment with python 3.11
- Install dependencies
  ```
  pip install -r requirements.txt
  ```
- Create a file like [.env.example](.env.example) and setup environment variables 
  - the database credentials should point to a [MariaDB](https://mariadb.org/) (the test database is used for [unit tests](glovdh/webscraping/test))
  - the NETRC filepath should point to a .netrc file with credentials for scraping 
  (registration: https://urs.earthdata.nasa.gov/users/new, more details below)
  - the [DJANGO_SECRET_KEY](https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-SECRET_KEY) can be generated via
  
    ```python -c "from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())" ```
- Run the scraper script (first run is a couple of hours) to load the database
    ```
    cd .. #cd working dir as the parent folder of the repo
    python glovdh_ord/scrape.py
    ```
- Run the application (see deployment section for non-local servers)
  ```
  python glovdh_ord/manage.py runserver 
  ```




# Tech Stack
- Scraper: Python
- API: [Django](https://www.djangoproject.com/)
- Database: MariaDB
- Frontend: [Angular](https://v17.angular.io/docs) and PlotlyJs

# Scraped Files

- Master files, prodiving metadata about VLBI sessions
    - https://cddis.nasa.gov/archive/vlbi/ivscontrol/
- TRF files, providing data about terrestrial reference frame
    - https://cddis.nasa.gov/archive/vlbi/ivsproducts/trf/
- CRF files, providing data about celestial reference frame
     - https://cddis.nasa.gov/archive/vlbi/ivsproducts/crf/
- EOP files for Earth Orientation Parameters
    - https://cddis.nasa.gov/archive/vlbi/ivsproducts/eopi/
    - https://cddis.nasa.gov/archive/vlbi/ivsproducts/eops/
- SKD files for metadata about a session scans and observations
    - https://cddis.nasa.gov/archive/vlbi/ivsdata/aux/2024/aov092/
- Other files for getting list of stations and sources
    - [cddis.nasa.gov/archive/vlbi/gsfc/ancillary/solve_apriori/IVS_SrcNamesTable.txt](https://cddis.nasa.gov/archive/vlbi/ivsproducts/eopi/)
    - [raw.githubusercontent.com/nvi-inc/sked_catalogs/main/position.cat](https://cddis.nasa.gov/archive/vlbi/ivsproducts/eopi/)

# Current Project Structure

``` 
root
--public           #WORKING DIR for scripts!
----glovdh_ord     #cloned git project
--private
----.env  #configs and credentials
```
Project structure + Working directory
Currently the project consists of two directories, public/ and private/. 

The private folder stores the virtual environment and the .env file that lists database, django and scraping
secrets for the project. There is an [example .env file](.env.example) that describes what secrets are needed
for the code to work.
This .env file is read by django in  [.glovdh/settings.py](glovdh/settings.py)

The public folder contains the git repository.
An important thing is the working directory. **Currently everything is setup in a way that the working directory is the _public_ directory**. 
So for example starting the django server (details later) would be (after activating the python environment)
```
source venv/bin/activate
cd public
python glovdh_ord/manage.py runserver
```

Another important file is the .netrc file. This file is used for authentication for different websites.
Since the scraped files are behind a login page, for example https://cddis.nasa.gov/archive/vlbi/ivsproducts/eops/, 
an account has to be used for the scraping.
The [example .env file](.env.example) describes a NETRC environment variable that tells where
this file should be found. 
On linux systems the .netrc file is in the _~home_ directory and contain something like this:  
```machine urs.earthdata.nasa.gov login myusername password mypw```  
If this file is not found by the system or has wrong credentials, the parsing will fail.  
More on a netrc files: https://www.labkey.org/Documentation/wiki-page.view?name=netrc


# SQL Database
Since Django does a great job to hide the database technology, the MariaDB itself is rarely at focus.
It’s important to keep in mind however what SQL technology is under django, because its API can sometimes change.
An example is the [bulk_create(..)](https://docs.djangoproject.com/en/5.1/ref/models/querysets/). 
The linked documentation of the function says that the ignore_conflicts parameter can change behavior
with underlying different SQL databases. 

# Intro to the django database framework
_(Django provides tools to create websites and RESTful APIS, for now lets stay closer to the database aspects.)_

Django is an object relational mapping (ORM) framework, that hides the underlying SQL database technology.
The models are defined in the [glovdh/models.py](glovdh/models.py) file.
These classes/Models declaratively define database tables, so in this readme models and tables will be used
interchangeably. Fields in models are columns in the SQL database, and using the python API of the model
makes it possible to query the underlying SQL tables. An example would be the following query:
```
EOPSolution.objects.filter(analyst_center_id=”asi”)
```
which selects every object(=SQL row/record) that has the analyst_center_id field set to “asi”.
The Models have to be populated with data, and thats the responsibility of the scraper script - to load the database
through django.


# WebScraping
To run:
```
python glovdh_ord/manage.py migrate
python glovdh_ord/scrape.py
```
(the first command is to create the database tables, the second is to run the scraper.)

The webscraping part of the project is responsible for keeping the database up-to-date.
[scrape.py](scrape.py) is designed to be run periodically, for example with a cron job.
The main object of the webscraping component is the ScrapedFile object (Django class).
These objects describe parsed files from the internet, keep track of their errors, last parsed dates and first
parsed dates. When the scraper is run, 
it checks for outdated files, files with errors and new files to download and parse.
The actor of the script is the WebParserAgent class.
Scraping steps:
1. initialize a parser agent. The parser agent needs 
   1. FileArchiveTarget - An abstract collection that tells which files to scrape
   2. SourceNameTableParserFactory - A factory that creates a parser for the given files
   3. Django models that are loaded with the parsed data
    ```
    WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='EOPIFileArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=EOPI_FILE_REGEX,
                minimum_last_file_update=PARSE_EOP_MINIMUM_DATE,
                archive_url=INTENSIVE_SESSION_URL_TEMPLATE.format(filename=''),
                file_url_template=INTENSIVE_SESSION_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, EOPI_FILE_REGEX)
            ),
            parser_factory=EopParserFactory(),
            django_models=[AnalystCenter, EOPSolution],
            db_handler=db,
            downloader=downloader
        )
    ```
   Each agent is responsible to load a single set of models. This agent above is designed to parse the EOPI files 
from the internet, The FileArchive is given, next to other things, the base url of the files on the web, a regex pattern to identify files.
The Agent also gets a factory. A factory is meant to create parsers for the files in question. The parsers, given by the factory,
are expected to match the django models given to the agent. Here the EopParserFactory is expected to instantiate parsers
that take the EOPI files and return two pandas dataframes. One for the AnalystCenter model and one for the EOPSolution model.
2. run the agent. The current algorithm can be described with the following steps:
   1. The agent asks the file archive for files to scrape.
   2. The file archive checks the web url for the file updates, queries the database for files with errors and outdated files
   based on the file updates it saw on the web. It gives back a list of file descriptors to scrape.
   3. The agent downloads the files that it received, creates a parser with the factory for each file
   4. The parser scans the file and gives back the dataframes
   5. The agent takes the dataframes and its Django models and loads the records to the database

Scraping is quite strict. If there was _any_ error with the file, the whole file is discarded and not loaded to the database
in any form. If there is a line in an eopi file that has a typing error that is not expected, the error is saved in
the database with the ScrapedFile and the parsing will be continued with the other files. It can be reparsed or retried
later or manually. (details later)

## Database Schema
The current schema can be viewed in [glovdh/models.py](glovdh/models.py)
where each model is a table with its fields, alternatively here is an svg image
about the exported schema visualization
![img](readme_images/db_schema.svg)


## Agent Caveats
- The django_models argument, that describes which models should be loaded have to match dataframe order that
the parsers give back. That is, if your parser gives back two dataframes, one about stations and one about Sessions
the django_model argument of the Agent has to match that, like \[Station, Session\]. Moreover, the columns of the dataframes
of the parsers have to match the field names of the models. 

## Add new files to parse
The architecture is quite modular, so addings new files consists of implementing small interfaces and giving
them to an agent.
To add new files to parse, we need a new ScrapeTargetCollection, a new ParserFactory with Parsers
and a new, perhaps modified Django model.

### About ScrapeTargetCollections and how to implement them
The ScrapeTargetCollection is an abstract class in 
[glovdh/webscraping/scraping_collections.py](glovdh/webscraping/scraping_collections.py) that has to be implemented
with a single function interface:
```
def get_target_files(self) -> List[FileMetadata]:
```
This function is responsible to give back a FileMetadata list that describes what URL the agent should check
and what was the last modified date of the file. Once this function is implemented it can be given to an agent.

### About ParserFactory and how to implement them
The ParserFactory is an abstract class in [glovdh/webscraping/parsers/common.py](glovdh/webscraping/parsers/common.py)
that is responsible for the creation of Parsers. This is important because for example different EOPI files can have
different formats/versions that this class is responsible which parser to use to interpret its content.
It has two functions
```
@abc.abstractmethod
def get_file_parser(self, file_url: str, filecontent: str) -> 'Parser':
```
is responsible to give back a parser after having seen the file_url and filecontent.

```
@abc.abstractmethod
def get_default_parser(self) -> 'Parser':
```
This function is used to get a default parser. This is usually used when there was an error
processing a file, but we need a parser to give back an empty dataframe with the correct columns
for mostly consistency reasons. If these two functions are implemented, the agent can use it to create parsers.

### About Parser and how to implement them

Parsers are used to parse the content of the files. Every file version has a parser that gives back a set of dataframes.
```
@abc.abstractmethod
def to_dataframes(self, filecontent: str) -> Iterable[pd.DataFrame]:
```
This function is fairly straightforward, it takes the filecontent and returns a set of dataframes.

```
    @abc.abstractmethod
    def get_empty(self) -> Iterable[pd.DataFrame]:
```
It also has the above mentioned get_empty function that needs to give back an empty dataframe with the correct columns.
#### Parser Caveats
 - The columns of the dataframe have to match the Django model fields. That is, the column names
are essentially matching the underlying SQL table column names.
 - The get_empty needs to return the same amount of dataframes in the same order with the same columns as to_dataframes

Once these functions are implemented, and the Factory gives back these parsers to the agent, the
agent is able to parse a new kind of file into dataframes.


### About Django Models and Loading them
Once the parsers give back the correct dataframes, they can be loaded via an Agent.
All thats needed is that the dataframe columns match the model fields. 

The DBHandler in [glovdh/webscraping/db_handler.py](glovdh/webscraping/db_handler.py) is responsible for
communicating with the database. Since if you have a foreign key in a django model, that will also be a django model
so these models have to be added before loading. This is done in the _add_foreign_keys_ of the DBHandler.
If you have a new class that is used as a foreign key in a model, you have to extend this function.

#### Database Caveats
- There is only a single way when a file is erroneous and still partially loaded to the database.
There are files that reference stations that are not in the database, since the stations are currently loaded
from https://raw.githubusercontent.com/nvi-inc/sked_catalogs/main/position.cat. If a station, in a TRF file for example,
is not present in this list, and hence in the database, the record is discarded but the loading will be continued with
the other records of the file. This whitelist can be edited under [glovdh/webscraping/consts.py](glovdh/webscraping/consts.py)
- Before running anything you have to run the initial migrations to create the database. As a usual django project,
  this is the [glovdh/migrations/0001_initial.py](glovdh/migrations/0001_initial.py) file. This file creates the tables
and indices. In this project there is a second file that will also run with the first
    ```
    python glovdh_ord/manage.py migrate
    ```
    command. This is  the
    [glovdh/migrations/0002_add_initial_session_types.py](glovdh/migrations/0002_add_initial_session_types.py) file that
    is responsible for seeing the SessionType model with initial values before the first scraping run.

## Changing django models, adding new models, migrations

Django allows for easy database changes through _manage.py_ . This django script is the tool mostly used to communicate
with the framework via command line.
Now that the new ScrapeTargetCollections, factories, parsers are all in place, the models are the last step for 
a successful loading. Any edit in models.py will be picked up by django. If you want to add a field a new model, just
add a new class in models.py that inherits from django.db.models.Model. After this, run
```
python glovdh_ord/manage.py makemigrations
```
this will detect changes and create a migrations file in [glodh/migrations](glovdh/migrations). After this run
```
python glovdh_ord/manage.py migrate
```
This will make and commit the changes in the database.
If you just want to modify a model, like adding a new column/field, edit the class and run the above commands.

Of course this is true for all the above paragraphs, if a new column has to be added in a parsing process, just add it
to the resulting dataframe, change the model and run the migrations.

> The makemigrations command creates a descriptive django migration file 
> under [glovdh/migrations](glovdh/migrations). Make sure these files are committed and up-to-date, so 
> if you have to migrate the database on the remote server, django will find these files. Since these
> files describe what the migration exactly is, django looks for these files if you run the migrate command
> and executes them. Committing these to the repository, allows the remote server that this application is hosted
> on to stay up-to-date with the database migrations. (You just have to run the migrate command on remote,
> which is detailed later in the deployment section).

## Testing
There are fairly extensive tests that focus on validating the code for webscraping part of the project,
the parsers and the database loading.
They can be run with
```
python glovdh_ord/manage.py test
```

# API
This component is responsible for providing a RESTful interface to the database. The API is built with Django.
It uses the above-mentioned django models to query the database and transform the data into JSON objects as HTTP 
responses.
To run:
```
python glovdh_ord/manage.py runserver
```

The api consists of simple endpoints that give back repsonses for a HTTP request.
Following django standards, the endpoints are defined in [glovdh/urls.py](glovdh/urls.py) and the views are defined under 
[glovdh/views](glovdh/views).

## Adding new endpoints
To add a new endpoint you have to implement a class inheriting from the rest_framework.views.APIView class
and register it in [glovdh/urls.py](glovdh/urls.py).

```
class ProgramsView(APIView):
    def get(self, request, *args, **kwargs):
        programs = VLBISession.objects.values_list('program', flat=True).distinct()
        serializer = StringListSerializer({"data": programs})
        return Response(serializer.data, status=status.HTTP_200_OK)
```
This is a simple endpoint that answers to a get request. It queries the VLBISession records and gives back a serialized
version of the data.
If you take a look at [glovdh/urls.py](glovdh/urls.py), its registered under _api/v1/list-programs_, so
if you send a GET request to https://glovdh.ethz.ch/api/v1/list-programs you will get back the programs in the database.
something like this.
You can also use to check the validity of the parameters, see this later under 'API Documentation'
```
{
    "data": [
        "CRL-KSP",
        "VLBA-GEOD",
        "VLBA-TEST",
        "IVS-INT-1",
        ...
    ]
}
```
## Serialization

Serializing
The return value has to be json serialized before passing it to a Response object. 
For this reason, I defined several serializers for the responses in [glovdh/serializers.py](glovdh/serializers.py). 
These serializers also help the self documentation of the API (details later).
Mostly, standard serializers are used that would be expected in other django applications, except for one.

One special Serializer is the QuerySerializer class that I implemented.
This serializer is used for larger queries. Since json is quite inefficient in space, because it always 
repeats the keys of an object even if they are the same 
```
{
    "data": [
        {"mykey1": 2, "mykey2": "cat"},
        {"mykey1": 3, "mykey2": "dog"},
        ...
    ]
}
```
I created this class which breaks down a serialized objects to rows and columns, so the result would be:
```
{
    "columns": ["mykey1", "mykey2"],
    "rows": [[2, "cat"], [3, "dog"]]
}
```
which is much more efficient for large queries. Usage:

```
serializer = QuerySerializer(
            child_serializer=EopSolutionSerializer(all_fields=True, file_is_eoxy=True),
        )
```
If you would use only the EopSolutionSerializer, you would end up with the repeating keys-like object mentioned above.
If this EopSolutionSerializer is passed to the QuerySerializer, it will break down the serialized object to rows and columns.
After this you just have to return the json form:
```
data = serializer.to_representation(query)
return Response(data, status=status.HTTP_200_OK)
```


## Api Caveats

 - API limitations
Currently there is an imposed hard-limit on all response sizes of queries endpoints, which is defined
in [glovdh/views/consts.py](glovdh/views/consts.py), that is set to 10_000.  
This 10_000 means that at maximum 10_000 records will be serialized for one query.
Currently I have not met a query where this size is reached,
and this limit can also be got around with the ‘offset’ request parameter to queries, 
that will offset the query results and for example return the #10_000-#20_000 record range if 
the offset is 10_000 in the query parameters. 
In the above file the date_formats are also defined, which describe what format is accepted as a query parameter for a date field.
 - Units: QuerySerializer gives back columns and rows of a query. Since units are also of interest, a third key
is included in the response, the units. This is a dictionary that describes the units of the columns. This dictionary 
can be found in [glovdh/views/consts.py](glovdh/views/consts.py) . If a column name is added here, and is returned
in a query, its unit will automatically be included in the response. 


## Admin Page

> For this you need to [create a superuser](https://docs.djangoproject.com/en/4.2/ref/django-admin/#createsuperuser).
> You can [change the password of the superuser](https://docs.djangoproject.com/en/4.2/ref/django-admin/#changepassword) anytime

Django helps to generate an admin page that helps to manage the database.
The admin page is under https://glovdh.ethz.ch/admin/ and can be accessed with the superuser credentials.
You can edit, delete and add records to the database with this interface, but I added some custom
functionality to help manage scraped files. 
First, every model on the admin page is augmented with some custom filtering, defined in 
[glovdh/admin.py](glovdh/admin.py). This makes it possible to search and filter for filenames, but error states
as well on the right:  

![readme_images/filters.png](readme_images/filters.png)  

Secondly there is a custom functioned defined, also in the above file, to reparse files. Simply select files and 
press the launch parsers action.

![readme_images/admin_page.png](readme_images/admin_page.png)


## Logs of Scraping
Every scraper run has a log associated with it. It details numbers about the parsing process and gives
a brief overview about the run.
They can be found under https://glovdh.ethz.ch/admin/logs


## API documentation - [Swagger](https://swagger.io/)
There is an OpenAPI standard documentation for the API. Its under https://glovdh.ethz.ch/api/v1/docs .
This documentation is generated by the [drf-spectacular](https://drf-spectacular.readthedocs.io/en/latest/) package.
This package mostly pick up the endpoints from urls.py and the defined views in the project. To augment this
I usually add decorators to endpoints like this
```
class ProgramsView(APIView):
    @extend_schema(
        request=None,
        responses={
            200: StringListSerializer
        },
        description="List all programs",
        tags=['Sessions']
    )
    def get(self, request, *args, **kwargs):
        ...
```
This tells which Serializer is used for the response and what endpoint does. 
If you would like to add Parameter validation, that also helps the schema generation.
For example this is the CRF Query endpoint
```
class QueryCrfView(APIView):
    class ValidationForm(forms.Form):
        analyst_center = forms.CharField(required=False, help_text='three letter code, like "asi"')
        filename = forms.CharField(required=False, help_text='filename like asi2023a.crf.gz')
        offset = forms.IntegerField(required=False, help_text='Query size is limited.'
                                                              ' You can offset the results to get the next chunk of data')
        all_fields = forms.BooleanField(required=False, help_text='whether to return all fields or just limited ones.'
                                                                  'Currently only returing source_file as an extra field.')

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: QuerySerializer,
            400: ErrorSerializer
        },
        description="Query CRF solutions from database",
        tags=['Query', 'Sources']
    )
    def get(self, request):

        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({
                'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query = ...
```
Here a validation mechanism is used as well for query verification. I defined a ValidationForm
that is used to validate the request parameters. If the form is not valid then it will return a 400 error.
Then this validation form is registered in the decorator to it gets picked up into the documentation.
The resulting https://glovdh.ethz.ch/api/v1/docs#/Sources/query_crf_retrieve documentation describes everything
that this endpoint does and what parameters it has.

NOTE: The API documentation page is mainly for documentation and is not meant to serve large queries. For
that, use python scripts or the frontend.

## API documentation - python examples
To give examples how the API can be used via scripts, there are small examples under
[https://glovdh.ethz.ch/examples](https://glovdh.ethz.ch/examples).
(The examples are stored on the backend and can be edited, extended under [api_examples](api_examples))

## Running the server and deployment
### Running locally
The server can be run locally with
```
python glovdh_ord/manage.py runserver
```
This will open run the django API and endpoints can be tested locally.

### Deployment to remote server
Before deployment, we have to make sure all files are in-place for django to serve.
The project can have many static files that have to be served, like the output of the Angular
build command (html, css, js files), static django files, the admin page and maybe some
custom html files that have to be shown to users.
[glovdh/settings.py](glovdh/settings.py) defines a STATICFILES_DIRS constant,
that tells django where it should collect static files from. Django will have to put all static files
under STATIC_ROOT so when a request comes in it will find them in a central place . 
Running this command will tell django to do just that.
```
glovh_ord/manage.py collectstatic –clear
```
Since the Angular frontend produces files that have to be served from a server
(see frontend section), we have to get them to the remove server. A simple way is to 
build and commit the angular project locally but this is error-prone and not recommended.
A better way is to build the frontend code during deployment so its always up-to-date.
To do this, you have to set up a node environment on the remote machine as well, and run
the ng build command from there. (You need to [install npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) to the remove
machine as well in this case)


>I also created a [deployment bash script](deploy.sh) that does everything automatically.
>It activates the virtual environment, sets up the corrects working directory, runs tests, 
>updates the django migrations, builds the angular project, collects the static files 
> and restarts the server.
> 
> To run it, just simply use
> ```
> bash glovdh_ord/deploy.sh
>```
> **Beware that if the django tests fail, currently the script does not exit and will continue deployment!**

After running this, the remote server should be up-to-date with changes and can be run like a 
WSGI server.


### WSGI Server
We also use a similar script like this where the requests are redirected to this django application.
```
import django
import os
import sys
from django.core.wsgi import get_wsgi_application
from pathlib import Path

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'glovdh_ord.glovdh.settings')
import pymysql
import traceback
pymysql.install_as_MySQLdb()
try:
       application = get_wsgi_application()
except Exception as e:
       print(e)
       traceback.print_exc()

```
(Check the trusted hosts in [settings.py](glovdh/settings.py) if 
untrusted hosts issues)


# Frontend
The glovdh frontend is meant to support visualization and querying.
[After installing the Node Package Manager (npm)](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
the frontend can be run locally with
```
cd glovdh_ord
cd glovdh-fe
npm install
ng serve
```
This will run a local frontend version (on default port 4200). Angular supports hot-reload
so if you change a file in, the angular server on 4200 will reflect your changes.

## Angular and its Components
Angular is a highly modular framework, that builds on components.
Each component has 3 main parts
- an .html file for page structure
- a .ts file for behaviour
- a .css file for styling

The components can be nested, and put together like lego pieces to build a website.
You can take a look at existing components under [glovdh-fe/src/app](glovdh-fe/src/app).
The main component is described by the app.component.html (in the same folder)
The frontend consists of several subpages, in this section I will go through
which components are used where and what their responsibilities are.
Additionally, there is another frequently used object in Angular which are services,
that enable components to communicate between eachother. 
The used services will also be detailed.

## High level structure
If you take a look at [glovdh-fe/src/app/app.component.html](glovdh-fe/src/app/app.component.html)
there is 3 main parts of the webpage.
The image above and the buttons on it,
the tab navigation and the changing content under the navigation:
![readme_images/high_level_structure.png](readme_images/high_level_structure.png)
The content that changes is always handled by the 
[router module](glovdh-fe/src/app/app-routing.module.ts).
This works with matching different paths on the frontend. For a given pattern/url 
it will show the corresponding component under the tab navigation.
The coming sections are about the different components, that are showed under the tab navigation
since the header image and the navigation itself are fixed on every page.

## [General Tab](https://glovdh.ethz.ch/general)
The general tab is for a brief overview of the data in the database.
Its code can be viewed under 
[glovdh-fe/src/app/general-tab](glovdh-fe/src/app/general-tab)
It mostly shows plots and tables about data that it queries from the API.
The API calls, plotting and the shown table deserve a separate section, but this page
is a good example of how we can nest components in Angular - 
the general component is a component itself that is nested in the main page,
and there are subcomponents like the table that is used as its children.

## [VLBI Sessions](https://glovdh.ethz.ch/sessions)
This page is meant to serve queries regarding session metadata.
![readme_images/session_query_page.png](readme_images/session_query_page.png)
The buttons/textbox values are bound to typescript fields, so when the 
```runQuery()``` (in [session-query.component.ts](glovdh-fe/src/app/session-query/session-query.component.ts)) 
function is called, it reads all the fields and send a request to the API for data, that is then visualized in a table.
The runQuery() function is called for every click on the RunQuery button that is declared
in the component's html page. We can also see that the <app-resuable-table> component
in the html file gets the `[data]="this.queryResult!.rows"` argument.
the queryResult field is updated when the API returns with the response, and since
angular properties are bound together, upon the field's value update, the table will
refresh itself to show the newly arrived data.

## Query Tabs - [Earth Orientation Parameters](https://glovdh.ethz.ch/eop), [Celestial Reference Frame](https://glovdh.ethz.ch/crf)
The EOP and CRF pages share a lot of similarities. They also have the query textboxes as the 
Session query page but the processing of queries are different.
These queries are handled
by a [query-view component](glovdh-fe/src/app/query-view). This component is responsible
for storing queries, and showing charts and tables about them.
![readme_images/query_pages.png](readme_images/query_pages.png)
On the image above, there is the list of queries stored by the query-view component. 
The blue box is all the query-view. As the green text states, we can switch between chart and table
view. The table is handled by a reusable-table component, and the charts are handled by
a chart-handler component. The chart-handler stores all the charts that are added to it
and shows them in a chart-wrapper component.

## [Api Examples](https://glovdh.ethz.ch/examples)
The API examples are stored in
[api_examples](api_examples). The earlier mentioned API endpoint gives back the examples that 
the frontend has to visualize.
The structure of the page
![readme_images/examples_page.png](readme_images/examples_page.png)
consists of two columns, the sidebar and the content. 
The content is built with the help of code-highlight components.
There is an additional service in 
[glovdh-fe/src/app/api-examples-content.service.ts](glovdh-fe/src/app/api-examples-content.service.ts)
that queries the API for the examples. The api-examples-content and api-examples-sidenav
components communicate with this service, and they both get the data about the examples,
they just visualize them in different ways. (More on special components and services later)


## [Source](https://glovdh.ethz.ch/source/1849%2B670), [Station](https://glovdh.ethz.ch/station/Br) and [Session](https://glovdh.ethz.ch/session/r1720) Pages
There are three additional pages that help summarize data for sources, stations and sessions.
They can be reached via the host/{source | station | session}/{id} urls
and their content is in source-view, session-view and station-view components.
The structures are very similar, all of them filter for data about the objects, and plot
some metrics about them.
![readme_images/source_view.png](readme_images/source_view.png)
There are two columns in every page, on the right columns there are html 
divs defined for plots. 
For example in 
[glovdh-fe/src/app/source-view/source-view.component.html](glovdh-fe/src/app/source-view/source-view.component.html)
the right column is defined as
```
  <div class="right-column">
    <div [hidden]="this.sessions.length == 0" class="diagram" id="sessionTypeChart"></div>
    <div [hidden]="this.sessions.length == 0" class="diagram" id="activityChart"></div>
    <div [hidden]="this.sessions.length == 0" class="diagram" id="obsBarPlot"></div>
    <div [hidden]="this.sessions.length == 0" class="diagram" id="scansBarPlot"></div>
    <div [hidden]="this.sessions.length == 0" class="diagram" id="crfPosChart"></div>
    <div [hidden]="this.sessions.length == 0" class="diagram" id="crfErrChart"></div>
  </div>
```
where each div is used to show a chart. More on plotting later but
if charts have to be modified or added, these div declarations are a good place to start.

## Plotting in general, query-view -> chart-handler -> chart-wrapper
Plotting is done with the javascript version of plotly: https://plotly.com/javascript/
The documentation is very good, and its fairly simple to use for basic plots.
For example the 
```
  plotSessionsPerYear(sessions: Session[]){
    let results = this.countProgramsPerYear(sessions);
    let aggregatedCounts = results.sessionCounts;


    let traces = this.getStackBarPlotTraces(aggregatedCounts);

    var layout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Sessions',
    };
    Plotly.newPlot('activityChart', traces, layout);
  }
```
function in [glovdh-fe/src/app/source-view/source-view.component.ts](glovdh-fe/src/app/source-view/source-view.component.ts)
 is responsible for plotting this barplot on the source-view pages:
![readme_images/source_barplots.png](readme_images/source_barplot.png)
The main outline of every plotting mechanism is to create traces (=data), define a layout
and then ask Plotly to put it in the declared div of the html part of the component.
This is the same thing that happens in the 
query-view and chart-handler components mentioned in the EOP/CRF query pages.
In the query-view:
```
  addQuery(query: Query) {
    ...
    if (this.page == AppConstants.EOP) {
      ...
    }
    if (this.page == AppConstants.CRF) {
      this.addMollweideProjection();
      this.addHistPlotMAS();
    }
    this.showCharts();
  }
```
depending on which page we are on, we add different plots to the chart-handler component
```
addHistPlotMAS(){
    ... //data setup
    this.chartsHandler.addHistPlotMAS(
      asc_uncertainty,
      dec_uncertainty,
      'Uncertaininty - ' + this.selectedQueryToPlot.title,
      'Uncertaininty - ' + this.selectedQueryToPlot.title,
    );
}
```
After this the chart-handler component adds the chart layout and data to its
registered charts:
```
addHistPlotMAS(
    asc_uncertainty: number[],
    declination_uncertainty: number[],
    title: string,
    chartName: string
  ) {
    
    const data = [
      ...
    ];

    const layout = {
      ...
    };

    const chart = {
      data: data,
      layout: layout,
      title: title,
      id: chartName
    };
    this.registeredCharts[chartName] = chart;
```
which are then plotted with a chart-wrapper in
[glovdh-fe/src/app/chart-handler/chart-handler.component.html](glovdh-fe/src/app/chart-handler/chart-handler.component.html)
```
<ng-container *ngFor="let keyvalue of registeredCharts | keyvalue">
    <div class="chart_container">
      <app-chart-wrapper
        [data]="keyvalue.value.data"
        [title]="keyvalue.value.title"
        [layout]="keyvalue.value.layout"
      >
      </app-chart-wrapper>
      <button
        mat-flat-button
        color="accent"
        id="removeChart"
        (click)="removeChart(keyvalue.key)"
      >
      Delete Chart
      </button>
    </div>
  </ng-container>
```


## Special Components and Services
There are some special components and services that are worth mentioning because
they often have to be modified if we want to add new functionailty to the app.

### Short intro services 
As mentioned before services are used to enable components to communicate with
eachother. Services dont have their own folder like components, because
they only consist of one file, like
[glovdh-fe/src/app/api-examples-content.service.ts](glovdh-fe/src/app/api-examples-content.service.ts)

### Api Service
The [api-service](glovdh-fe/src/app/api-service.service.ts) is the
main abstraction for database queries. Each function here is a call to the API
that returns an async response for the query. If new endpoints are added and
the UI uses them, its best if you add a function here for querying.
Then in the components, the angular dependency injection mechanism is used
so that the components have a reference to this service. In the component
constructor we have to add the service:
```
  constructor(
    private apiService: ApiServiceService,
  ) {}
```
This constructor is seen many times in the project, since a lot of components
communicate with the API. 

### Reusable table
I created a single component to show a tabular format of the data.
This component is
[glovdh-fe/src/app/resuable-table](glovdh-fe/src/app/resuable-table).
This component is also many times used in the project in a lot of components like:
```
<app-resuable-table
  *ngIf="showQueryResult && !runningQuery"
  [columnNames]="this.queryResult!.columns"
  [data]="this.queryResult!.rows"
  [name]="currentQueryName"
  [columnUnits]="this.queryResult?.units"
  [pageSizes]="[100, 200, 500]"
></app-resuable-table>
```

This creates a table with the given data, rows and units.
[table](glovdh-fe/src/app/resuable-table/resuable-table.component.ts) has 
automatic functionalities like units display, csv export and also adjusts
the names of the columns to a more human-friendly format. If we want to
change how a column name is displayed on the UI, the 
```
  public viewColumnNames: any = {...}
```
field of the reusable-table component will be useful - these translations are used
to show the user the columns and also these will be the exported .csv columns.
An additional feature of this component is that is automatically adds links
to certain pages, like sessions and stations if they are referenced in a column.
These links are created in the [html file of the table](glovdh-fe/src/app/resuable-table/resuable-table.component.html)
and can be extended for new use-cases

### App Root: app.component
The root component of the Angular project is
[glovdh-fe/src/app/app.component.html](glovdh-fe/src/app/app.component.html)
This component defined the headerImage-TabNavigation-PageComponent structure
that is used throughout the project. If the main layout has to be changed,
this component should be edited.

### Router Service
The routing module, referenced on the bottom of the root app.component is responsible for 
showing the pages that are navigated to. If there is a new page where a new component
should be shown, the 
[glovdh-fe/src/app/app-routing.module.ts](glovdh-fe/src/app/app-routing.module.ts)
file should be the starting point of the changes.


## On different types of URLs

It is important to distinguish two types of routes that are used in the project. 

The first group is a one that is handled by the frontend.
If I visit https://glovdh.ethz.ch/examples page, it will send a request to the backend. 
The backed tries to match it with a route defined in
[glovdh/urls.py](glovdh/urls.py)
Django goes through the list and returns the first view that matches the url. 
The “https://glovdh.ethz.ch/examples” path will match only the last one (default, catches everything)
This returns the starting page, index.html file that was generated by the angular deployment.
This index.html file references other javascript, css etc files that are then also requested by the frontend 
When all the frontend files (images, css, html, js) are returned, the javascript code will match the /examples 
route to one of the angular routes defined in 
[glovdh-fe/src/app/app-routing.module.ts](glovdh-fe/src/app/app-routing.module.ts)
which will then show the correct component according to the path.

The other type are pages that are directly served from the backend. These pages are the docs and admin pages 
- https://glovdh.ethz.ch/api/v1/docs 
- https://glovdh.ethz.ch/admin

When I request these urls, the urls.py will match it with the /admin and the 
/docs route

These views return a complete html immediately. With this the Angular 
javascript files are not loaded and the returned html is shown (the admin page and the docs page are both pre-generated 
html files by django and drf-spectacular respectively.
This opens up two ways to integrate generated HTML files in the project

## Adding a new page
### Displaying a standalone HTML file that is returned by the backend
There are static files in a django project that are served for a request. 
These files include images, the files (html, js, css etc) files generated by angular,
or the admin page files that are generated by Django.
[glovdh/settings.py](glovdh/settings.py) define where the djando project should look for static
files if there is a request for one. If we have static files, we should add it to the STATICFILES_DIRS
list in the file above. Then, running
```
glovh_ord/manage.py collectstatic –clear
```
will tell django to look in every directory in STATICFILES_DIRS and collect the static files to a centralized location
under STATIC_ROOT.
After this if there is a request for _glovdh.ethz.ch/myfile.html_ django will try to find it under STATIC_ROOT and return it,
making the file viewable for the user
>Note: The output of the angular project is also a bundle of css, js and html files. If we take a look at 
> STATICFILE_DIRS we can see that the output of the angular deployment's folder is also there. This tells django
> to collect these files to serve as well. So when somebody asks for index.html, it will return it. And then index.html
> references javascript files and css files that are also returned for subsequent requests. The above solution only differs 
> in that the prepared HTML file is a standalone file, but in theory it can reference other files (css, js) as well like angular does
> we just have to make sure those files are also under the STATIC_ROOT of django.

### Displaying HTML content with Angular
If there is an HTML file, but it should be embedded to a component next to other content or under a button, it is fairly easy to accomplish.
Lets say we are looking to modify mycomponent.html. The html then should contain something like
```
<div [innerHTML]="htmlContent"></div>
```
While the mycomponent.ts file should contain a public field with the content

```public htmlContent = "<p>Example</p>".```

The field’s value can also be set with an http call’s response of course, for example making a javascript call 
thorught the api service:
```
this.htmlContent = this.apiService.getMyHTML()
```
using the html content of the response to set the value of htmlContent field in this example. 
This will render the received html content within the component.

## Building the Angular Project
If any frontend changes are made and ready to be deployed, run 
```
ng build
```
_from the [glovdh-fe](glovdh-fe) folder._
This will compress and pack the angular files into files that are ready to be served on the server.
The output of the command can be found under 
[glovdh-fe/dist/glovdh-fe/browser](glovdh-fe/dist/glovdh-fe/browser).
You can run this command on the local machine, commit the changes to the repository and then
run the deployment script on the remote server to update the frontend. This is error-prone
because you have to make sure the committed files are up-to-date and only one version is committed 
to the repository (plus the angular build output is usually ignored by git so you have to add
them manually). A better way for deployment is building the frontend code on the remote server,
see deployment section for more info.

# Developer Flow for API and Frontend
## New endpoint
Implementing new endpoints is quite fast with django's Object-Oriented
endpoint classes and ORM interface. Once a new endpoint is implemented
testing locally is a useful step. Using a free, thin http client like
[postman](https://www.postman.com)
or [bruno](https://www.usebruno.com) can speed up
development a lot. It allows for sending requests with custom payloads and
headers to quickly test out the newly implemented endpoint and catch bugs.
## New Frontend Component Design
Angular supports hot-reload which enables interactive component design.
If the Angular project is run with [ng serve](https://v17.angular.io/cli/serve)
Angular will have a development server locally (usually on port 4200 by default)
that interactively refreshes and responds to any local sourcefile changes.
The result of changing and saving typescript, css or html files will instantly 
be seen on the port in the browser to move fast with design.
If you are testing locally, it can be useful to change the API_ENDPOINT
constant in [consts.py](glovdh-fe/src/app/consts.ts) to localhost:{django_port}
so if you are developing the django API together with the frontend, the frontend will send
all requests to the local API that is being changed.
> Make sure that the above-mentioned API_ENDPOINT const is set to 'https://glovdh.ethz.ch/' when you run
> ng build. If this is forgotten and left as localhost, the deployed code will not work on the remote
> server (it will not find the django localhost server on that computer).

# Troubleshooting
## Import Errors In Python
As things are currently set-up in the python part of the repository, 
especially the webscraping module, every import has to be an absolute
import originating from **glovdh_ord.**.. . For example, 
```
from glovdh_ord.glovdh.models import EOPSolution
```
will succeed however 

```
from glovdh.models import EOPSolution
```
will fail. This is because the glovdh folder is not added to the PYTHONPATH
environment variable. To use these shorter imports, one would have to
add a line to every script that is run to add the new import roots to
the PYTHONPATH or change the working directory of the script
## Api URL not found
If API urls are not found, one of the followings could be the cause:
- The new url is not added to [urls.py](glovdh/urls.py) in the django application
- The url ends with a slash: 'myhost/myendpoint/' instead of 'myhost/myendpoint' 
and this causes the urls to not match
## Deployment Errors
### Import fixes and database file
If the deployed version results in an internal server error,
most it could potentially mean that
- an import was added to the new version and is not an absolute import (see above)
- django could not read the database secrets in [settingy.py](glovdh/settings.py)
- In the frontend code, the api service is pointing to a localhost url that was changed to 
work with Angular locally
and not the production url https://glovdh.ethz.ch
### Static files
If the deployment seemingly succeeds however the new version is not visible,
it could mean that the static files were not properly updated. Check the
[static](static) directory. The folder should contain only one version of the files
main-xxx.js, polyfills-xxx.js and styles-xxx.css. Additionally, these files should be
referenced in [index.html](static/index.html). 
If these look correct, also double-check the 
[angular build output folder](glovdh-fe/dist/glovdh-fe/browser). The static files
there should have been all copied to the [static](static) folder with
```
python glovdh_ord/manage.py collectstatic --clear
```
So these files should also be in-sync with
the contents of the [static](static) folder

## Finding Angular Components on UI
Since angular components are generated and organized in a flat folder under
[glovdh-fe/src/app](glovdh-fe/src/app) it can be hard to identify which components
is where at first. While I tried keeping an intuitive and consistent naming-scheme, 
the component html layouts can help look for nested components. An easy way to find
a component (if the used IDE does not support this with ctrl+click or something)
is simply searching for the component's name with ctrl+f 
throughout the angular project potentially filtering for html files. 
It will quickly show where the component is nested. Beware the
components' name and the name they are referenced by in a nested html.
For example, the [api-examples-content](glovdh-fe/src/app/api-examples-content)
component is nested with a prefix as 
```
<app-api-examples-content></app-api-examples-content>
```
in [the html of api-examples](glovdh-fe/src/app/api-examples/api-examples.component.html)
component.
