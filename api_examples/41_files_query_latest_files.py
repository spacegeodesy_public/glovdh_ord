import pandas as pd
import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-files'
params = {
    'limit': 10,
    'sort_by': '-parsed_date'
}
resp = requests.get(ENDPOINT)
resp.raise_for_status()
data = resp.json()
print(pd.DataFrame(data['rows'], columns=data['columns']).head(2))
#                   parsed_date         last_modified error     file_url
#0  2024-07-12T22:00:42.662248Z  2024-07-04T15:35:03Z  None    https://cddis.nasa...
#1  2024-07-12T22:01:15.174473Z  2024-02-14T16:05:08Z  None    https://cddis.nasa...
