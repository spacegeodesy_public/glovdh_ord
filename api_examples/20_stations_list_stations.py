import pandas as pd
import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/list-stations'
resp = requests.get(ENDPOINT)
resp.raise_for_status()
data = resp.json()
print(pd.DataFrame(
    data['rows'],
    columns=data['columns']).head(2)
      )
#  code      name  longitude  latitude   source_file
#0   13     DSS13     243.21     35.25  position.cat
#1   14  GOLDMARS     243.11     35.43  position.cat
