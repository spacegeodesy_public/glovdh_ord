import pandas as pd
import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/source'
source = '0420-014'
resp = requests.get(f'{ENDPOINT}/{source}')
resp.raise_for_status()
data = resp.json()
print(data['info'])  # information about the source names
print(pd.DataFrame(data['sessions']['rows'],
                   columns=data['sessions']['columns']).head(2)
      )
#  session_id       program  ... session_type         source_file
#0     vo4199      VGOS-OPS  ...      24-hour      master2024.txt
#1     s22423  VGOS-24INT-S  ...      24-hour      master2024.txt
print(pd.DataFrame(data['position']['rows'],
                   columns=data['position']['columns']).head(2)
      )
#     source         RA        DEC    sig_RA   sig_DEC      source_file
#0  0357-263  59.890341 -25.741297  0.000008  0.000255  asi2021a.crf.gz
#1  0356+322  59.937137  32.346432  0.000003  0.000067  asi2021a.crf.gz
