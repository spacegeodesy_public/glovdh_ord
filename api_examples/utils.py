import dataclasses
from typing import Callable

@dataclasses.dataclass(frozen=True)
class ApiExample:
    chapter: str
    title: str
    description: str
    code_filename: str

