import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/list-programs'
resp = requests.get(ENDPOINT)
resp.raise_for_status()
data = resp.json()
print(data['data'])
# ['IVS-INT-2', 'AOV', ...]
