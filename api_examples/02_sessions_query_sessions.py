import requests
import pandas as pd

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-sessions'
query_params = {
    'min_date': '2022-01-01',
    'max_date': '2023-01-01',
    'programs': ['VGOS-INT-A', 'IVS-INT-00'],
    'type': 'intensive'
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
intensive_sessions_2022 = pd.DataFrame(data['rows'], columns=data['columns'])
print(intensive_sessions_2022.head(2))
#   session_id     program  ... session_type         source_file
#0      ii2363  IVS-INT-00  ...    intensive  master2022-int.txt
#1      ii2355  IVS-INT-00  ...    intensive  master2022-int.txt
