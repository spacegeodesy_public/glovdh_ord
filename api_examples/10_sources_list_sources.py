import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/list-sources'
resp = requests.get(ENDPOINT)
resp.raise_for_status()
data = resp.json()
print(data['data'])
# ['1600+43B', 'HD146361', ...]
