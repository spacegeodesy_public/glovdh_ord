import requests
import pandas as pd

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-eop'
query_params = {
    'filename': 'asi2023a',
    'all_fields': True
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
asi2023a_solutions = pd.DataFrame(data['rows'], columns=data['columns'])
print(asi2023a_solutions.head(2))
#                        epoch       XPO  ...  analyst_center       source_file
# 0  2019-09-17T05:59:32.179200Z  0.207091  ...             asi  asi2023a.eoxy.gz
# 1  2019-11-15T05:59:32.179200Z  0.151576  ...             asi  asi2023a.eoxy.gz
