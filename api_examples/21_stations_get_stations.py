import pandas as pd
import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/station'
station = 'Wz'
resp = requests.get(f'{ENDPOINT}/{station}')
resp.raise_for_status()
data = resp.json()
print(data['info'])  # information about the source names
print(pd.DataFrame(data['sessions']['rows'],
                   columns=data['sessions']['columns']).head(2)
      )
#  session_id    program  ... session_type         source_file
# 0     i24365  IVS-INT-1  ...    intensive  master2024-int.txt
# 1     q24365  IVS-INT-3  ...    intensive  master2024-int.txt
print(pd.DataFrame(data['position']['rows'],
                   columns=data['position']['columns']).head(2)
      )
#  station             ref_epoch  ... analyst_center      source_file
# 0      Wz  2015-01-01T00:00:28Z  ...            bkg  bkg2023b.trf.gz
# 1      Wz  2010-01-01T00:00:00Z  ...            ivs  ivs2023b.trf.gz
