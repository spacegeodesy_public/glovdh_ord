import requests
import pandas as pd
from matplotlib import pyplot as plt

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/station'
station = 'Wz'
resp = requests.get(f'{ENDPOINT}/{station}')
resp.raise_for_status()
data = resp.json()

df = pd.DataFrame(data['sessions']['rows'], columns=data['sessions']['columns'])
df['year'] = pd.to_datetime(df.time_start).dt.year
after2020 = df[df.year >= 2020]

pivot = (after2020.groupby(['year', 'program'])
         .agg({'session_id': 'count'})
         .reset_index().rename(columns={'session_id': 'count'})
         .pivot_table(index='year', columns='program', values='count'))

pivot.plot(kind='bar', stacked=True)
plt.ylabel('Session Count')
plt.title(f'Sessions for Station {station}')
plt.legend(bbox_to_anchor=(1.0, 1.0))
plt.tight_layout()
plt.show()
# -> Stacked barplot for sessions just like on https://glovdh.ethz.ch/station/Wz

