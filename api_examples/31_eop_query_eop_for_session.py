import requests
import pandas as pd

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-eop'
query_params = {
    'session_id': 'r1623',
    'all_fields': True
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
r1623_solutions = pd.DataFrame(data['rows'], columns=data['columns'])
print(r1623_solutions.head(2))
#                         epoch       XPO  ...  analyst_center       source_file
# 0  2014-02-04T05:15:12.960000Z  0.023710  ...             ivs  ivs2023a.eoxy.gz
# 1  2014-02-04T05:07:12.576000Z  0.023789  ...             ivs  ivs22q4e.eops.gz
