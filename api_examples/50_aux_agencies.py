import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/list-agencies'
resp = requests.get(ENDPOINT)
resp.raise_for_status()
data = resp.json()
print(data['data'])
# ['asi', 'bkg', 'cgs', ...]
