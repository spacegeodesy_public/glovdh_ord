import requests
import pandas as pd

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-crf'
query_params = {
    'filename': 'asi2022a.crf.gz',
    'all_fields': True
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
asi2022a_solutions = pd.DataFrame(data['rows'], columns=data['columns'])
print(asi2022a_solutions.head(2))
#     source        RA        DEC  ...  num_rates  analyst_center      source_file
# 0  0000+212  0.830625  21.495697  ...          0             asi  asi2022a.crf.gz
# 1  0000-160  0.863601 -14.215151  ...          0             asi  asi2022a.crf.gz

query_params = {
    'analyst_center': 'asi',
    'all_fields': False,
    'offset': 10000
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
all_asi_solutions_chunk2 = pd.DataFrame(data['rows'], columns=data['columns'])
print(all_asi_solutions_chunk2.head(2))
#     source         RA        DEC    sig_RA   sig_DEC      source_file
# 0  0357-263  59.890341 -25.741297  0.000008  0.000255  asi2021a.crf.gz
# 1  0356+322  59.937137  32.346432  0.000003  0.000067  asi2021a.crf.gz
