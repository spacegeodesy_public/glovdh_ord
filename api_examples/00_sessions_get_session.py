import requests

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/session'
session_id = 'vo4143'
url = f'{ENDPOINT}/{session_id}'
resp = requests.get(url)
resp.raise_for_status()
data = resp.json()
print(data.keys())
# ['session', 'baselines', 'sources', 'stations']
