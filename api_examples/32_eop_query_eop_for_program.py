import requests
import pandas as pd

ENDPOINT = 'https://glovdh.ethz.ch/api/v1/query-eop'
query_params = {
    'programs': ["IVS-INT-2", "AOV"],
    'min_date': '2023-02-04',
    'max_date': '2024-02-04',
}
resp = requests.get(ENDPOINT, params=query_params)
resp.raise_for_status()
data = resp.json()
last_year_solutions = pd.DataFrame(data['rows'], columns=data['columns'])
print(last_year_solutions.head(2))
#                         epoch       XPO  ...  sig_dEPS  session_id
# 0  2023-02-15T05:29:36.009600Z -0.027579  ...       NaN      aov080
# 1  2023-02-15T05:29:32.208000Z -0.027692  ...       NaN      aov080