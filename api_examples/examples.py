from glovdh_ord.api_examples.utils import ApiExample

EXAMPLES = [
    ApiExample(
        chapter='Sessions',
        title='Session Information',
        description='Getting a session information is easy. The returned objects'
                    ' contain information about the session, its baselines, observed '
                    'sources and participating stations.',
        code_filename='00_sessions_get_session.py'
    ),
    ApiExample(
        chapter='Sessions',
        title='Get VLBI programs',
        description='Get a list of all VLBI programs in the database.',
        code_filename='01_sessions_get_programs.py'
    ),
    ApiExample(
        chapter='Sessions',
        title='Query Sessions',
        description='Query based on programs, session type and time is easy with the API.'
                    ' In general, all query-... endpoints return rows of data under the "rows" key'
                    ' and the column names under the "columns" key. This makes it easy to convert'
                    ' the response to a pandas DataFrame for example.',
        code_filename='02_sessions_query_sessions.py'
    ),
    ApiExample(
            chapter='Sources',
            title='List All Sources',
            description='Get a list of Sources in the database with the API.'
                        'The list returns the IVS-Names since it is used as a '
                        'primary key in the database.',
            code_filename='10_sources_list_sources.py'
    ),
    ApiExample(
        chapter='Sources',
        title='Get information about a source by name',
        description='You can query with IVS-Name, IERS-Name, '
                    'J2000-Name-Short or J2000-Name-Long names as well. '
                    'Much like the station information endpoint, you will get back 3 keys, '
                    'describing the basic information, the associated sessions and the '
                    'latest CRF estimates',
        code_filename='11_sources_get_source.py'
    ),
    ApiExample(
        chapter='Sources',
        title='Query CRF Solutions',
        description='You can query CRF solutions from a specific file'
                    'or from a specific analyst center. All query endpoint'
                    'obey a 10.000 record limit. If your query would return more '
                    'records, you can use the offset parameter to get the next chunk '
                    '(offset=10000 for the next 10.000 records).',
        code_filename='12_sources_query_crf.py'
    ),
    ApiExample(
        chapter='Stations',
        title='List Stations',
        description='To get the location, name and code of all stations in a dataframe'
                    ' you can use the list-stations endpoint. Similar to queries, it returns '
                    'a "rows" and "columns" key in the response.',
        code_filename='20_stations_list_stations.py'
    ),
    ApiExample(
        chapter='Stations',
        title='Get information about a station by code',
        description='Similar to the /source endpoint, you can query general information'
                    ', location and associated session information about a station.',
        code_filename='21_stations_get_stations.py'
    ),
    ApiExample(
        chapter='Earth Orientation Parameters (EOP)',
        title='Query EOP solutions from a file',
        description='You can query EOP solutions from a specific file using its name such as "asi2023a".'
                    ' The database stores the filename as a primary key, and this'
                    ' query will try to match it to one using a "filename.startswith(param)" lookup.'
                    ' Because of this convenience logic, the recommended way to query is always '
                    ' with the file\'s full name, including its extension, in this case as "asi2023a.eoxy.gz".'
                    ' To get a list of what files are in the database, you can use the /query-files endpoints.',
        code_filename='30_eop_query_eop_for_file.py'
    ),
    ApiExample(
        chapter='Earth Orientation Parameters (EOP)',
        title='Query EOP solutions for a specific session',
        description='You can supply a session_id parameter to filter only for'
                    ' solutions for a given session.',
        code_filename='31_eop_query_eop_for_session.py'
    ),
    ApiExample(
        chapter='Earth Orientation Parameters (EOP)',
        title='Query EOP solutions for a program from a time interval',
        description='You can filter for programs and time intervals as well.',
        code_filename='32_eop_query_eop_for_program.py'
    ),
    ApiExample(
        chapter='Files',
        title='Check a file\'s status',
        description='You can check for a file in the database using its filename.',
        code_filename='40_files_query_file_status.py'
    ),
    ApiExample(
        chapter='Files',
        title='Check for latest files',
        description='You can query the latest files like so. '
                    'Use a - prefix for descending order.',
        code_filename='41_files_query_latest_files.py'
    ),
    ApiExample(
        chapter='Auxiliary',
        title='Check analyst centers',
        description='Get a list of all analyst centers in the database.',
        code_filename='50_aux_agencies.py'
    ),
    ApiExample(
        chapter='Example Plots',
        title='Plot Yearly Sessions for a Station or Source',
        description='Plotting the number of sessions per program for every year is easy if '
                    'everything is put into a dataframe. Because of the API symmetry with sources, '
                    'getting the same plot for a given source is the same '
                    'as the code below just with the /source/{source_id} endpoint',
        code_filename='60_plots_station_sessions.py'
    ),
    ApiExample(
        chapter='Example Plots',
        title='Plot Yearly Scans or Observations for a Station or Source',
        description='We have to go a bit further if We want to get the same stacked barplot but for scans and '
                    'observations. The code below shows how to get the number of scans for a station per year, but '
                    'it can be done similarly for observations, and for a given source as well. For sources, just'
                    ' use the /source and /source-stats endpoints.',
        code_filename='61_plots_station_scans_obs.py'
    )
]