import requests
import pandas as pd
from matplotlib import pyplot as plt

station = 'Wz'

STATION_ENDPOINT = 'https://glovdh.ethz.ch/api/v1/station'
resp = requests.get(f'{STATION_ENDPOINT}/{station}')
resp.raise_for_status()
station_data = resp.json()

STATS_ENDPOINT = 'https://glovdh.ethz.ch/api/v1/station-stats'
resp = requests.get(f'{STATS_ENDPOINT}/{station}')
resp.raise_for_status()
stats_data = resp.json()

stats_df = pd.DataFrame(stats_data['rows'], columns=stats_data['columns'])
sessions_df = pd.DataFrame(station_data['sessions']['rows'],
                           columns=station_data['sessions']['columns'])

df = pd.merge(stats_df, sessions_df, on='session_id')
df['year'] = pd.to_datetime(df.time_start).dt.year
after2020 = df[df.year >= 2020]

pivot = (after2020.groupby(['year', 'program'])
         .agg({'scans': 'sum'})  # or 'observations' instead of scans!
         .pivot_table(index='year', columns='program', values='scans'))

pivot.plot(kind='bar', stacked=True)
plt.ylabel('Number Of Scans')
plt.title(f'Scans for Station {station}')
plt.legend(bbox_to_anchor=(1.0, 1.0))
plt.tight_layout()
plt.show()
# -> Stacked barplot for scans just like on https://glovdh.ethz.ch/station/Wz
