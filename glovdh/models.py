"""
Objects that are stored in the GLOVDH database as SQL tables.
Under some models UniqueConstraints are used to ensure data integrity.
Adding database indices under certain models here, can be used to speed up queries.
"""

from django.db import models
from django.db.models import UniqueConstraint


class ScrapedFile(models.Model):
    filename = models.CharField(max_length=32, primary_key=True)
    file_url = models.CharField(max_length=255)
    parsed_date = models.DateTimeField()
    last_modified = models.DateTimeField()
    error = models.CharField(max_length=255, null=True)


class AnalystCenter(models.Model):
    code = models.CharField(max_length=3, primary_key=True)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)


class Source(models.Model):
    ivs_name = models.CharField(max_length=16, primary_key=True)
    j2000_name_long = models.CharField(max_length=16, null=True)
    j2000_name_short = models.CharField(max_length=32, null=True)
    iers_name = models.CharField(max_length=32, null=True)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)


class Station(models.Model):
    code = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=16)
    longitude = models.FloatField()
    latitude = models.FloatField()
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)


class SessionType(models.Model):
    name = models.CharField(max_length=9, primary_key=True)


class VLBISession(models.Model):
    session_id = models.CharField(max_length=16, primary_key=True)
    program = models.CharField(max_length=16, null=True)
    time_start = models.DateTimeField(null=True)
    time_end = models.DateTimeField(null=True)
    stations = models.CharField(max_length=512, null=True)
    dbc = models.CharField(max_length=6, null=True)
    operation_center = models.CharField(max_length=8, null=True)
    corr = models.CharField(max_length=8, null=True)
    subm = models.CharField(max_length=8, null=True)
    session_type = models.ForeignKey(SessionType, on_delete=models.CASCADE)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)


CRF_SOLUTION_UNIQUE_FIELDS = ['source_file', 'source']
class CRFSolution(models.Model):
    source = models.ForeignKey(Source, null=True, on_delete=models.CASCADE)
    RA = models.FloatField(null=True)
    DEC = models.FloatField(null=True)
    sig_RA = models.FloatField(null=True)
    sig_DEC = models.FloatField(null=True)
    correlation = models.FloatField(null=True)
    w_mean_mjd = models.DateTimeField(null=True)
    first_mjd = models.DateTimeField(null=True)
    last_mjd = models.DateTimeField(null=True)
    num_sessions = models.IntegerField(null=True)
    num_delays = models.IntegerField(null=True)
    num_rates = models.IntegerField(null=True)
    analyst_center = models.ForeignKey(AnalystCenter, on_delete=models.CASCADE)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=CRF_SOLUTION_UNIQUE_FIELDS,
                             name='unique_constr_crf_solution')
        ]

TRF_SOLUTION_UNIQUE_FIELDS = ['source_file', 'station']
class TRFSolution(models.Model):
    station = models.ForeignKey(Station, on_delete=models.CASCADE, null=True)
    ref_epoch = models.DateTimeField(null=True)
    epoch_start = models.DateTimeField(null=True)
    epoch_end = models.DateTimeField(null=True)
    solution_version = models.CharField(max_length=255, null=True)
    estimate_stax = models.FloatField(null=True)
    estimate_stay = models.FloatField(null=True)
    estimate_staz = models.FloatField(null=True)
    estimate_velx = models.FloatField(null=True)
    estimate_vely = models.FloatField(null=True)
    estimate_velz = models.FloatField(null=True)
    error_stax = models.FloatField(null=True)
    error_stay = models.FloatField(null=True)
    error_staz = models.FloatField(null=True)
    error_velx = models.FloatField(null=True)
    error_vely = models.FloatField(null=True)
    error_velz = models.FloatField(null=True)
    analyst_center = models.ForeignKey(AnalystCenter, on_delete=models.CASCADE)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=TRF_SOLUTION_UNIQUE_FIELDS,
                             name='unique_constr_trf_solution')
        ]


EOP_SOLUTION_UNIQUE_FIELDS = ['source_file', 'session_id']
class EOPSolution(models.Model):
    epoch = models.DateTimeField()
    XPO = models.FloatField(null=True)
    YPO = models.FloatField(null=True)
    UT1UTC = models.FloatField(null=True)
    dPSI = models.FloatField(null=True)
    dX = models.FloatField(null=True)
    dEPS = models.FloatField(null=True)
    dY = models.FloatField(null=True)
    sig_XPO = models.FloatField(null=True)
    sig_YPO = models.FloatField(null=True)
    sig_UT1UTC = models.FloatField(null=True)
    sig_dPSI = models.FloatField(null=True)
    sig_dX = models.FloatField(null=True)
    sig_dEPS = models.FloatField(null=True)
    sig_dY = models.FloatField(null=True)
    wrms = models.FloatField(null=True)
    cor_XPO_YPO = models.FloatField(null=True)
    cor_XPO_UT1UTC = models.FloatField(null=True)
    cor_YPO_UT1UTC = models.FloatField(null=True)
    cor_dPDE = models.FloatField(null=True)
    nobs = models.IntegerField(null=True)
    session_id = models.ForeignKey(VLBISession, on_delete=models.CASCADE, null=True)
    XPO_r = models.FloatField(null=True)
    YPO_r = models.FloatField(null=True)
    lod = models.FloatField(null=True)
    dPSI_r = models.FloatField(null=True)
    dY_r = models.FloatField(null=True)
    dEPS_r = models.FloatField(null=True)
    dX_r = models.FloatField(null=True)
    sig_XPO_r = models.FloatField(null=True)
    sig_YPO_r = models.FloatField(null=True)
    sig_lod = models.FloatField(null=True)
    sig_dPSI_r = models.FloatField(null=True)
    sig_dX_r = models.FloatField(null=True)
    sig_dEPS_r = models.FloatField(null=True)
    sig_dY_r = models.FloatField(null=True)
    network = models.CharField(max_length=512, null=True)
    analyst_center = models.ForeignKey(AnalystCenter, on_delete=models.CASCADE)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=EOP_SOLUTION_UNIQUE_FIELDS,
                             name='unique_constr_eop_solution')
        ]

SOURCE_STATISTICS_UNIQUE_FIELDS = ['source_file', 'session_id', 'source']
class SourceStatistic(models.Model):
    session_id = models.ForeignKey(VLBISession, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    scans = models.IntegerField(default=0)
    observations = models.IntegerField(default=0)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)
    class Meta:
        constraints = [
            UniqueConstraint(fields=SOURCE_STATISTICS_UNIQUE_FIELDS,
                             name='unique_constr_source_stats')
        ]

STATION_STATISTICS_UNIQUE_FIELDS = ['source_file', 'session_id', 'station']
class StationStatistic(models.Model):
    session_id = models.ForeignKey(VLBISession, on_delete=models.CASCADE)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    scans = models.IntegerField(default=0)
    observations = models.IntegerField(default=0)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=STATION_STATISTICS_UNIQUE_FIELDS,
                             name='unique_constr_station_stats')
        ]


BASELINE_STATISTICS_UNIQUE_FIELDS = ['source_file', 'session_id', 'station1', 'station2']

class BaselineStatistic(models.Model):
    session_id = models.ForeignKey(VLBISession, on_delete=models.CASCADE)
    station1 = models.ForeignKey(Station, on_delete=models.CASCADE, related_name='statistics_station1')
    station2 = models.ForeignKey(Station, on_delete=models.CASCADE, related_name='statistics_station2')
    observations = models.IntegerField(default=0)
    source_file = models.ForeignKey(ScrapedFile, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=BASELINE_STATISTICS_UNIQUE_FIELDS,
                             name='unique_constr_baselines')
        ]