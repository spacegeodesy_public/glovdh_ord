from django.apps import AppConfig


class glovdhConfig(AppConfig):
    name = 'glovdh_ord.glovdh'
    verbose_name = 'Glovdh Project'
