"""
This file is used to describe added functionalities of the admin page in django.
Every model has a dedicated page that can be extended with filters, query fields.
Extra actions can also be added, like ScrapedFileAdmin.reparse that is responsible
for the reparse action for a given ScrapedFile (initiating full reparse of a given file)
"""
import datetime
import subprocess
import sys


from django.contrib import admin
from django.contrib import messages
from django.utils.html import format_html
from django.utils.translation import ngettext

from glovdh_ord.glovdh.models import *
from django.utils.translation import gettext_lazy as _

SCRAPE_SKD_SCRIPT_PATH = 'glovdh_ord/scrape.py'

class ErrorNotNullFilter(admin.SimpleListFilter):
    title = _('Has Error')
    parameter_name = 'has_error'

    def lookups(self, request, model_admin):
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.exclude(error__isnull=True)
        if self.value() == 'no':
            return queryset.filter(error__isnull=True)
        return queryset

@admin.register(ScrapedFile)
class ScrapedFileAdmin(admin.ModelAdmin):
    list_filter = (ErrorNotNullFilter, 'error')
    list_display = ('file_url', 'parsed_date', 'last_modified',)
    search_fields = ('file_url', 'error')
    ordering = ('-parsed_date',)

    actions = ['reparse']

    @admin.action(description='Launch parsers to update files')
    def reparse(self, request, queryset):
        current_time = datetime.datetime.now()
        run_id = 'manual_' + current_time.strftime('%Y%m%d_%H%M%S')
        run_id_args = ['--run_id', run_id]

        urls = list(queryset.values_list('file_url', flat=True))
        urls_args = ['--urls', *urls]

        cmd = [sys.executable, SCRAPE_SKD_SCRIPT_PATH] + urls_args + run_id_args
        subprocess.Popen(cmd)

        self.message_user(request, format_html(
            ngettext(
                f"Parsing of %d url started with run_id: <a href='/admin/logs/{run_id}.log'>{run_id}.log</a>.",
                f"Parsing of %d urls started with run_id: <a href='/admin/logs/{run_id}.log'>{run_id}.log</a>.",
                len(urls),
            ) % len(urls)), messages.INFO)


@admin.register(AnalystCenter)
class AnalystCenterAdmin(admin.ModelAdmin):
    list_display = ('code', 'source_file')
    search_fields = ('code', )


@admin.register(BaselineStatistic)
class BaselineStatisticAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'source_file')


@admin.register(SourceStatistic)
class SourceStatisticAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'source', 'source_file')


@admin.register(StationStatistic)
class StationStatisticAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'station', 'source_file')


@admin.register(TRFSolution)
class TRFSolutionAdmin(admin.ModelAdmin):
    list_display = ('station', 'source_file')

@admin.register(CRFSolution)
class CRFSolutionAdmin(admin.ModelAdmin):
    list_display = ('source', 'source_file')


@admin.register(EOPSolution)
class EOPSolutionAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'analyst_center', 'source_file')


@admin.register(VLBISession)
class VLBISessionAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'program', 'time_start', 'source_file')
    search_fields = ('session_id',)
    ordering = ('-time_start',)


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display = ('ivs_name', 'j2000_name_long', 'j2000_name_short', 'iers_name', 'source_file')
    search_fields = ('ivs_name', 'j2000_name_long', 'j2000_name_short', 'iers_name')


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'source_file')
    search_fields = ('code', 'name',)
