"""
Serializers that are used for serializing django QuerySets to text format for http responses.
(currently all queries are serialized to json).
"""

from typing import List

from django.db.models import QuerySet
from rest_framework import serializers

from glovdh_ord.glovdh.models import CRFSolution, EOPSolution, ScrapedFile, BaselineStatistic, SourceStatistic, \
    StationStatistic, VLBISession, TRFSolution, Station, Source
from glovdh_ord.glovdh.views.consts import UNITS


class StringListSerializer(serializers.Serializer):
    """
    Serializer that takes a queryset of a string list.
    and serializes it to a json object, where 'data' key stores the string
    list of the query.
    """
    data = serializers.ListField(
        child=serializers.CharField()
    )


class EopQueryOptionsSerializer(serializers.Serializer):
    programs = serializers.ListField(
        child=serializers.CharField()
    )
    source_files = serializers.ListField(
        child=serializers.CharField()
    )


class FreeSearchSerializer(serializers.Serializer):
    stations = serializers.ListField(
        child=serializers.CharField()
    )
    sources = serializers.ListField(
        child=serializers.CharField()
    )
    sessions = serializers.ListField(
        child=serializers.CharField()
    )
    analystCenters = serializers.ListField(
        child=serializers.CharField()
    )
    programmes = serializers.ListField(
        child=serializers.CharField()
    )


class ErrorSerializer(serializers.Serializer):
    errors = serializers.ListField(
        child=serializers.CharField()
    )


class CRFSolutionSerializer(serializers.ModelSerializer):
    """
    Responsible for serializing a CRF solution. If its created with a django
    query set that consists of CRF objects, it will create a json representation of the objects.
    self.Meta.fields describe which fields to include in the resulting json objects
    """
    class Meta:
        model = CRFSolution

    def __init__(self, *args, **kwargs):
        """
        if there is an all_fields=True key-word argument,
        it will include more fields in the resulting json
        """

        all_fields = kwargs.pop('all_fields', False)

        super().__init__(*args, **kwargs)
        if all_fields:
            self.Meta.fields = [
                'source',
                'RA',
                'DEC',
                'sig_RA',
                'sig_DEC',
                'correlation',
                'w_mean_mjd',
                'first_mjd',
                'last_mjd',
                'num_sessions',
                'num_delays',
                'num_rates',
                'analyst_center',
                'source_file',
            ]
        else:
            self.Meta.fields = ['source', 'RA', 'DEC',
                                'sig_RA', 'sig_DEC', 'source_file']


class EopSolutionSerializer(serializers.ModelSerializer):
    """
    Responsible for serializing a EOP solution. If its created with a django
    query set that consists of EOP objects, it will create a json representation of the objects.
    self.Meta.fields describe which fields to include in the resulting json objects
    """

    class Meta:
        model = EOPSolution

    def __init__(self, *args, **kwargs):
        """
        if there is an all_fields=True key-word argument,
        it will include more fields in the resulting json

        file_is_eoxy argument controls whether the file is eoxy or not.
        This modifies the serialized fields of the object (dY vs dPSI for example)
        """

        all_fields = kwargs.pop('all_fields', False)
        eoxy_file = kwargs.pop('file_is_eoxy', False)
        super().__init__(*args, **kwargs)
        if all_fields:
            self.Meta.fields = \
                [
                    'epoch',
                    'XPO',
                    'YPO',
                    'UT1UTC',
                    'dPSI',
                    'dX',
                    'dEPS',
                    'dY',
                    'sig_XPO',
                    'sig_YPO',
                    'sig_UT1UTC',
                    'sig_dPSI',
                    'sig_dX',
                    'sig_dEPS',
                    'sig_dY',
                    'wrms',
                    'cor_XPO_YPO',
                    'cor_XPO_UT1UTC',
                    'cor_YPO_UT1UTC',
                    'cor_dPDE',
                    'nobs',
                    'session_id',
                    'XPO_r',
                    'YPO_r',
                    'lod',
                    'dPSI_r',
                    'dY_r',
                    'dEPS_r',
                    'dX_r',
                    'sig_XPO_r',
                    'sig_YPO_r',
                    'sig_lod',
                    'sig_dPSI_r',
                    'sig_dX_r',
                    'sig_dEPS_r',
                    'sig_dY_r',
                    'network',
                    'analyst_center',
                    'source_file'
                ]
        else:
            base_fields = ['XPO', 'YPO', 'UT1UTC']
            eoxy_fields = ['dX', 'dY'] if eoxy_file else ['dEPS', 'dPSI']
            all_cols = base_fields + eoxy_fields
            cols_w_conf = ['session_id', 'epoch'] + sum([[col, 'sig_' + col] for col in all_cols], [])
            self.Meta.fields = cols_w_conf


class PrimitiveTypeField(serializers.Field):
    """
    Serializer used to serialize primitive types.
    (probably reinventing the wheel but could not find an implementation)
    """

    ALLOWED_PRIMITIVES = (str, int, float, None, bool)

    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        if any(isinstance(data, f) for f in self.ALLOWED_PRIMITIVES):
            return data
        self.fail('invalid_type', input_type=type(data).__name__)


class DummySerializer(serializers.Serializer):
    pass


class QuerySerializer(serializers.Serializer):
    """
    If there is a query set that should be serialized, normally We would get a result like
    crf_solutions = [
        {mykey1: myvalue1, mykey2: mavalue2},
        {mykey1: myvalue3, mykey2: mavalue4},
        {mykey1: myvalue5, mykey2: mavalue6}
        ]

    which is quite redundant in space especially if the query set is large.

    This serializer takes a queryset and serializes them into a more efficient representation.
    crf_solutions = {
            columns: [mykey1, mykey2],
            values: [[myvalue1, myvalue2], [myvalue3, myvalue4], [myvalue5, myvalue6]],
        }

    additionally it includes a 'units' key which describe the dimensions of the columns
    (if the column does not have known units, its an empty string)
    final result example:
        crf_solutions = {
            columns: [mykey1, mykey2],
            values: [[myvalue1, myvalue2], [myvalue3, myvalue4], [myvalue5, myvalue6]],
            units: [m/s, '']
        }

    units are defined as UNITS in consts.py (see import)
    """
    columns = serializers.ListField(
        child=serializers.CharField()
    )
    units = serializers.ListField(
        child=serializers.CharField()
    )
    data = serializers.ListField(
        child=serializers.ListField(
            child=PrimitiveTypeField()
        )
    )


    def __init__(self, child_serializer: serializers.ModelSerializer = None, query_columns=()):
        if child_serializer:
            self.child_is_dummy = False
            self.child_serializer = child_serializer
        else:
            self.child_is_dummy = True
            self.query_columns = query_columns
            self.child_serializer = DummySerializer()
        super().__init__()

    def is_valid(self, *, raise_exception=False):
        return True

    def _get_fields_from_meta(self) -> List[str]:
        django_model = self.child_serializer.Meta.model
        fields = django_model._meta.fields
        fields = [f.name for f in fields]
        if hasattr(self.child_serializer.Meta, 'exclude'):
            fields = [f for f in fields if f not in self.child_serializer.Meta.exclude]
        elif isinstance(self.child_serializer.Meta.fields, tuple) or isinstance(self.child_serializer.Meta.fields,
                                                                                list):
            fields = [f for f in fields if f in self.child_serializer.Meta.fields]
        else:
            assert self.child_serializer.Meta.fields == '__all__', 'Cannot interpret Meta fields'

        return fields

    def get_units_for(self, columns: List[str]) -> List[str]:
        DEFAULT_UNIT = ''
        return [UNITS.get(column, DEFAULT_UNIT) for column in columns]

    def to_representation(self, query_set: QuerySet):
        if self.child_is_dummy:
            columns = self.query_columns
        else:
            columns = self._get_fields_from_meta()
        try:
            rows = list(query_set.values_list(*columns).distinct().all())
        except TypeError:
            rows = list(query_set.values_list(*columns).all())

        units = self.get_units_for(columns)
        return {
            "columns": columns,
            "units": units,
            "rows": rows
        }


class ScrapedFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScrapedFile
        fields = ('file_url', 'parsed_date', 'last_modified', 'error')


class BaselineStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaselineStatistic
        exclude = ('id',)


class SourceStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = SourceStatistic
        exclude = ('id',)


class StationStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = StationStatistic
        exclude = ('id',)


class VLBISessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VLBISession
        fields = '__all__'


class SessionStatsSerializer(serializers.Serializer):
    session = serializers.JSONField()
    baselines = serializers.JSONField()
    sources = serializers.JSONField()
    stations = serializers.JSONField()


class ObjectInfoSerializer(serializers.Serializer):
    info = serializers.JSONField()
    sessions = serializers.JSONField()
    position = serializers.JSONField()


class TRFSolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TRFSolution
        fields = '__all__'
        exclude = ('id',)


class StationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Station
        fields = '__all__'

class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = '__all__'
