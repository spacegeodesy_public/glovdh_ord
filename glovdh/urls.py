from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test
from django.urls import path, re_path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from glovdh_ord.glovdh.views.common import ProgramsView, AgenciesView, FreeSearchView
from glovdh_ord.glovdh.views.crf import CrfQueryOptionsView, QueryCrfView
from glovdh_ord.glovdh.views.eop import EopQueryOptionsView, QueryEopView
from glovdh_ord.glovdh.views.home import UiFileView, StartPageView, ApiExamplesView
from glovdh_ord.glovdh.views.logs import LogsListView, LogView
from glovdh_ord.glovdh.views.session import SessionStatsView, QuerySessionsView
from glovdh_ord.glovdh.views.source import souceView, SourceListView, SourceStatsView
from glovdh_ord.glovdh.views.station import StationView, StationListView, StationStatsView
from glovdh_ord.glovdh.views.scraped_files import QueryScrapedFileView, FileTypeCountsView


def user_is_admin(user):
    return user.is_superuser


urlpatterns = [
    path('api/v1/', include([
        path('list-agencies', AgenciesView.as_view(), name='list-agencies'),
        path('list-programs', ProgramsView.as_view(), name='list-programs'),
        path('list-stations', StationListView.as_view(), name='list-station'),
        path('list-sources', SourceListView.as_view(), name='list-sources'),
        path('query-eop-options', EopQueryOptionsView.as_view(), name='eop-options'),
        path('query-crf-options', CrfQueryOptionsView.as_view(), name='crf-options'),
        path('query-eop', QueryEopView.as_view(), name='query-eop'),
        path('query-crf', QueryCrfView.as_view(), name='query-crf'),
        path('query-sessions', QuerySessionsView.as_view(), name='query-sessions'),
        path('query-files', QueryScrapedFileView.as_view(), name='query-files'),
        path('filetype-counts', FileTypeCountsView.as_view(), name='filetype-counts'),
        path('search', FreeSearchView.as_view(), name='free-search'),
        path('station/<str:station_code>', StationView.as_view(), name='station'),
        path('station-stats/<str:station_code>', StationStatsView.as_view(), name='station-stats'),
        path('source/<str:source_name>', souceView.as_view(), name='source'),
        path('source-stats/<str:source_name>', SourceStatsView.as_view(), name='source'),
        path('session/<str:session_id>', SessionStatsView.as_view(), name='source-stats'),
        path('schema', SpectacularAPIView.as_view(), name='schema'),
        path('docs', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    ])),
    path('api-examples', ApiExamplesView.as_view(), name='api-examples'),
    path('admin/logs/<str:run_id>', user_passes_test(user_is_admin)(LogView.as_view()), name='log_view'),
    path('admin/logs', user_passes_test(user_is_admin)(LogsListView.as_view()), name='logs_list_view'),
    path('admin', admin.site.urls),
    re_path('^.+\.(?:css|js|html|svg|ico|jpg)$', UiFileView.as_view(), name='ui_file_view'),
    re_path(r'^.*$', StartPageView.as_view(), name='start_page'),
]
