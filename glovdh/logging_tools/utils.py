import datetime
import logging
import os
import sys

from glovdh_ord.glovdh.logging_tools.json_logger import JSONFormatter


def setup_logging(run_id=None):

    logging.basicConfig(level=logging.DEBUG)
    if logging.getLogger().handlers:
        logging.getLogger().handlers = []
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    logdir = 'webscraping_logs'
    os.makedirs(logdir, exist_ok=True)

    if run_id is None:
        current_time = datetime.datetime.now()
        run_id = current_time.strftime('%Y%m%d_%H%M%S')
    log_file = f'{run_id}.log'

    if log_file in os.listdir(logdir):
        raise ValueError(f'Logfile "{log_file}" generated from run_id already exists in log directory.'
                         f' run_id must be unique!')

    file_handler = logging.FileHandler(os.path.join(logdir, log_file))
    file_handler.setLevel(logging.INFO)
    file_formatter = JSONFormatter()
    file_handler.setFormatter(file_formatter)

    console_handler = logging.StreamHandler(sys.stderr)
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)

    # Get the root logger
    root_logger = logging.getLogger()

    # Add handlers to the root logger
    root_logger.addHandler(file_handler)
    root_logger.addHandler(console_handler)

    return run_id
