from typing import List

from django import forms
from django.db.models import Q
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from glovdh_ord.glovdh.models import Station, Source, EOPSolution, CRFSolution
from glovdh_ord.glovdh.views.utils import openAPI_parameters_from_form
from glovdh_ord.glovdh.serializers import StringListSerializer, FreeSearchSerializer, ErrorSerializer
from django.http import JsonResponse
from glovdh_ord.glovdh.models import VLBISession, AnalystCenter


class ProgramsView(APIView):
    @extend_schema(
        request=None,
        responses={
            200: StringListSerializer
        },
        description="List all programs",
        tags=['Sessions']
    )
    def get(self, request, *args, **kwargs):
        programs = VLBISession.objects.values_list('program', flat=True).distinct()
        serializer = StringListSerializer({"data": programs})
        return Response(serializer.data, status=status.HTTP_200_OK)

class AgenciesView(APIView):
    class ValidationForm(forms.Form):
        project = forms.MultipleChoiceField(required=False,
                                            choices=[('eop', 'eop'), ('crf', 'crf')],
                                            help_text="null, 'eop' or 'crf' to filter for agencies"
                                                      " that produce results for the area")

    @extend_schema(
        request=None,
        tags=['Agencies'],
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: StringListSerializer,
            400: ErrorSerializer
        },
        description="Endpoint to get agencies that have results in general, or for CRF or for EOP"
    )
    def get(self, request, *args, **kwargs):
        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)


        if project_filter := request.GET.get('project'):
            if project_filter == 'eop':
                agencies = EOPSolution.objects.values_list('analyst_center', flat=True).distinct()
            elif project_filter == 'crf':
                agencies = CRFSolution.objects.values_list('analyst_center', flat=True).distinct()
            else:
                return Response({
                    'error': f'Invalid project_filter: {project_filter}'
                }, status=status.HTTP_400_BAD_REQUEST)
        else:
            agencies = AnalystCenter.objects.values_list('code', flat=True).distinct()

        serializer = StringListSerializer({
            "data": agencies})
        return Response(serializer.data, status=status.HTTP_200_OK)


class FreeSearchView(APIView):

    CATEGORY_LIMIT = 10
    class ValidationForm(forms.Form):
        searchword = forms.CharField(required=True, help_text='Search word')

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: FreeSearchSerializer,
            400: ErrorSerializer
        },
        description="A dict containing result stations, sources, sessions and programmes",
        tags=['Other']
    )
    def get(self, request, *args, **kwargs):
        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        searchword = request.GET.get('searchword')
        if not searchword:
            return JsonResponse({
                'error': 'searchword parameter is required'}, status=400)

        stations = (Station.objects.filter(Q(name__icontains=searchword) | Q(code__icontains=searchword))
                    .values_list('code', flat=True)
                    .distinct()[:self.CATEGORY_LIMIT])

        sources = Source.objects.filter(ivs_name__icontains=searchword)\
                      .values_list('ivs_name', flat=True)[:self.CATEGORY_LIMIT]

        sessions = VLBISession.objects.filter(session_id__icontains=searchword)\
                       .values_list('session_id',flat=True)\
                       .distinct()[:self.CATEGORY_LIMIT]

        centers = AnalystCenter.objects.filter(code__icontains=searchword)\
            .values_list('code', flat=True)[:self.CATEGORY_LIMIT]

        programmes = VLBISession.objects.filter(
            program__icontains=searchword
        ).values_list('program', flat=True).distinct()[:self.CATEGORY_LIMIT]

        serializer = FreeSearchSerializer(
            {
                "stations": list(stations),
                "sources": list(sources),
                "sessions": list(sessions),
                "analystCenters": list(centers),
                "programmes": list(programmes)
            }
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

