import os
from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.views import View
from rest_framework.views import APIView

LOG_FOLDER = "webscraping_logs"

class LogsListView(APIView):
    """
    Log list view to serve https://glovdh.ethz.ch/admin/logs
    """
    def get(self, request):
        logs_folder = LOG_FOLDER
        logs = os.listdir(logs_folder)
        return render(request, 'logs.html', {'run_ids': logs})

class LogView(APIView):
    """
    Log view to serve https://glovdh.ethz.ch/admin/logs/{run_id}
    """
    def get(self, request, run_id):
        log_file_path = os.path.join(LOG_FOLDER, run_id)
        if os.path.exists(log_file_path):
            with open(log_file_path, 'r') as log_file:
                log_content = log_file.readlines()
            return render(request, 'log_detail.html', {'log_content': log_content})
        else:
            return HttpResponseNotFound('<h1>404 - Log not found - Maybe run has not started yet? Try refreshing.</h1>')
