from django import forms
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response

from glovdh_ord.glovdh.models import CRFSolution
from glovdh_ord.glovdh.views.common import openAPI_parameters_from_form
from glovdh_ord.glovdh.views.consts import QUERY_LIMIT

from rest_framework.views import APIView

from glovdh_ord.glovdh.serializers import ErrorSerializer, StringListSerializer, \
    QuerySerializer, CRFSolutionSerializer




class QueryCrfView(APIView):
    class ValidationForm(forms.Form):
        analyst_center = forms.CharField(required=False, help_text='three letter code, like "asi"')
        filename = forms.CharField(required=False, help_text='filename like asi2023a.crf.gz')
        offset = forms.IntegerField(required=False, help_text='Query size is limited.'
                                                              ' You can offset the results to get the next chunk of data')
        all_fields = forms.BooleanField(required=False, help_text='whether to return all fields or just limited ones.'
                                                                  'Currently only returing source_file as an extra field.')

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: QuerySerializer,
            400: ErrorSerializer
        },
        description="Query CRF solutions from database",
        tags=['Query', 'Sources']
    )
    def get(self, request):

        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({
                'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query = CRFSolution.objects.select_related('source_file')

        if center := form.cleaned_data['analyst_center']:
            query = query.filter(analyst_center_id=center)
        if file := form.cleaned_data['filename']:
            query = query.filter(source_file_id__filename__startswith=file)

        offset = form.cleaned_data['offset'] or 0
        end_idx = offset + QUERY_LIMIT
        query = query[offset:end_idx]

        serializer = QuerySerializer(
            child_serializer=CRFSolutionSerializer(all_fields=form.cleaned_data['all_fields']),
        )
        data = serializer.to_representation(query)
        return Response(data, status=status.HTTP_200_OK)


class CrfQueryOptionsView(APIView):
    class ValidationForm(forms.Form):
        analyst_center = forms.CharField(required=True)

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: StringListSerializer,
            400: ErrorSerializer
        },
        description="Endpoint to get CRF files for a given analyst center.",
        tags=['Other']
    )
    def get(self, request):
        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({
                'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query = self._get_crf_queryset(request)
        source_files = list(sorted(query.values_list('source_file_id', flat=True).distinct(),
                                   reverse=True,
                                   key=lambda sf: sf.split('/')[-1]))

        serializer = StringListSerializer({
            "data": source_files})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def _get_crf_queryset(self, request):
        analyst_center = request.GET.get('analyst_center')
        query = CRFSolution.objects.filter(analyst_center_id=analyst_center)

        return query