import datetime

from django import forms
from django.http import JsonResponse
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from glovdh_ord.glovdh.models import VLBISession, BaselineStatistic, SourceStatistic, StationStatistic

from glovdh_ord.glovdh.views.common import openAPI_parameters_from_form
from glovdh_ord.glovdh.views.consts import DATE_INPUT_FORMATS
from glovdh_ord.glovdh.views.utils import StringListField
from glovdh_ord.glovdh.serializers import SessionStatsSerializer, BaselineStatisticSerializer, \
    SourceStatisticSerializer, StationStatisticSerializer, VLBISessionSerializer, ErrorSerializer, QuerySerializer


class SessionStatsView(APIView):

    @extend_schema(
        request=None,
        responses={
            200: SessionStatsSerializer,
            404: {
                "description": "No such session"
            }
        },
        description="Session details",
        tags=['Sessions']

    )
    def get(self, request, session_id):
        try:
            session = VLBISession.objects.get(pk=session_id)

            baselines = BaselineStatistic.objects.filter(session_id=session_id)
            sources = SourceStatistic.objects.filter(session_id=session_id)
            stations = StationStatistic.objects.filter(session_id=session_id)

            session_data = VLBISessionSerializer(session).data
            baselines_data = BaselineStatisticSerializer(baselines, many=True).data
            sources_data = SourceStatisticSerializer(sources, many=True).data
            stations_data = StationStatisticSerializer(stations, many=True).data

            ser = SessionStatsSerializer(
                {
                    'session': session_data,
                    'baselines': baselines_data,
                    'sources': sources_data,
                    'stations': stations_data
                })
            return Response(ser.data, status=status.HTTP_200_OK)

        except VLBISession.DoesNotExist:
            return JsonResponse({
                                    'error': 'No such session'}, status=404)


class QuerySessionsView(APIView):
    class ValidationForm(forms.Form):
        programs = StringListField(required=False, initial=None,
                                   help_text='comma separated list of programs. Like "AOV,APSG"')
        min_date = forms.DateTimeField(required=False,
                                       input_formats=DATE_INPUT_FORMATS,
                                       help_text=f'Earliest starting date. Formats:{" , ".join(DATE_INPUT_FORMATS)}')
        max_date = forms.DateTimeField(required=False,
                                       input_formats=DATE_INPUT_FORMATS,
                                       help_text=f'Latest starting date. Formats:{" , ".join(DATE_INPUT_FORMATS)}')
        type = forms.ChoiceField(choices=[('all', 'All'), ('intensive', 'Intensive'), ('24-hour', '24-hour')],
                                 help_text='Session type to filter for. Default is all. Can be "intensive" or "24-hour"',
                                 required=False)

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: QuerySerializer,
            400: ErrorSerializer
        },
        description="Query VLBI sessions",
        tags=['Query', 'Sessions']
    )
    def get(self, request):

        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query = VLBISession.objects.all()
        if (programs := form.cleaned_data['programs']) is not None:
            query = query.filter(program__in=programs)
        if (min_date := form.cleaned_data['min_date']) is not None:
            min_datetime_utc = min_date.replace(tzinfo=datetime.timezone.utc)
            query = query.filter(time_start__gte=min_datetime_utc)
        if (max_date := form.cleaned_data['max_date']) is not None:
            max_datetime_utc = max_date.replace(tzinfo=datetime.timezone.utc)
            query = query.filter(time_start__lte=max_datetime_utc)
        if (session_type := form.cleaned_data['type']) not in ('all', None, ''):
            query = query.filter(session_type=session_type)

        query = query.order_by('-time_start')

        serializer = QuerySerializer(
            child_serializer=VLBISessionSerializer()
        )

        data = serializer.to_representation(query)

        return Response(data, status=status.HTTP_200_OK)
