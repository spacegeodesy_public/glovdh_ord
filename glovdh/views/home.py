from django.http import HttpResponse, JsonResponse
import os

from rest_framework.views import APIView

import glovdh_ord.glovdh.settings as settings
from glovdh_ord.api_examples.examples import EXAMPLES

NON_TEXT_FILES = [
    '.svg',
    '.jpg',
    '.ico'
]
class StartPageView(APIView):
    def get(self, request):
        BASE_DIR = settings.STATICFILES_DIRS[0]
        file_name = 'index.html'
        file_path = os.path.join(BASE_DIR, file_name)
        try:
            with open(file_path, 'r') as file:
                # Read the contents of the file
                file_content = file.read()
            # Return the file content as the HTTP response
            return HttpResponse(file_content, content_type='text/html')
        except FileNotFoundError:
            # Return a 404 response if the file is not found
            return HttpResponse('File not found', status=404)

class UiFileView(APIView):
    """
    returns static files from the static directory.
    TODO: This is a really hacky implementation to serve both angular files
    TODO: that usually look like 'myjsfile.js' of 'myhtml.html' (only filename)
    TODO: and also django admin files
    TODO: that are queried with '/static/admin/js/myjsfile.js' format (full path)
    TODO: probably also reinvented the wheel with this, have to serve them both from static filder
    """
    def get(self, request):
        BASE_DIR = settings.STATIC_ROOT
        file_path = request.path.lstrip('/')
        if file_path.startswith('static/'):
            file_path = file_path[len('static/'):]
        is_not_text = any(file_path.endswith(ext) for ext in NON_TEXT_FILES)

        file_path = os.path.join(BASE_DIR, file_path)
        print(file_path)
        read_mode = 'rb' if is_not_text else 'r'
        enc = 'utf-8' if read_mode == 'r' else None
        try:
            with open(file_path, read_mode, encoding=enc) as file:
                # Read the contents of the file
                file_content = file.read()
            # Return the file content as the HTTP response
            if file_path.endswith('.js'):
                content_type = 'text/javascript'
            elif file_path.endswith('.css'):
                content_type = 'text/css'
            elif file_path.endswith('.html'):
                content_type = 'text/html'
            elif file_path.endswith('.svg'):
                content_type = 'image/svg+xml'
            elif file_path.endswith('.ico'):
                content_type = 'image/x-icon'
            else:
                content_type = 'text/plain'
            return HttpResponse(file_content, content_type=content_type)
        except FileNotFoundError:
            # Return a 404 response if the file is not found
            return HttpResponse('File not found', status=404)



class ApiExamplesView(APIView):

    def _def_read_example_file_content(self, filename: str) -> str:
        filepath = os.path.join(settings.BASE_DIR, 'api_examples', filename)
        with open(filepath, 'r', encoding='utf-8') as file:
            return file.read()
    def get(self, request):
        resps = []
        for example in sorted(EXAMPLES, key=lambda e: e.code_filename):
            try:
                content = self._def_read_example_file_content(example.code_filename)
                resps.append({
                    'chapter': example.chapter,
                    'title': example.title,
                    'description': example.description,
                    'code': content
                })
            except Exception as e:
                print(e)
                continue

        return JsonResponse({'examples': resps}, status=200)


