from typing import List

from django import forms
from drf_spectacular.utils import OpenApiParameter
from rest_framework.exceptions import ValidationError


def openAPI_parameters_from_form(form: forms.Form) -> List:
    reduce_map = {
        forms.CharField: str,
        forms.IntegerField: int,
        forms.BooleanField: bool,
        StringListField: str,
    }
    return [
        OpenApiParameter(
            name=field_name,
            description=field.help_text,
            required=field.required,
            type=reduce_map.get(type(field), str),
            default=field.initial,
        )
        for field_name, field in form.fields.items()
    ]


class StringListField(forms.Field):

    def clean(self, value):
        if value is None:
            return self.initial
        if isinstance(value, str):
            return value.split(',')
        raise ValidationError('Expected a list')

