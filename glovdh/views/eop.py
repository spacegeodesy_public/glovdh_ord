import datetime

from django import forms
from django.db.models import Q

from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from glovdh_ord.glovdh.models import EOPSolution
from glovdh_ord.glovdh.views.common import openAPI_parameters_from_form
from glovdh_ord.glovdh.views.consts import QUERY_LIMIT, DATE_INPUT_FORMATS
from glovdh_ord.glovdh.views.utils import StringListField
from glovdh_ord.glovdh.serializers import ErrorSerializer, EopQueryOptionsSerializer, QuerySerializer, \
    EopSolutionSerializer

UNKNOWN = '-UNKNOWN-'
class QueryEopView(APIView):
    class ValidationForm(forms.Form):
        filename = forms.CharField(required=False, help_text='filename like asi2023a.eoxy.gz')
        session_id = forms.CharField(required=False, help_text='Session Name')
        min_date = forms.DateTimeField(required=False,
                                       input_formats=DATE_INPUT_FORMATS,
                                       help_text=f'Formats:{" , ".join(DATE_INPUT_FORMATS)}')
        max_date = forms.DateTimeField(required=False,
                                       input_formats=DATE_INPUT_FORMATS,
                                       help_text=f'Formats:{" , ".join(DATE_INPUT_FORMATS)}')
        programs = StringListField(required=False, help_text="Comma separated programs to filter for. Like 'AOV,APSG'."
                                                             "To include results that have no identified programs"
                                                             f" and sessions simply include the {UNKNOWN} value in "
                                                             f"the list just like a program.")

        offset = forms.IntegerField(required=False, help_text='Query size is limited.'
                                                              ' You can offset the results to get the next chunk of data')
        all_fields = forms.BooleanField(required=False, help_text='whether to return all fields or just common ones.')

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: QuerySerializer,
            400: ErrorSerializer
        },
        description="Query Earth Orientation Parameters solutions",
        tags=['Query', 'EOP']
    )
    def get(self, request):

        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({
                'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)
        query = EOPSolution.objects.select_related('session_id', 'source_file')
        if filename := form.cleaned_data['filename']:
            query = query.filter(source_file__filename__startswith=filename)
        if session_id := form.cleaned_data['session_id']:
            query = query.filter(session_id=session_id)
        if programs := form.cleaned_data['programs']:
            q = Q(session_id__program__in=programs)
            if UNKNOWN in programs:
                q = q | Q(session_id__isnull=True)
            query = query.filter(q)

        if (min_date := form.cleaned_data['min_date']) is not None:
            min_datetime_utc = min_date.replace(tzinfo=datetime.timezone.utc)
            query = query.filter(session_id__time_start__gte=min_datetime_utc)
        if (max_date := form.cleaned_data['max_date']) is not None:
            max_datetime_utc = max_date.replace(tzinfo=datetime.timezone.utc)
            query = query.filter(session_id__time_start__lte=max_datetime_utc)

        query = query.order_by('epoch')
        offset = form.cleaned_data['offset'] or 0
        end_idx = offset + QUERY_LIMIT
        query = query[offset:end_idx]

        is_eoxy_file = '.eoxy' in form.cleaned_data['filename']
        ser = QuerySerializer(
            child_serializer=EopSolutionSerializer(all_fields=form.cleaned_data['all_fields'],
                                                   file_is_eoxy=is_eoxy_file),
        )
        data = ser.to_representation(query)
        return Response(data, status=status.HTTP_200_OK)


class EopQueryOptionsView(APIView):
    class ValidationForm(forms.Form):
        analyst_center = forms.CharField(required=True, help_text='three letter code, like "asi"')
        filename = forms.CharField(required=False, help_text='filename like asi2023a.eoxy.gz')

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: EopQueryOptionsSerializer,
            400: ErrorSerializer
        },
        description="Endpoint to get EOP files and programs for a given "
                    " analyst center and optional file combination.",
        tags=['Other']
    )
    def get(self, request):
        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({
                'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query = self._get_eop_queryset(request)

        query = query.select_related('session_id').values_list('session_id__program', flat=True).distinct()


        programs = list(query)

        try:
            none_in_programs = True
            program_is_null_index = programs.index(None)
            del programs[program_is_null_index]
        except ValueError:
            none_in_programs = False

        programs = list(sorted(programs))
        if none_in_programs:
            programs.append(UNKNOWN)

        source_files = list(sorted(query.values_list('source_file_id', flat=True).distinct(), reverse=True,
                                   key=lambda sf: sf.split('/')[-1]))

        serializer = EopQueryOptionsSerializer(
            {
                'programs': programs,
                'source_files': source_files
            }
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def _get_eop_queryset(self, request):
        analyst_center = request.GET.get('analyst_center')
        query = EOPSolution.objects.filter(analyst_center_id=analyst_center)

        if filename := request.GET.get('filename'):
            query = query.filter(source_file_id=filename)

        return query
