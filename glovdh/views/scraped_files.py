from django import forms
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from glovdh_ord.glovdh.models import ScrapedFile

from django.db.models import Count, Case, When, Value, CharField

from glovdh_ord.glovdh.views.common import openAPI_parameters_from_form
from glovdh_ord.glovdh.serializers import ErrorSerializer, QuerySerializer, ScrapedFileSerializer


class QueryScrapedFileView(APIView):
    DEFAULT_LIMIT = 10
    DEFAULT_SORT_BY = 'parsed_date'

    class ValidationForm(forms.Form):
        filename = forms.CharField(required=False, help_text='Filename. Can be used to get details of a file.'
                                                             'Like "asi2023a.eoxy.gz"')
        limit = forms.IntegerField(required=False, help_text='limit the number of results')
        sort_by = forms.ChoiceField(
            choices=[
                ('parsed_date', 'parsed_date'),
                ('-parsed_date', '-parsed_date'),
                ('last_modified', 'last_modified'),
                ('-last_modified', '-last_modified')
            ],
            required=False,
            help_text='sort by the given field. "-" prefix means descending'
        )

    @extend_schema(
        request=None,
        parameters=openAPI_parameters_from_form(ValidationForm()),
        responses={
            200: QuerySerializer,
            400: ErrorSerializer},
        description="An endpoint to list station data",
        tags=['Query', 'Files']
    )
    def get(self, request):
        form = self.ValidationForm(request.GET)
        if not form.is_valid():
            ser = ErrorSerializer({'errors': form.errors})
            return Response(data=ser.data, status=status.HTTP_400_BAD_REQUEST)

        query_file_name = form.cleaned_data.get('filename')
        limit = form.cleaned_data.get('limit') or self.DEFAULT_LIMIT
        sort_by = form.cleaned_data.get('sort_by') or self.DEFAULT_SORT_BY

        query = ScrapedFile.objects.all()
        if query_file_name:
            query = query.filter(filename__iexact=query_file_name)

        query = query.order_by(sort_by)[:limit]

        ser = QuerySerializer(
            child_serializer=ScrapedFileSerializer(),
        )
        data = ser.to_representation(query)
        return Response(data, status=status.HTTP_200_OK)


class FileTypeCountsView(APIView):

    @extend_schema(
        request=None,
        parameters=[],
        responses={
            200: QuerySerializer
        },
        description="An endpoint to get the file types and their counts in the database",
        tags=['Files']
    )
    def get(self, request):

        queryset = (
            ScrapedFile.objects
            .annotate(
                file_type=Case(
                    When(filename__icontains='eops', then=Value('eops')),
                    When(filename__icontains='eoxy', then=Value('eoxy')),
                    When(filename__icontains='eopi', then=Value('eopi')),
                    When(filename__icontains='trf', then=Value('trf')),
                    When(filename__icontains='crf', then=Value('crf')),
                    When(filename__icontains='skd', then=Value('skd')),
                    When(filename__icontains='master', then=Value('master')),
                    default=Value('other'),
                    output_field=CharField()
                )
            )
            .values('file_type')
            .annotate(count=Count('*'))
            .order_by('file_type')
        )

        columns = ['file_type', 'count']
        ser = QuerySerializer(
            query_columns=columns,
        )
        data = ser.to_representation(queryset)
        return Response(data, status=status.HTTP_200_OK)
