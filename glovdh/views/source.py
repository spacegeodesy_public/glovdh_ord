from django.db.models import Q, Max, F
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from glovdh_ord.glovdh.models import Source, SourceStatistic, CRFSolution, VLBISession
from glovdh_ord.glovdh.serializers import StringListSerializer, SourceSerializer, ObjectInfoSerializer, \
    VLBISessionSerializer, QuerySerializer, SourceStatisticSerializer


class souceView(APIView):

    @extend_schema(
        request=None,
        responses={
            200: ObjectInfoSerializer,
            404: {
                "description": "Source not found"
            }
        },
        description="Source details",
        tags=['Sources']
    )
    def get(self, request, source_name):
        objs = Source.objects.filter(Q(ivs_name=source_name) | Q(j2000_name_long=source_name)
                                      | Q(j2000_name_short=source_name) | Q(iers_name=source_name))

        if not objs.exists():
            return Response({
                'error': 'Source not found'
            }, status=404
            )

        source = objs.first()
        source_data = SourceSerializer(source).data

        sessions = VLBISession.objects.filter(
            pk__in=SourceStatistic.objects.filter(source_id=source.pk).values_list('session_id', flat=True)
        ).order_by('-time_start')

        session_data = QuerySerializer(
            child_serializer=VLBISessionSerializer()
        ).to_representation(sessions)

        max_filenames = CRFSolution.objects.filter(source=source) \
            .values('analyst_center') \
            .annotate(max_filename=Max('source_file_id'))

        latest_crf_solutions = CRFSolution.objects.filter(source=source,
                                                          analyst_center__in=max_filenames.values('analyst_center'),
                                                          source_file_id__in=max_filenames.values('max_filename')) \
            .order_by('analyst_center', F('source_file_id').desc())

        latest_crf_solutions_data = QuerySerializer(
            query_columns=['RA', 'DEC', 'sig_RA', 'sig_DEC', 'source_file_id']
        ).to_representation(latest_crf_solutions)

        ser = ObjectInfoSerializer(
            {
                'info': source_data,
                'sessions': session_data,
                'position': latest_crf_solutions_data
            }
        )
        return Response(ser.data, status=status.HTTP_200_OK)




class SourceListView(APIView):

    @extend_schema(
        request=None,
        parameters=[],
        responses={
            200: StringListSerializer,
            404: {
                "description": "Source not found"
            }
        },
        description="An endpoint to list all sources in the database",
        tags=['Sources']
    )
    def get(self, request):
        sources = Source.objects.all()
        sources = list(sources.values_list('ivs_name', flat=True))
        ser = StringListSerializer({"data": sources})

        return Response(ser.data, status=status.HTTP_200_OK)

class SourceStatsView(APIView):


    @extend_schema(
        request=None,
        responses={
            200: QuerySerializer
        },
        description="Get back scans and observations data about the source",
        tags=['Sources']
    )
    def get(self, request, source_name: str):

        stats = SourceStatistic.objects.filter(source_id=source_name)
        ser = QuerySerializer(
            child_serializer=SourceStatisticSerializer()
        )
        return Response(ser.to_representation(stats))
