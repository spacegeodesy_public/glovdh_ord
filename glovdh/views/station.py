from django.db.models import Max, F
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from glovdh_ord.glovdh.models import VLBISession, Station, TRFSolution, StationStatistic
from glovdh_ord.glovdh.serializers import QuerySerializer, TRFSolutionSerializer, VLBISessionSerializer, \
    StationSerializer, ObjectInfoSerializer, StationStatisticSerializer


class StationView(APIView):

    @extend_schema(
        request=None,
        responses={
            200: ObjectInfoSerializer,
            400: {'description': 'Station not found'}
        },
        description="Station details",
        tags=['Stations']
    )
    def get(self, request, station_code):

        session_query = (VLBISession.objects
                         .filter(stations__contains=station_code)
                         .order_by('-time_start'))
        try:
            station = Station.objects.get(code=station_code)
        except Station.DoesNotExist:
            return Response({'error': 'Station not found'}, status=400)


        max_filenames = TRFSolution.objects.filter(station_id=station_code) \
            .values('analyst_center') \
            .annotate(max_filename=Max('source_file_id'))

        latest_trf_solutions = TRFSolution.objects.filter(station_id=station_code,
                                                          analyst_center__in=max_filenames.values('analyst_center'),
                                                          source_file_id__in=max_filenames.values('max_filename')) \
            .order_by('analyst_center', F('source_file_id').desc())


        station_info = StationSerializer(station).data

        session_data = QuerySerializer(
            child_serializer=VLBISessionSerializer()
        ).to_representation(session_query)

        trf_query_data = QuerySerializer(
            child_serializer=TRFSolutionSerializer()
        ).to_representation(latest_trf_solutions)

        res = ObjectInfoSerializer(
            {
                'info': station_info,
                'sessions': session_data,
                'position': trf_query_data
            }
        )

        return Response(res.data, status=status.HTTP_200_OK)


class StationListView(APIView):

    @extend_schema(
        request=None,
        responses={
            200: QuerySerializer
        },
        description="An endpoint to list station data",
        tags=['Stations']
    )
    def get(self, request):

        stations = Station.objects.all()
        ser = QuerySerializer(
            child_serializer=StationSerializer()
        )
        data = ser.to_representation(stations)
        return Response(data, status=status.HTTP_200_OK)



class StationStatsView(APIView):

    @extend_schema(
        request=None,
        responses={
            200: QuerySerializer
        },
        description="Get back scans and observations data about the station",
        tags=['Stations']
    )
    def get(self, request, station_code: str):

        stats = StationStatistic.objects.filter(station=station_code)
        ser = QuerySerializer(
            child_serializer=StationStatisticSerializer()
        )
        return Response(ser.to_representation(stats))

