
import os

DATAFOLDER = os.path.join("glovdh_ord", "glovdh", "webscraping", "test", "test_data")

def get_test_file_path(subdir:str, fname: str):
    if subdir:
        return os.path.join(DATAFOLDER, subdir, fname)
    return os.path.join(DATAFOLDER, fname)
