import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import mjd_to_datetime
from glovdh_ord.glovdh.webscraping.parsers.crf_parsers import ASIParserCRF, AUSParserCRF, BKGParserCRF, OPAParserCRF
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class TestCRFParsers(unittest.TestCase):

    def _hms_to_degrees(self, hour: float, minute: float, second: float) -> float:
        hour_deg = hour * (360 / 24)
        minute_deg = minute * (360 / (24 * 60))
        second_deg = second * (360 / (24 * 60 * 60))

        total_degrees = hour_deg + minute_deg + second_deg
        return total_degrees

    def _DEC_to_degrees(self, degree: float, arcminute: float, arcsecond: float) -> float:
        degree, arcminutes, arcseconds = float(degree), float(arcminute), float(arcsecond)
        return degree + arcminutes / 60 + arcseconds / 3600

    def test_asi_parser(self):
        fname = 'asi2021a.crf'

        p = ASIParserCRF(fname)

        with open(get_test_file_path('crf', fname)) as f:
            content = f.read()

        df = p.to_dataframes(content)[0]

        expected_first_row = {
            'source': '2357-326',
            'RA': self._hms_to_degrees(0, 0, 20.39997960),
            'DEC': self._DEC_to_degrees(-32, 21, 01.2336683),
            'sig_RA': 0.00000467,
            'sig_DEC': 0.0001571,
            'correlation': 0.1198,
            'w_mean_mjd': mjd_to_datetime(56978.282673),
            'first_mjd': mjd_to_datetime(52305.750000),
            'last_mjd': mjd_to_datetime(58440.335417),
            'num_sessions': 5,
            'num_delays': 305,
            'num_rates': 0,
            'analyst_center': 'asi',
            'source_file': 'asi2021a.crf'
        }

        self.assertEqual(len(df), 4791)
        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'source': '2357-007',
            'RA': self._hms_to_degrees(23, 59, 36.82108083),
            'DEC': self._DEC_to_degrees(-0, 31, 12.8617639),
            'sig_RA': 0.00001209,
            'sig_DEC': 0.0001888,
            'correlation': -0.1570,
            'w_mean_mjd': mjd_to_datetime(58898.049089),
            'first_mjd': mjd_to_datetime(58504.417350),
            'last_mjd': mjd_to_datetime(58981.493056),
            'num_sessions': 3,
            'num_delays': 107,
            'num_rates': 0,
            'analyst_center': 'asi',
            'source_file': 'asi2021a.crf'
        }

        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_last_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_aus_parser(self):
        fname = 'aus2022a.crf'

        p = AUSParserCRF(fname)

        with open(get_test_file_path('crf',fname)) as f:
            content = f.read()

        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 5205)

        expected_first_row = {
            'source': '0000-160',
            'RA': self._hms_to_degrees(0, 3, 27.26417582),
            'DEC': self._DEC_to_degrees(-15, 47, 5.4560470),
            'sig_RA': 0.00001434,
            'sig_DEC': 0.0005654,
            'correlation': -0.0276,
            'w_mean_mjd': mjd_to_datetime(57916.5),
            'first_mjd': mjd_to_datetime(57135.3),
            'last_mjd': mjd_to_datetime(58795.1),
            'num_sessions': 4,
            'num_delays': 195,
            'num_rates': 0,
            'analyst_center': 'aus',
            'source_file': 'aus2022a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_first_row = {
            'source': '2051-172',
            'RA': self._hms_to_degrees(20, 53, 58.69029867),
            'DEC': self._DEC_to_degrees(-17, 2, 26.3335727),
            'sig_RA': 0.00001208,
            'sig_DEC': 0.0004781,
            'correlation': -0.3000,
            'w_mean_mjd': mjd_to_datetime(58713.2),
            'first_mjd': mjd_to_datetime(58593.3),
            'last_mjd': mjd_to_datetime(58829.9),
            'num_sessions': 2,
            'num_delays': 148,
            'num_rates': 0,
            'analyst_center': 'aus',
            'source_file': 'aus2022a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_bkg_parser(self):
        fname = 'bkg2022a.crf'

        p = BKGParserCRF(fname)

        with open(get_test_file_path('crf',fname)) as f:
            content = f.read()

        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 4776)


        expected_first_row = {
            'source': '2357-326',
            'RA': self._hms_to_degrees(0, 0, 20.39997462),
            'DEC': self._DEC_to_degrees(-32, 21, 1.2338636),
            'sig_RA': 0.00000665,
            'sig_DEC': 0.0002162,
            'correlation': -0.195,
            'w_mean_mjd': mjd_to_datetime(56307.7),
            'first_mjd': mjd_to_datetime(52305.8),
            'last_mjd': mjd_to_datetime(57770.0),
            'num_sessions': 2,
            'num_delays': 142,
            'num_rates': pd.NA,
            'analyst_center': 'bkg',
            'source_file': 'bkg2022a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)


        expected_first_row = {
            'source': '2357-318',
            'RA': self._hms_to_degrees(23, 59, 35.49153681),
            'DEC': self._DEC_to_degrees(-31, 33, 43.8246733),
            'sig_RA': 0.00000124,
            'sig_DEC': 0.0000239,
            'correlation': -0.153,
            'w_mean_mjd': mjd_to_datetime(56936.1),
            'first_mjd': mjd_to_datetime(52408.7),
            'last_mjd': mjd_to_datetime(59771.7),
            'num_sessions': 288,
            'num_delays': 5084,
            'num_rates': pd.NA,
            'analyst_center': 'bkg',
            'source_file': 'bkg2022a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)


    def test_opa_parser(self):
        fname = 'opa2023a.crf'

        p = OPAParserCRF(fname)

        with open(get_test_file_path('crf',fname)) as f:
            content = f.read()

        df = p.to_dataframes(content)[0]
        self.assertEqual(len(df), 4621)


        expected_first_row = {
            'source': '2357-326',
            'RA': self._hms_to_degrees(0, 0, 20.39998009),
            'DEC': self._DEC_to_degrees(-32, 21, 1.2337084),
            'sig_RA': 0.00000555,
            'sig_DEC': 0.0001915,
            'correlation': -0.0464,
            'w_mean_mjd': mjd_to_datetime(56559.8),
            'first_mjd': mjd_to_datetime(52306.7),
            'last_mjd': mjd_to_datetime(57776.0),
            'num_sessions': 4,
            'num_delays': 237,
            'num_rates': 0,
            'analyst_center': 'opa',
            'source_file': 'opa2023a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_first_row = {
            'source': '2357-318',
            'RA': self._hms_to_degrees(23, 59, 35.49153836),
            'DEC': self._DEC_to_degrees(-31, 33, 43.8246075),
            'sig_RA': 0.00000141,
            'sig_DEC': 0.0000271,
            'correlation': -0.1399,
            'w_mean_mjd': mjd_to_datetime(56828.9),
            'first_mjd': mjd_to_datetime(52409.7),
            'last_mjd': mjd_to_datetime(59921.8),
            'num_sessions': 263,
            'num_delays': 4649,
            'num_rates': 0,
            'analyst_center': 'opa',
            'source_file': 'opa2023a.crf'
        }
        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)


# Run the tests
if __name__ == '__main__':
    unittest.main()
