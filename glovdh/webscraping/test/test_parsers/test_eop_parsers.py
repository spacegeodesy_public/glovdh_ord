import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import mjd_to_datetime, ParserException
from glovdh_ord.glovdh.webscraping.parsers.eop_parsers import EOPParser2_0, EOPParser2_2, BKGParserEOP, EOPParser2_1, \
    EOPParser3_0, IAAParserEOP, OPAParserEOP
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class TestEopFormatParsers(unittest.TestCase):

    def test_eopformat_parser20(self):
        with open(get_test_file_path('eop', 'asi2020a.eops.gz')) as f:
            content = f.read()

        parser = EOPParser2_0('asi2020a.eops.gz')
        analyst_center_df, df = parser.to_dataframes(content)
        self.assertEqual(len(df), 3688)

        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'asi')

        expected_first_row = {
            'epoch': mjd_to_datetime(44927.227083),
            'XPO': -0.1069176,
            'YPO': 0.2828920,
            'UT1UTC': 0.1090802,
            'dPSI': 2.752,
            'dX': pd.NA,
            'dEPS': 0.029,
            'dY': pd.NA,
            'sig_XPO': 0.00042095,
            'sig_YPO': 0.00044947,
            'sig_UT1UTC': 0.00003400,
            'sig_dPSI': 0.6489,
            'sig_dX': pd.NA,
            'sig_dEPS': 0.2466,
            'sig_dY': pd.NA,
            'wrms': 27.5,
            'cor_XPO_YPO': 0.1414,
            'cor_XPO_UT1UTC': -0.7784,
            'cor_YPO_UT1UTC': -0.1471,
            'cor_dPDE': -0.0170,
            'nobs': 877,
            'session_id': 'xat811',
            'XPO_r': -0.0002771,
            'YPO_r': 0.0017579,
            'lod': 0.0022532,
            'dPSI_r': 0.0,
            'dX_r': pd.NA,
            'dEPS_r': 0.0,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.0004255,
            'sig_YPO_r': 0.0004454,
            'sig_lod': 0.00002858,
            'sig_dPSI_r': 0.0,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': 0.0,
            'sig_dY_r': pd.NA,
            'network': '[]',
            'analyst_center': "asi",
            'source_file': 'asi2020a.eops.gz'
        }

        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'epoch': mjd_to_datetime(58837.270833),
            'XPO': 0.0950430,
            'YPO': 0.2761546,
            'UT1UTC': -0.1735329,
            'dPSI': -109.127,
            'dX': pd.NA,
            'dEPS': -6.343,
            'dY': pd.NA,
            'sig_XPO': 0.00002837,
            'sig_YPO': 0.00002946,
            'sig_UT1UTC': 0.00000183,
            'sig_dPSI': 0.0748,
            'sig_dX': pd.NA,
            'sig_dEPS': 0.0364,
            'sig_dY': pd.NA,
            'wrms': 29.7,
            'cor_XPO_YPO': -0.0066,
            'cor_XPO_UT1UTC': 0.3874,
            'cor_YPO_UT1UTC': 0.1419,
            'cor_dPDE': -0.1367,
            'nobs': 4024.0,
            'session_id': 'r4925',
            'XPO_r': 0.0000149,
            'YPO_r': 0.0003913,
            'lod': 0.0006673,
            'dPSI_r': 0.0,
            'dX_r': pd.NA,
            'dEPS_r': 0.0,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.0001096,
            'sig_YPO_r': 0.0001058,
            'sig_lod': 0.00000620,
            'sig_dPSI_r': 0.0,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': 0.0,
            'sig_dY_r': pd.NA,
            'network': '[]',
            'analyst_center': "asi",
            'source_file': 'asi2020a.eops.gz'
        }

        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_last_row),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_eopformat_parser21(self):
        fname = 'gsf2020a.eopi.gz.txt'
        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = EOPParser2_1(fname)
        analyst_center_df, df = parser.to_dataframes(content)
        self.assertEqual(len(df), 12963)

        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'gsf')

        expected_first_row = {
            'epoch': mjd_to_datetime(49850.791667),
            'XPO': pd.NA,
            'YPO': pd.NA,
            'UT1UTC': 0.0395959,
            'dPSI': pd.NA,
            'dX': pd.NA,
            'dEPS': pd.NA,
            'dY': pd.NA,
            'sig_XPO': pd.NA,
            'sig_YPO': pd.NA,
            'sig_UT1UTC': 1.38e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': pd.NA,
            'sig_dEPS': pd.NA,
            'sig_dY': pd.NA,
            'wrms': 13.9,
            'cor_XPO_YPO': 0.0,
            'cor_XPO_UT1UTC': 0.0,
            'cor_YPO_UT1UTC': 0.0,
            'cor_dPDE': 0.0,
            'nobs': 15,
            'session_id': 'i95133',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["G3", "Wz"]',
            'analyst_center': 'gsf',
            'source_file': 'gsf2020a.eopi.gz.txt'}

        testing_na_values = 'testing_dummy_value_glovdh'

        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_first_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)
        expected_last_row = {
            'epoch': mjd_to_datetime(59974.790972),
            'XPO': pd.NA,
            'YPO': pd.NA,
            'UT1UTC': -0.0153431,
            'dPSI': pd.NA,
            'dX': pd.NA,
            'dEPS': pd.NA,
            'dY': pd.NA,
            'sig_XPO': pd.NA,
            'sig_YPO': pd.NA,
            'sig_UT1UTC': 8.1e-06,
            'sig_dPSI': pd.NA,
            'sig_dX': pd.NA,
            'sig_dEPS': pd.NA,
            'sig_dY': pd.NA,
            'wrms': 66.5,
            'cor_XPO_YPO': 0.0,
            'cor_XPO_UT1UTC': 0.0,
            'cor_YPO_UT1UTC': 0.0,
            'cor_dPDE': 0.0,
            'nobs': 63,
            'session_id': 'v23030',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["K2", "Oe"]',
            'analyst_center': 'gsf',
            'source_file': 'gsf2020a.eopi.gz.txt'}
        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_eopformat_parser30(self):
        fname = 'gsiint2c.eopi.gz.txt'
        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = EOPParser3_0(fname)
        analyst_center_df, df = parser.to_dataframes(content)
        self.assertEqual(len(df), 2042)
        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'gsi')

        expected_first_row = {
            'epoch': mjd_to_datetime(52741.333681),
            'XPO': pd.NA,
            'YPO': pd.NA,
            'UT1UTC': -0.3502037,
            'dPSI': pd.NA,
            'dX': pd.NA,
            'dEPS': pd.NA,
            'dY': pd.NA,
            'sig_XPO': pd.NA,
            'sig_YPO': pd.NA,
            'sig_UT1UTC': 4e-06,
            'sig_dPSI': pd.NA,
            'sig_dX': pd.NA,
            'sig_dEPS': pd.NA,
            'sig_dY': pd.NA,
            'wrms': 4.0,
            'cor_XPO_YPO': pd.NA,
            'cor_XPO_UT1UTC': pd.NA,
            'cor_YPO_UT1UTC': pd.NA,
            'cor_dPDE': pd.NA,
            'nobs': 15,
            'session_id': 'k03102',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Ts", "Wz"]',
            'analyst_center': 'gsi',
            'source_file': 'gsiint2c.eopi.gz.txt'}

        testing_na_values = 'testing_dummy_value_glovdh'

        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_first_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)
        expected_last_row = {
            'epoch': mjd_to_datetime(60169.333808),
            'XPO': pd.NA,
            'YPO': pd.NA,
            'UT1UTC': -0.0089276,
            'dPSI': pd.NA,
            'dX': pd.NA,
            'dEPS': pd.NA,
            'dY': pd.NA,
            'sig_XPO': pd.NA,
            'sig_YPO': pd.NA,
            'sig_UT1UTC': 1.08e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': pd.NA,
            'sig_dEPS': pd.NA,
            'sig_dY': pd.NA,
            'wrms': 26.02,
            'cor_XPO_YPO': pd.NA,
            'cor_XPO_UT1UTC': pd.NA,
            'cor_YPO_UT1UTC': pd.NA,
            'cor_dPDE': pd.NA,
            'nobs': 24,
            'session_id': 'q23225',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Mk", "Wz"]',
            'analyst_center': 'gsi',
            'source_file': 'gsiint2c.eopi.gz.txt'}
        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_eopformat_parser22(self):
        fname = 'asi2023a.eoxy.gz'
        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = EOPParser2_2(fname)
        analyst_center_df, df = parser.to_dataframes(content)

        self.assertEqual(len(df), 7223)
        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'asi')
        expected_first_row = {
            'epoch': mjd_to_datetime(44089.994123),
            'XPO': -0.0507264,
            'YPO': 0.39915,
            'UT1UTC': 0.0146345,
            'dPSI': pd.NA,
            'dX': -1.767,
            'dEPS': pd.NA,
            'dY': 3.482,
            'sig_XPO': 0.0005871,
            'sig_YPO': 0.0022562,
            'sig_UT1UTC': 0.00018384,
            'sig_dPSI': pd.NA,
            'sig_dX': 1.5627,
            'sig_dEPS': pd.NA,
            'sig_dY': 4.1073,
            'wrms': 47.29,
            'cor_XPO_YPO': 0.0047,
            'cor_XPO_UT1UTC': -0.1261,
            'cor_YPO_UT1UTC': -0.1111,
            'cor_dPDE': 0.5666,
            'nobs': 448,
            'session_id': 'xus79c',
            'XPO_r': 0.0028756,
            'YPO_r': 0.0015995,
            'lod': 0.001835774,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.00039352,
            'sig_YPO_r': 0.00044806,
            'sig_lod': 2.7186e-05,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Hs", "Gb", "Oo"]',
            'analyst_center': 'asi',
            'source_file': 'asi2023a.eoxy.gz'}

        testing_na_values = 'testing_dummy_value_glovdh'

        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_first_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'epoch': mjd_to_datetime(60307.270511),
            'XPO': 0.1425207,
            'YPO': 0.2016323,
            'UT1UTC': 0.0089176,
            'dPSI': pd.NA,
            'dX': 0.447,
            'dEPS': pd.NA,
            'dY': -0.159,
            'sig_XPO': 0.0002457,
            'sig_YPO': 0.0002119,
            'sig_UT1UTC': 2.363e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': 0.1235,
            'sig_dEPS': pd.NA,
            'sig_dY': 0.1131,
            'wrms': 29.82,
            'cor_XPO_YPO': 0.334,
            'cor_XPO_UT1UTC': 0.3962,
            'cor_YPO_UT1UTC': 0.9101,
            'cor_dPDE': 0.3689,
            'nobs': 1802,
            'session_id': 'r41135',
            'XPO_r': -0.0029439,
            'YPO_r': -0.0001685,
            'lod': -0.000145103,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.0003867,
            'sig_YPO_r': 0.00035398,
            'sig_lod': 1.8031e-05,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Bd", "Ft", "Ht", "Ma", "Ns", "Wz"]',
            'analyst_center': 'asi',
            'source_file': 'asi2023a.eoxy.gz'}

        testing_na_values = 'testing_dummy_value_glovdh'

        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_bkg_parser(self):
        fname = 'bkg2022a.eoxy.gz'
        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = BKGParserEOP(fname)
        analyst_center_df, df = parser.to_dataframes(content)

        self.assertEqual(len(df), 6302)
        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'bkg')

        expected_first_row = {
            'epoch': mjd_to_datetime(45704.241345),
            'XPO': -0.146455,
            'YPO': 0.099163,
            'UT1UTC': 0.3891208,
            'dPSI': pd.NA,
            'dX': 0.107,
            'dEPS': pd.NA,
            'dY': 0.067,
            'sig_XPO': 0.001079,
            'sig_YPO': 0.003238,
            'sig_UT1UTC': 8e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': 0.573,
            'sig_dEPS': pd.NA,
            'sig_dY': 0.83,
            'wrms': 54,
            'cor_XPO_YPO': 0.3195,
            'cor_XPO_UT1UTC': -0.8364,
            'cor_YPO_UT1UTC': -0.4872,
            'cor_dPDE': -0.0815,
            'nobs': 755,
            'session_id': 'ia152',
            'XPO_r': -0.004611,
            'YPO_r': -0.007415,
            'lod': 0.0015377,
            'dPSI_r': pd.NA,
            'dX_r': 0.0,
            'dEPS_r': pd.NA,
            'dY_r': 0.0,
            'sig_XPO_r': 0.002768,
            'sig_YPO_r': 0.008562,
            'sig_lod': 0.0002531,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': 0.0,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': 0.0,
            'network': '["Hr", "Mo", "Ri", "Wf"]',
            'analyst_center': 'bkg',
            'source_file': 'bkg2022a.eoxy.gz'
            }
        testing_na_values = 'testing_dummy_value_glovdh'
        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_first_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'epoch': mjd_to_datetime(59957.269817),
            'XPO': 0.033498,
            'YPO': 0.213417,
            'UT1UTC': -0.0172446,
            'dPSI': pd.NA,
            'dX': 0.333,
            'dEPS': pd.NA,
            'dY': -0.095,
            'sig_XPO': 4e-05,
            'sig_YPO': 5e-05,
            'sig_UT1UTC': 2.4e-06,
            'sig_dPSI': pd.NA,
            'sig_dX': 0.028,
            'sig_dEPS': pd.NA,
            'sig_dY': 0.032,
            'wrms': 37,
            'cor_XPO_YPO': -0.1095,
            'cor_XPO_UT1UTC': 0.336,
            'cor_YPO_UT1UTC': -0.1959,
            'cor_dPDE': -0.0654,
            'nobs': 2767,
            'session_id': 'r41085',
            'XPO_r': -0.002301,
            'YPO_r': 0.001232,
            'lod': 0.0001584,
            'dPSI_r': pd.NA,
            'dX_r': 0.0,
            'dEPS_r': pd.NA,
            'dY_r': 0.0,
            'sig_XPO_r': 0.000112,
            'sig_YPO_r': 0.000157,
            'sig_lod': 7.3e-06,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': 0.0,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': 0.0,
            'network': '["Bd", "Ht", "Is", "Ke", "Kk", "Mc", "Ns", "Wz", "Yg"]',
            'analyst_center': 'bkg',
            'source_file': 'bkg2022a.eoxy.gz'}

        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)


    def test_network_parsing_failes(self):
        fname = 'iaa2007a.eops.gz' #contains an invalid network PtBrFdHnKkKpLaMkNlNyOvScTsWzH

        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = IAAParserEOP(fname)
        self.assertRaises(ParserException, parser.to_dataframes, content)

    def test_iaa_parser(self):
        fname = 'iaa2007a_network_corrected.eops.gz'

        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = IAAParserEOP(fname)
        analyst_center_df, df = parser.to_dataframes(content)

        self.assertEqual(len(df), 4977)
        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'iaa')

        expected_last_row = {
            'epoch': mjd_to_datetime(44203.20382),
            'XPO': 0.138974,
            'YPO': 0.321553,
            'UT1UTC': -0.2616764,
            'dPSI': -1.056,
            'dX': pd.NA,
            'dEPS': 1.324,
            'dY': pd.NA,
            'sig_XPO': 0.001704,
            'sig_YPO': 0.00179,
            'sig_UT1UTC': 8.91e-05,
            'sig_dPSI': 0.616,
            'sig_dX': pd.NA,
            'sig_dEPS': 0.675,
            'sig_dY': pd.NA,
            'wrms': 31,
            'cor_XPO_YPO': -0.3109,
            'cor_XPO_UT1UTC': -0.834,
            'cor_YPO_UT1UTC': 0.588,
            'cor_dPDE': -0.545,
            'nobs': 285,
            'session_id': 'xatl79',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Gb", "Ef", "Oo", "Hs"]',
            'analyst_center': 'iaa',
            'source_file': 'iaa2007a_network_corrected.eops.gz'}

        testing_na_values = 'testing_dummy_value_glovdh'
        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'epoch': mjd_to_datetime(59226.20808),
            'XPO': 0.057629,
            'YPO': 0.31499,
            'UT1UTC': -0.1738552,
            'dPSI': 0.15,
            'dX': pd.NA,
            'dEPS': 0.144,
            'dY': pd.NA,
            'sig_XPO': 3.9e-05,
            'sig_YPO': 4.3e-05,
            'sig_UT1UTC': 2.3e-06,
            'sig_dPSI': 0.03,
            'sig_dX': pd.NA,
            'sig_dEPS': 0.032,
            'sig_dY': pd.NA,
            'wrms': 105,
            'cor_XPO_YPO': -0.2259,
            'cor_XPO_UT1UTC': 0.4545,
            'cor_YPO_UT1UTC': -0.1596,
            'cor_dPDE': 0.0352,
            'nobs': 9429,
            'session_id': '210111',
            'XPO_r': pd.NA,
            'YPO_r': pd.NA,
            'lod': pd.NA,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': pd.NA,
            'sig_YPO_r': pd.NA,
            'sig_lod': pd.NA,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Wz", "Kk", "Sg", "Sh", "Ht", "Ma", "Mc", "Nt", "Ns", "On", "Ys", "Ho"]',
            'analyst_center': 'iaa',
            'source_file': 'iaa2007a_network_corrected.eops.gz'}

        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

    def test_opa_parser(self):
        fname = 'opa2019i.eopi.gz.txt'

        with open(get_test_file_path('eop', fname)) as f:
            content = f.read()

        parser = OPAParserEOP(fname)
        analyst_center_df, df = parser.to_dataframes(content)
        self.assertEqual(len(analyst_center_df), 1)
        self.assertEqual(analyst_center_df['code'].iloc[0], 'opa')

        self.assertEqual(len(df), 7952)

        expected_first_row = {
            'epoch': mjd_to_datetime(49721.87708),
            'XPO': 0.0,
            'YPO': 0.0,
            'UT1UTC': -28.6124806,
            'dPSI': pd.NA,
            'dX': 0.0,
            'dEPS': pd.NA,
            'dY': 0.0,
            'sig_XPO': 0.0,
            'sig_YPO': 0.0,
            'sig_UT1UTC': 4.12e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': 0.0,
            'sig_dEPS': pd.NA,
            'sig_dY': 0.0,
            'wrms': 11.26,
            'cor_XPO_YPO': 0.0,
            'cor_XPO_UT1UTC': 0.0,
            'cor_YPO_UT1UTC': 0.0,
            'cor_dPDE': 0.0,
            'nobs': 15,
            'session_id': 'i95004',
            'XPO_r': 0.0,
            'YPO_r': 0.0,
            'lod': 0.0,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.0,
            'sig_YPO_r': 0.0,
            'sig_lod': 0.0,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["G3", "Wz"]',
            'analyst_center': 'opa',
            'source_file': 'opa2019i.eopi.gz.txt'}

        testing_na_values = 'testing_dummy_value_glovdh'
        pd.testing.assert_series_equal(df.iloc[0].fillna(testing_na_values)
                                       , pd.Series(expected_first_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_last_row = {
            'epoch': mjd_to_datetime(59341.80139),
            'XPO': 0.0,
            'YPO': 0.0,
            'UT1UTC': -37.1827861,
            'dPSI': pd.NA,
            'dX': 0.0,
            'dEPS': pd.NA,
            'dY': 0.0,
            'sig_XPO': 0.0,
            'sig_YPO': 0.0,
            'sig_UT1UTC': 1.04e-05,
            'sig_dPSI': pd.NA,
            'sig_dX': 0.0,
            'sig_dEPS': pd.NA,
            'sig_dY': 0.0,
            'wrms': 42.56,
            'cor_XPO_YPO': 0.0,
            'cor_XPO_UT1UTC': 0.0,
            'cor_YPO_UT1UTC': 0.0,
            'cor_dPDE': 0.0,
            'nobs': 20,
            'session_id': 'i21127',
            'XPO_r': 0.0,
            'YPO_r': 0.0,
            'lod': 0.0,
            'dPSI_r': pd.NA,
            'dX_r': pd.NA,
            'dEPS_r': pd.NA,
            'dY_r': pd.NA,
            'sig_XPO_r': 0.0,
            'sig_YPO_r': 0.0,
            'sig_lod': 0.0,
            'sig_dPSI_r': pd.NA,
            'sig_dX_r': pd.NA,
            'sig_dEPS_r': pd.NA,
            'sig_dY_r': pd.NA,
            'network': '["Kk", "Wz"]',
            'analyst_center': 'opa',
            'source_file': 'opa2019i.eopi.gz.txt'}
        pd.testing.assert_series_equal(df.iloc[-1].fillna(testing_na_values)
                                       , pd.Series(expected_last_row).fillna(testing_na_values),
                                       check_names=False,
                                       check_datetimelike_compat=True)


# Run the tests
if __name__ == '__main__':
    unittest.main()
