import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.source_name_table_parser import SourceNameTableParser
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class TestSourceNameTableParser(unittest.TestCase):
    def test_source_parsers(self):
        fname = 'IVS_SrcNamesTable.txt'

        with open(get_test_file_path('other', fname), 'r') as f:
            content = f.read()

        p = SourceNameTableParser(fname)

        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 7186)

        first_row = {
            'ivs_name': '2357+025',
            'j2000_name_long': 'J000019.2+024814',
            'j2000_name_short': 'J0000+0248',
            'iers_name': pd.NA,
            'source_file': 'IVS_SrcNamesTable.txt'
        }
        ser = pd.Series(first_row).astype('string')
        pd.testing.assert_series_equal(df.iloc[0], ser,
                                       check_names=False,
                                       check_datetimelike_compat=True)

        last_row = {
            'ivs_name': '2357-007',
            'j2000_name_long': 'J235936.8-003112',
            'j2000_name_short': 'J2359-0031',
            'iers_name': pd.NA,
            'source_file': 'IVS_SrcNamesTable.txt'
        }
        ser = pd.Series(last_row).astype('string')
        pd.testing.assert_series_equal(df.iloc[-1], ser,
                                       check_names=False,
                                       check_datetimelike_compat=True)


if __name__ == '__main__':
    unittest.main()
