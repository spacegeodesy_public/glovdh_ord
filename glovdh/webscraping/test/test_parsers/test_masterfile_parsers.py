import datetime
import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.master_parsers import MasterFileParser1_0, MasterFileParser2_0
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path
from glovdh_ord.glovdh.webscraping.consts import NORMAL_SESSION


class TestMasterFileParsers(unittest.TestCase):

    def test_master_file_parser1_0(self):

        fname = 'master07.txt'

        with open(get_test_file_path('master', fname)) as f:
            content = f.read()

        p = MasterFileParser1_0(fname)
        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 173)

        expected_first_row = {
            'program': 'IVS-R1257',
            'time_start': datetime.datetime(year=2007, month=1, day=2, hour=17, tzinfo=datetime.timezone.utc),
            'time_end': datetime.datetime(year=2007, month=1, day=3, hour=17, tzinfo=datetime.timezone.utc),
            'session_id': 'r1257',
            'stations': 'HhKkNyWz',
            'dbc': 'XA',
            'operation_center': 'NASA',
            'corr': 'BONN',
            'subm': 'NASA',
            'session_type': NORMAL_SESSION,
            'source_file':'master07.txt'
        }

        pd.testing.assert_series_equal(df.iloc[0], pd.Series(expected_first_row),
                                       check_datetimelike_compat=True,
                                       check_names=False)

        expected_last_row = {
            'program': 'IVS-R4308',
            'time_start': datetime.datetime(year=2007, month=12, day=27, hour=18, minute=30, tzinfo=datetime.timezone.utc),
            'time_end': datetime.datetime(year=2007, month=12, day=28, hour=18, minute=30, tzinfo=datetime.timezone.utc),
            'session_id': 'r4308',
            'stations': 'FtKkMaSvTcWzZc',
            'dbc': 'XE',
            'operation_center': 'USNO',
            'corr': 'WASH',
            'subm': 'USNO',
            'session_type': NORMAL_SESSION,
            'source_file':'master07.txt'
        }

        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_last_row),
                                       check_datetimelike_compat=True,
                                       check_names=False)

    def test_is_intensive(self):

        self.assertTrue(MasterFileParser2_0('master08-int.txt')._is_intensive())
        self.assertFalse(MasterFileParser2_0('master07.txt')._is_intensive())
        self.assertFalse(MasterFileParser1_0('master2023.txt')._is_intensive())
        self.assertTrue(MasterFileParser1_0('master2024-int.txt')._is_intensive())
        self.assertFalse(MasterFileParser2_0('master84.txt')._is_intensive())
        self.assertTrue(MasterFileParser2_0('master94-int.txt')._is_intensive())


    def test_master_file_parser2_0(self):

        fname = 'master2024.txt'

        with open(get_test_file_path('master', fname)) as f:
            content = f.read()

        p = MasterFileParser2_0(fname)
        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 224)

        expected_second_row = {
            'program': 'VGOS-OPS',
            'time_start': datetime.datetime(year=2024, month=1, day=3, hour=12, tzinfo=datetime.timezone.utc),
            'time_end': datetime.datetime(year=2024, month=1, day=4, hour=12, tzinfo=datetime.timezone.utc),
            'session_id': 'vo4003',
            'stations': 'GsK2MgOeOwSaWfWs',
            'dbc': None,
            'operation_center': 'NASA',
            'corr': 'VIEN',
            'subm': 'NASA',
            'session_type': NORMAL_SESSION,
            'source_file':'master2024.txt'
        }

        pd.testing.assert_series_equal(df.iloc[1], pd.Series(expected_second_row),
                                       check_datetimelike_compat=True,
                                       check_names=False)

        expected_last_row = {
            'program': 'IVS-R1',
            'time_start': datetime.datetime(year=2024, month=12, day=30, hour=17, minute=0, tzinfo=datetime.timezone.utc),
            'time_end': datetime.datetime(year=2024, month=12, day=31, hour=17, minute=0, tzinfo=datetime.timezone.utc),
            'session_id': 'r11188',
            'stations': 'FtHtKvMaSh',
            'dbc': None,
            'operation_center': 'NASA',
            'corr': 'BONN',
            'subm': 'NASA',
            'session_type': NORMAL_SESSION,
            'source_file': 'master2024.txt'
        }

        pd.testing.assert_series_equal(df.iloc[-1], pd.Series(expected_last_row),
                                       check_datetimelike_compat=True,
                                       check_names=False)


if __name__ == '__main__':
    unittest.main()
