import datetime
import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.trf_parsers import TRFParser
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class TestTRFParsers(unittest.TestCase):


    def test_epoch_conversion(self):
        helper = TRFParser(None)

        d1 = '00:07:60'
        expected1 = datetime.datetime(year=2000, month=1, day=7, hour=0, minute=1,  tzinfo=datetime.timezone.utc)

        d2 = '99:160:00'
        expected2 = datetime.datetime(year=1999, month=6, day=9, tzinfo=datetime.timezone.utc)

        d3 = '20:366:3661'
        expected3 = datetime.datetime(year=2020, month=12, day=31, hour=1, minute=1, second=1, tzinfo=datetime.timezone.utc)

        self.assertEqual(expected1, helper._epoch_to_date(d1))
        self.assertEqual(expected2, helper._epoch_to_date(d2))
        self.assertEqual(expected3, helper._epoch_to_date(d3))




    def test_trf_parsers(self):
        fname = 'ivs2023b.trf'

        with open(get_test_file_path('trf', fname), 'r') as f:
            content = f.read()

        p = TRFParser(fname)

        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 960 / 6)
        helper = TRFParser(None)
        expected_nyales20 = {
            'station': 'NYALES20',
            'ref_epoch': helper._epoch_to_date('10:001:00000'),
            'epoch_start': helper._epoch_to_date('94:277:64898'),
            'epoch_end': helper._epoch_to_date('23:181:66545'),
            'solution_version': 1,
            'estimate_stax': 1.20246256968331E+06,
            'estimate_stay': 2.52734497243063E+05,
            'estimate_staz': 6.23776617802048E+06,
            'estimate_velx': -1.43791212918781E-02,
            'estimate_vely': 7.61699278649959E-03,
            'estimate_velz': 1.08725576226187E-02,
            'error_stax': 1.17106E-04,
            'error_stay': 1.30898E-04,
            'error_staz': 1.84363E-04,
            'error_velx': 1.00941E-05,
            'error_vely': 1.04905E-05,
            'error_velz': 2.04390E-05,
            'analyst_center': 'ivs',
            'source_file': 'ivs2023b.trf'
        }
        nyales20_row = df[df.station == 'NYALES20'].iloc[0]
        pd.testing.assert_series_equal(nyales20_row, pd.Series(expected_nyales20),
                                       check_names=False,
                                       check_datetimelike_compat=True)

        expected_ohiggins  = {
            'station': 'OHIGGINS',
            'ref_epoch': helper._epoch_to_date('10:001:00000'),
            'epoch_start': helper._epoch_to_date('15:035:64833'),
            'epoch_end': helper._epoch_to_date('23:039:62976'),
            'solution_version': 3,
            'estimate_stax': 1.52583325690421E+06,
            'estimate_stay': -2.43246366611424E+06,
            'estimate_staz': -5.67617445253710E+06,
            'estimate_velx': 1.73222119889972E-02,
            'estimate_vely': -1.29902133014239E-03,
            'estimate_velz': -4.90653182976045E-04,
            'error_stax': 2.44432E-03,
            'error_stay': 2.91424E-03,
            'error_staz': 5.36708E-03,
            'error_velx': 2.35746E-04,
            'error_vely': 2.86455E-04,
            'error_velz': 5.14294E-04,
            'analyst_center': 'ivs',
            'source_file': 'ivs2023b.trf'
        }
        row_ohiggins_v3 = df[(df.station == 'OHIGGINS') & (df.solution_version == 3)].iloc[0]
        pd.testing.assert_series_equal(row_ohiggins_v3, pd.Series(expected_ohiggins),
                                       check_names=False,
                                       check_datetimelike_compat=True)


if __name__ == '__main__':
    unittest.main()
