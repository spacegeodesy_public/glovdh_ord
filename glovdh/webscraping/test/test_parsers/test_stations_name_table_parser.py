import unittest

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.stations_name_table_parser import StationParser
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class TestStationParser(unittest.TestCase):
    def test_station_parsers(self):
        fname = 'position.cat'

        with open(get_test_file_path('other', fname), 'r') as f:
            content = f.read()

        p = StationParser(fname)

        df = p.to_dataframes(content)[0]

        self.assertEqual(len(df), 215-18)

        first_row = {
            'name': 'AGGO',
            'code': 'Ag',
            'longitude': 360-57.93,
            'latitude': -34.91,
            'source_file': 'position.cat'
        }
        ser = pd.Series(first_row)

        pd.testing.assert_series_equal(df.iloc[0], ser,
                                       check_names=False,
                                       check_datetimelike_compat=True)

        last_row = {
            'name': 'ZELENCHK',
            'code': 'Zc',
            'longitude': 360-318.43,
            'latitude': 43.79,
            'source_file': 'position.cat'
        }
        ser = pd.Series(last_row)
        pd.testing.assert_series_equal(df.iloc[-1], ser,
                                       check_names=False,
                                       check_datetimelike_compat=True)


if __name__ == '__main__':
    unittest.main()
