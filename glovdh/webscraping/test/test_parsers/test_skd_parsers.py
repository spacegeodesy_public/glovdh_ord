import unittest

from glovdh_ord.glovdh.webscraping.parsers.skd.skd import SKDParser
from glovdh_ord.glovdh.webscraping.parsers.skd_parser_adapter import SKDParserAdapter
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path


class SKDParserAdapterTest(unittest.TestCase):
    def test_skd_adapter(self):
        fname = 'aov079.skd'

        with open(get_test_file_path('other', fname)) as f:
            content = f.read()

        parser = SKDParserAdapter(fname)
        sources, stations, baselines = parser.to_dataframes(content)

        reference_parser = SKDParser()
        reference_parser.parse(content)

        scans_per_source = {s: s.nscans for s in reference_parser.sources.sources if s.nscans > 0}
        obs_per_source = {s: s.nobs for s in reference_parser.sources.sources if s.nscans > 0}
        scans_per_station = {s.name: s.nscans for s in reference_parser.stations.stations if s.nscans > 0}
        obs_per_station = {s.name: s.nobs for s in reference_parser.stations.stations if s.nscans > 0}
        obs_per_baseline = reference_parser.baseline_obs

        for source, n_scans in scans_per_source.items():
            rows = sources[sources.iers_name == source.name]
            self.assertEqual(len(rows), 1)
            row = rows.iloc[0]
            self.assertEqual(row['observations'], obs_per_source[source])
            if source.altname == '':
                source.altname = None
            self.assertEqual(row['ivs_name'], source.altname)
            self.assertEqual(row['iers_name'], source.name)
            self.assertEqual(row['scans'], n_scans)
            self.assertEqual(row['source_file'], fname)

        for station, n_scans in scans_per_station.items():
            rows = stations[stations.station==station]
            self.assertEqual(len(rows), 1)
            row = rows.iloc[0]
            self.assertEqual(row['observations'], obs_per_station[station])
            self.assertEqual(row['scans'], n_scans)
            self.assertEqual(row['source_file'], fname)

        for baseline, obs in obs_per_baseline.items():
            station1, station2 = list(map(str.strip, baseline.split('-')))
            rows = baselines[(baselines.station1 == station1) & (baselines.station2 == station2)]
            self.assertEqual(len(rows), 1)
            row = rows.iloc[0]
            self.assertEqual(row['observations'], obs)
            self.assertEqual(row['source_file'], fname)
