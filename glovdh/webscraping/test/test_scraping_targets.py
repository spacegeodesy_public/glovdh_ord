from datetime import datetime, timezone
from unittest.mock import patch
from django.test import TestCase

from glovdh_ord.glovdh.models import ScrapedFile
from glovdh_ord.glovdh.webscraping.db_handler import DBHandler
from glovdh_ord.glovdh.webscraping.scraping_collections import StationURLScrapeTarget, SKDFilesTarget, FileArchiveTarget
from glovdh_ord.glovdh.webscraping.test.test_utils import get_test_file_path
from glovdh_ord.glovdh.webscraping.web_utils import FileMetadata, Downloader

#https://raw.githubusercontent.com/nvi-inc/sked_catalogs/main/antenna.cat
mock_antenna_file_header = """
    * VERSION 2024-01-11_iGSFC 
    * CATALOG antenna.cat  
    * 
    * ANTENNA.CAT - antenna information 
    *  
    * Updates (most recent at the top): 
    * 2024-01-11 JMG Added SESHAN13, TIANMA13, URUMQI13 per 2023-12-06 Fengchun
    * 2023-04-18 JMG Changed RAEGSMAR limits from 300-780 to 0-420
"""


class MockDownloader(Downloader):

    def __init__(self, return_value=''):
        self.return_value = return_value

    def download_content(self, url: str) -> str:
        return self.return_value


class TestScrapingTargets(TestCase):
    yesterday = datetime(year=2024, month=1, day=1, tzinfo=timezone.utc)
    today = datetime(year=2024, month=1, day=2, tzinfo=timezone.utc)
    def setUp(self):

        ScrapedFile.objects.create(
            filename='myfile1',
            file_url='myhost/myurl1',
            parsed_date=self.yesterday,
            last_modified=self.yesterday
        ),
        ScrapedFile.objects.create(
            filename='myfile2',
            file_url='myhost/myurl2',
            parsed_date=self.yesterday,
            last_modified=self.yesterday
        ),
        ScrapedFile.objects.create(
            filename='myfile3',
            file_url='myhost/myurl3',
            parsed_date=self.yesterday,
            last_modified=self.yesterday
        ),

    def test_nothing_changed(self):

        archive = FileArchiveTarget(
            tag='test',
            db_handler=DBHandler(),
            file_url_pattern='.*',
            minimum_last_file_update=self.yesterday,
            archive_url='myhost/',
            file_url_template='myhost/{filename}',
            downloader=MockDownloader()
        )

        updates = {
            'myurl1': self.yesterday,
            'myurl2': self.yesterday,
            'myurl3': self.yesterday
        }
        archive.parse_file_updates_table = lambda *args, **kwargs: updates

        self.assertEqual(archive.get_target_files(),
                         [])

    def test_outdated(self):


        archive = FileArchiveTarget(
            tag='test',
            db_handler=DBHandler(),
            file_url_pattern='.*',
            minimum_last_file_update=self.yesterday,
            archive_url='myhost/',
            downloader=MockDownloader(''),
            file_url_template='myhost/{filename}'
        )

        updates = {
            'myurl1': self.today,
            'myurl2': self.yesterday,
            'myurl3': self.yesterday
        }
        archive.parse_file_updates_table = lambda *args, **kwargs: updates

        res = archive.get_target_files()
        expected = [FileMetadata(file_url='myhost/myurl1', last_modified=self.today)]
        res = list(sorted(res, key=lambda x: x.file_url))
        for i in range(1):
            self.assertEqual(res[i].file_url, expected[i].file_url)
            self.assertEqual(res[i].last_modified, expected[i].last_modified)


    def test_antenna_parse_latest_change(self):

        archive = StationURLScrapeTarget(
            tag='test',
            db_handler=DBHandler(),
            file_url='randomurl',
            downloader=MockDownloader(mock_antenna_file_header)
        )

        expected = datetime(year=2024, month=1, day=11, tzinfo=timezone.utc)

        self.assertEqual(archive._parse_last_modified(), expected)

    def test_update_parsing(self):

        with open(get_test_file_path('', 'skd_2023_kt009_at_20240219.html')) as f:
            content = f.read()

        updates = FileArchiveTarget.parse_file_updates_table(content)
        expected = \
            {
                'MD5SUMS': datetime.strptime('2023:09:02 00:05:17','%Y:%m:%d %H:%M:%S'),
                'SHA512SUMS': datetime.strptime('2023:09:02 00:05:17','%Y:%m:%d %H:%M:%S'),
                'kt009k2.log': datetime.strptime('2023:09:01 23:35:05','%Y:%m:%d %H:%M:%S'),
                'kt009k2_full.log.bz2': datetime.strptime('2023:09:01 23:35:05','%Y:%m:%d %H:%M:%S'),
                'kt009kk.log': datetime.strptime('2023:09:02 00:05:01', '%Y:%m:%d %H:%M:%S')
            }

        for filename, expected_date in expected.items():
            expected_date = expected_date.replace(tzinfo=timezone.utc)
            self.assertEqual(expected_date, updates[filename])

