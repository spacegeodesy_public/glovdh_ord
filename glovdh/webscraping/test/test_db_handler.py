import datetime

import pandas as pd

from django.test import TestCase

from glovdh_ord.glovdh.models import ScrapedFile, Source, Station, VLBISession, AnalystCenter, SourceStatistic, \
    SessionType, EOPSolution, CRFSolution, TRFSolution, StationStatistic, BaselineStatistic
from glovdh_ord.glovdh.webscraping.db_handler import DBHandler, DBHandlerException
from glovdh_ord.glovdh.webscraping.web_utils import FileMetadata
from glovdh_ord.glovdh.webscraping.consts import INTENSIVE_SESSION, NORMAL_SESSION


class DBHandlerTestCase(TestCase):
    def setUp(self):
        self.db_handler = DBHandler()
        today = datetime.datetime.now().astimezone(datetime.timezone.utc)
        yesterday = today - datetime.timedelta(days=1)
        ScrapedFile.objects.create(filename='scraped_yesterday.txt', file_url='scraped_yesterday.txt', parsed_date=yesterday, last_modified=yesterday)
        ScrapedFile.objects.create(filename='scraped_yesterday1.txt', file_url='scraped_yesterday1.txt', parsed_date=yesterday, last_modified=yesterday)
        ScrapedFile.objects.create(filename='scraped_yesterday2.txt', file_url='scraped_yesterday2.txt', parsed_date=yesterday, last_modified=yesterday)

    def test_outdated_files(self):
        today = datetime.datetime.now().astimezone(datetime.timezone.utc)
        yesterday = today - datetime.timedelta(days=1, minutes=5)

        metadatas = [
            FileMetadata(
                file_url='scraped_yesterday.txt',
                last_modified=yesterday),
            FileMetadata(
                file_url='scraped_yesterday1.txt',
                last_modified=today),
            FileMetadata('scraped_yesterday2.txt',
                         last_modified=today)
        ]

        files = self.db_handler.get_outdated_file_urls(metadatas)

        self.assertEqual(len(files), 2)
        expected = {f.file_url for f in metadatas if f.file_url != 'scraped_yesterday.txt'}

        self.assertEqual(files, expected)

        files = self.db_handler.get_outdated_file_urls([])
        self.assertEqual(files, set())

    def test_analyst_center_field_transformation(self):
        AnalystCenter.objects.create(code='asi', source_file=ScrapedFile.objects.get(pk='scraped_yesterday.txt'))
        AnalystCenter.objects.create(code='bkg', source_file=ScrapedFile.objects.get(pk='scraped_yesterday.txt'))

        records = [
            {
                'meta': None,
                'analyst_center': 'asi'
            },
            {
                'meta': [],
                'analyst_center': 'bkg'
            },
        ]

        self.db_handler.analyst_center_field_to_analyst_center_obj(records)
        self.assertEqual(records[0]['analyst_center'], AnalystCenter.objects.get(pk='asi'))
        self.assertEqual(records[1]['analyst_center'], AnalystCenter.objects.get(pk='bkg'))

        self.assertRaises(DBHandlerException,
                          self.db_handler.analyst_center_field_to_analyst_center_obj,
                          [
                              {
                                  'meta': None,
                                  'analyst_center': 'non_existing_center'
                              }
                          ]
                          )

    def test_missing_files(self):
        metadatas = [
            FileMetadata(file_url='not_in_db.txt',
                         last_modified=datetime.datetime.now().astimezone(datetime.timezone.utc)),
            FileMetadata(file_url='scraped_yesterday1.txt',
                         last_modified=datetime.datetime.now().astimezone(datetime.timezone.utc)),
            FileMetadata(file_url='scraped_yesterday1.txt',
                         last_modified=datetime.datetime.now().astimezone(datetime.timezone.utc)),
        ]
        files = self.db_handler.get_missing_file_urls(metadatas)

        self.assertEqual(files, {'not_in_db.txt'})
        self.assertEqual(set(), self.db_handler.get_missing_file_urls([]))

    def test_session_type_field_transformation(self):
        records = [
            {
                'session_type': INTENSIVE_SESSION
            },
            {
                'session_type': NORMAL_SESSION
            }
        ]

        self.db_handler.session_types_to_session_types_obj(records)
        self.assertEqual(records[0]['session_type'], SessionType.objects.get(pk=INTENSIVE_SESSION))
        self.assertEqual(records[1]['session_type'], SessionType.objects.get(pk=NORMAL_SESSION))
        self.assertRaises(SessionType.DoesNotExist, self.db_handler.session_types_to_session_types_obj,
                          [
                              {
                                  'session_type': 'non_existing_type'
                              }
                          ])
        self.assertRaises(SessionType.DoesNotExist, self.db_handler.session_types_to_session_types_obj,
                          [
                              {
                                  'session_type': None
                              }
                          ])

    def test_source_file_field_transformation(self):
        records = [
            {
                'other_meta': {},
                'None': None,
                'source_file': 'scraped_yesterday.txt'
            },
            {
                'other_meta': {
                    's': []},
                'None': None,
                'source_file': 'scraped_yesterday1.txt'
            },
            {
                'source_file': 'scraped_yesterday2.txt'
            },
            {
                'source_file': 'not_existing'
            },
        ]

        self.assertRaises(ScrapedFile.DoesNotExist, self.db_handler.source_file_field_to_scrapedfile_obj, records)
        self.db_handler.source_file_field_to_scrapedfile_obj(records[:-1])

        self.assertEqual(records[0]['source_file'], ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))
        self.assertEqual(records[1]['source_file'], ScrapedFile.objects.get(file_url='scraped_yesterday1.txt'))
        self.assertEqual(records[2]['source_file'], ScrapedFile.objects.get(file_url='scraped_yesterday2.txt'))

        self.assertRaises(DBHandlerException, self.db_handler.source_file_field_to_scrapedfile_obj,
                          [
                              {
                                  'source_file': None
                              }
                          ])

        empty = []
        self.db_handler.source_file_field_to_scrapedfile_obj(empty)
        self.assertEqual(empty, [])

    def test_iau_name_field_transformation(self):
        Source.objects.create(ivs_name='ivs_name0',
                              source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))
        Source.objects.create(ivs_name='ivs_name1',
                              j2000_name_long='j2000_name_long1',
                              source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))
        Source.objects.create(ivs_name='ivs_name2',
                              j2000_name_short='j2000_name_short2',
                              source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))
        Source.objects.create(ivs_name='ivs_name3',
                              iers_name='iers_name3',
                              source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))

        records = [
            {
                'other_meta': {},
                'source': 'ivs_name0',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'other_meta': {
                    's': []},
                'source': 'j2000_name_long1',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'other_meta': {
                    's': []},
                'source': 'j2000_name_short2',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'other_meta': {
                    's': []},
                'source': 'iers_name3',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'other_meta': {
                    's': []},
                'source': 'not_existing',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'other_meta': {
                    's': []},
                'source': None,
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
        ]

        self.db_handler.source_fields_to_source_obj(records[:-1])
        self.assertEqual(records[0]['source'], Source.objects.get(ivs_name='ivs_name0'))
        self.assertEqual(records[1]['source'], Source.objects.get(ivs_name='ivs_name1'))
        self.assertEqual(records[2]['source'], Source.objects.get(ivs_name='ivs_name2'))
        self.assertEqual(records[3]['source'], Source.objects.get(ivs_name='ivs_name3'))
        self.assertEqual(records[4]['source'], Source.objects.get(ivs_name='not_existing'))

        self.assertRaises(DBHandlerException, self.db_handler.source_fields_to_source_obj, records)

        empty = []
        self.db_handler.source_fields_to_source_obj(empty)
        self.assertEqual(empty, [])

    def test_station_field_transformation(self):
        Station.objects.create(code='C0', name='code0', latitude=0, longitude=0,
                               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday1.txt'))
        Station.objects.create(code='C1', name='code1', latitude=0, longitude=0,
                               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday1.txt'))
        Station.objects.create(code='C2', name='code1', latitude=0, longitude=0,
                               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))
        Station.objects.create(code='C3', name='code3', latitude=0, longitude=0,
                               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday2.txt'))

        records = [
            {
                'station': 'code0',
                'other_meta': [],
                'source_file': 'test.txt'
            },
            {
                'station': 'CODE0',
                'other_meta': None,
                'source_file': 'test.txt'
            },
            {
                'station': 'cOdE3',
                'source_file': 'test.txt'
            },
            {
                'station': 'C2',
                'other_meta': [],
                'source_file': 'test.txt'
            },
            {
                'station': 'nonexisting_station',
                'other_meta': [],
                'source_file': 'test.txt'
            },
            {
                'station': None,
                'other_meta': [],
                'source_file': 'test.txt'
            }
        ]

        self.db_handler.station_field_to_station_obj(records[:-2])
        self.assertEqual(records[0]['station'], Station.objects.get(code='C0'))
        self.assertEqual(records[1]['station'], Station.objects.get(code='C0'))
        self.assertEqual(records[2]['station'], Station.objects.get(code='C3'))
        self.assertEqual(records[3]['station'], Station.objects.get(code='C2'))

        self.assertRaises(DBHandlerException, self.db_handler.station_field_to_station_obj, records)

        empty = []
        self.db_handler.session_id_field_to_session_obj(empty)
        self.assertEqual(empty, [])

    def test_session_id_field_transformation(self):
        VLBISession.objects.create(session_id='sess0',
                                   source_file=ScrapedFile.objects.get(file_url='scraped_yesterday2.txt'),
                                   session_type=SessionType.objects.get(pk=INTENSIVE_SESSION))
        VLBISession.objects.create(session_id='sess1',
                                   source_file=ScrapedFile.objects.get(file_url='scraped_yesterday1.txt'),
                                   session_type=SessionType.objects.get(pk=INTENSIVE_SESSION))
        VLBISession.objects.create(session_id='sess2',
                                   source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'),
                                   session_type=SessionType.objects.get(pk=INTENSIVE_SESSION))

        records = [
            {
                'session_id': 'SESS0',
                'other_meta': [],
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': 'sess2',
                'other_meta': [],
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': 'Sess1',
                'other_meta': [],
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': '??--??',
                'other_meta': None,
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': pd.NA,
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': None,
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
            {
                'session_id': 'non_existing_session_id',
                'source_file': ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
            },
        ]

        self.db_handler.session_id_field_to_session_obj(records)
        self.assertEqual(records[0]['session_id'], VLBISession.objects.get(session_id='sess0'))
        self.assertEqual(records[1]['session_id'], VLBISession.objects.get(session_id='sess2'))
        self.assertEqual(records[2]['session_id'], VLBISession.objects.get(session_id='Sess1'))
        self.assertEqual(records[3]['session_id'], None)
        self.assertEqual(records[4]['session_id'], None)
        self.assertEqual(records[5]['session_id'], None)

        empty = []
        self.db_handler.session_id_field_to_session_obj(empty)
        self.assertEqual(empty, [])

    def test_skd_file_difference(self):
        session1 = VLBISession('session1', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION)
                               , source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))

        session2 = VLBISession('session2', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION)
                               , source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt'))

        source1 = Source('source1', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday1.txt'))

        source_stat = SourceStatistic(session_id=session1, source=source1,
                                      source_file=ScrapedFile.objects.get(file_url='scraped_yesterday1.txt')
                                      )

        session1.save()
        session2.save()
        source1.save()
        source_stat.save()

        session_ids = self.db_handler.get_sessions_with_missing_skd_file()

        self.assertListEqual(session_ids, ['session2'])

    def test_eop_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        VLBISession(session_id='mysession', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION),
                    source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        first_batch = [
            {
                'session_id': 'mysession',
                'source_file': 'scraped_yesterday.txt',
                'epoch': datetime.datetime.now().astimezone(datetime.timezone.utc),
                'analyst_center': 'asi',
                'dY': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, EOPSolution)

        second_batch = [
            {
                'session_id': 'mysession',
                'source_file': 'scraped_yesterday.txt',
                'epoch': datetime.datetime.now().astimezone(datetime.timezone.utc),
                'analyst_center': 'asi',
                'dY': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, EOPSolution)

        objects = EOPSolution.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].dY, 7.0)

    def test_crf_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        Source(ivs_name='mysource',
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()

        first_batch = [
            {
                'source': 'mysource',
                'source_file': 'scraped_yesterday.txt',
                'analyst_center': 'asi',
                'RA': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, CRFSolution)

        second_batch = [
            {
                'source': 'mysource',
                'source_file': 'scraped_yesterday.txt',
                'analyst_center': 'asi',
                'RA': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, CRFSolution)

        objects = CRFSolution.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].RA, 7.0)

    def test_trf_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        Station(code='ms', name='mystation', longitude=0, latitude=0,
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()

        first_batch = [
            {
                'station': 'mystation',
                'source_file': 'scraped_yesterday.txt',
                'analyst_center': 'asi',
                'estimate_stax': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, TRFSolution)

        second_batch = [
            {
                'station': 'mystation',
                'source_file': 'scraped_yesterday.txt',
                'analyst_center': 'asi',
                'estimate_stax': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, TRFSolution)

        objects = TRFSolution.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].estimate_stax, 7.0)

    def test_station_stats_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        Station(code='ms', name='mystation', longitude=0, latitude=0,
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()
        VLBISession(session_id='mysession', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION),
                    source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()

        first_batch = [
            {
                'session_id': 'mysession',
                'station': 'mystation',
                'source_file': 'scraped_yesterday.txt',
                'scans': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, StationStatistic)

        second_batch = [
            {
                'session_id': 'mysession',
                'station': 'mystation',
                'source_file': 'scraped_yesterday.txt',
                'scans': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, StationStatistic)

        objects = StationStatistic.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].scans, 7.0)

    def test_source_stats_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        Source(ivs_name='mysource',
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()
        VLBISession(session_id='mysession', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION),
                    source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()

        first_batch = [
            {
                'session_id': 'mysession',
                'source': 'mysource',
                'source_file': 'scraped_yesterday.txt',
                'scans': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, SourceStatistic)

        second_batch = [
            {
                'session_id': 'mysession',
                'source': 'mysource',
                'source_file': 'scraped_yesterday.txt',
                'scans': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, SourceStatistic)

        objects = SourceStatistic.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].scans, 7.0)


    def test_baseline_stats_solution_unique_constraint(self):
        AnalystCenter(code='asi', source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()
        Station(code='s1',name='blue_station', longitude=0, latitude=0,
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()
        Station(code='s2',name='red_station', longitude=0, latitude=0,
               source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')
               ).save()
        VLBISession(session_id='mysession', session_type=SessionType.objects.get(pk=INTENSIVE_SESSION),
                    source_file=ScrapedFile.objects.get(file_url='scraped_yesterday.txt')).save()

        first_batch = [
            {
                'session_id': 'mysession',
                'station1': 'red_station',
                'station2': 'blue_station',
                'source_file': 'scraped_yesterday.txt',
                'observations': 6.0,
            }
        ]

        self.db_handler.load_model(first_batch, BaselineStatistic)

        second_batch = [
            {
                'session_id': 'mysession',
                'station1': 'red_station',
                'station2': 'blue_station',
                'source_file': 'scraped_yesterday.txt',
                'observations': 7.0,
            }
        ]

        self.db_handler.load_model(second_batch, BaselineStatistic)

        objects = BaselineStatistic.objects.all()
        self.assertEqual(len(objects), 1)
        self.assertEqual(objects[0].observations, 7.0)
