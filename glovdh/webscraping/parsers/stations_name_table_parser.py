from typing import Optional

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import TabularDataParser, split_and_strip, ParserFactory


class StationParserFactory(ParserFactory):
    """
    Factory responsible for Station parser creation
    """
    def get_default_parser(self) -> 'TabularDataParser':
        return StationParser(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'TabularDataParser':
        return StationParser(file_url)



class StationParser(TabularDataParser):
    output_dtypes = {
        'name': 'string',
        'code': 'string',
        'longitude': 'float64',
        'latitude': 'float64',
        'source_file': 'string'
    }

    input_columns = ['code', 'name', 'X (m)', 'Y (m)', 'Z (m)',
                     'Occ.Code', 'longitude', 'latitude', 'Origin']

    def __init__(self, filename: Optional[str]):
        super().__init__(filename)
        self.table_header_seen = False

    def line_to_values(self, line: str) -> list:
        if line.startswith('*'):
            line = line[1:]
        values = super().line_to_values(line)
        values = values[:len(self.input_columns)]
        return values

    def is_data_line(self, line: str) -> bool:
        if line.replace(' ', '').startswith(
                '*ID Name'.replace(' ', '')):
            self.table_header_seen = True
            return False
        if self.table_header_seen:
            return True
        return not line.startswith('*') and self.table_header_seen

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:

        #https://github.com/nvi-inc/sked_catalogs/blob/main/position.cat
        #stores longitudes differently
        df.longitude = df.longitude.astype('float64').apply(lambda x: 360.0-x)

        return df
