import abc
import logging

from datetime import datetime, timedelta, timezone
from typing import List, Optional, Dict, Union, Iterable

import numpy as np
import pandas as pd
from pandas import DatetimeTZDtype

UTC_OFFSET = timedelta(hours=0)


class FactoryException(Exception):
    pass


class ParserException(Exception):
    pass


class Parser(abc.ABC):
    """
    The base class for all parsers.
    They are meant to take a file content and
    output multiple dataframes as a result.
    """

    def __init__(self):
        self.logger = logging.getLogger(type(self).__name__)

    @abc.abstractmethod
    def to_dataframes(self, filecontent: str) -> Iterable[pd.DataFrame]:
        """Converts a string filecontent into multiple dataframes"""
        raise NotImplementedError

    @abc.abstractmethod
    def get_empty(self) -> Iterable[pd.DataFrame]:
        """Returns an iterable of the empty dataframes.
        The columns of the empty dataframes should be the same columns as its
        corresponding pair. This function is useful when there is an error while
        processing a file so the result should contain 0 records"""
        raise NotImplementedError


class ParserFactory(abc.ABC):
    """
    The Parser factories should be responsible for creating the parsers.
    The Subclasses of this class should decide which parser to use for a given
    file.
    """

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def url_to_filename(self, file_url: str):
        """Returns a filename from a url
        >>> url_to_filename('http://www.myhost.com/myfile.txt')
        myfile.txt
        >>> url_to_filename('myfile.txt')
        myfile.txt
        """
        return file_url.split('/')[-1]

    @abc.abstractmethod
    def get_default_parser(self) -> 'Parser':
        """Get a default parser. Useful when there is an error
        and Parser.get_empty() has to be called to get the empty dataframes
        but we have not yet created a parser for that
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_file_parser(self, file_url: str, filecontent: str) -> 'Parser':
        """
        This function should be used to decide which parser to use for a given file
        Parameters
        ----------
        file_url: file url
        filecontent: already read string content of the file

        Returns
        -------
        Parser object to use for the file
        """
        raise NotImplementedError


class TabularDataParser(Parser):
    """
    A parser class more specialized to read human-readable tables from a text file.
    ONLY able to produce a single output dataframe currently.
    The parser goes line by line and read the data is self.is_data_line() returns true.
    It will try to split on whitespaces and get a list of values that have the same length as
    self.input_columns. If the values have different lengths than the input columns, an exception
    is raised and parsing is aborted
    """

    def __init__(self, file_url: Optional[str]):
        super().__init__()
        self.file_url = file_url

    @property
    @abc.abstractmethod
    def input_columns(self) -> List[str]:
        """
        Returns: A list of string column names that should be used to name the columns
        of the read table. The column names do not have to match the colum names in the text file
        as those are not read. The input column names should be a superset of self.output_dtypes.keys()
        as the output column names are based on these columns.
        -------

        """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def output_dtypes(self) -> Dict[str, str]:
        """
        Returns: a mapping between column_name: pandas_column_type_string.
        This tells the parser object what columns should be in the output dataframe.
        The keys will be used as column names, the values will be used to set the datatypes
        of the resulting dataframe. The columns (keys) should be a subset of the columns
        of self.input_columns. The other columns will be dropped in the result dataframe
        -------
        """
        raise NotImplementedError

    @abc.abstractmethod
    def is_data_line(self, line: str) -> bool:
        """

        Parameters
        ----------
        line: A string line of the read file

        Returns
        -------
        True if the line's content should be part of the resulting dataframe

        """
        raise NotImplementedError

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        A hook to overried to postprocess the newly read dataframe with input_columns and raw values

        Parameters
        ----------
        df: The newly read dataframe from the file

        Returns
        -------
        """
        df.columns = list(self.output_dtypes.keys())
        return df

    def get_empty(self) -> List[pd.DataFrame]:
        """

        Returns
        -------
        An empty dataframe with standard output_columns of the parser

        """
        return [pd.DataFrame(columns=list(self.output_dtypes.keys()))]

    def cast_to_target_types(self, df: pd.DataFrame):
        """
        Modifies the parameter dataframe to have the desired target types INPLACE.
        LOCALIZES datetime inputs to UTC

        Parameters
        ----------
        df: the read dataframe from the file.


        """
        tz_columns = [col for col, val in self.output_dtypes.items() if
                      val == 'datetime64[ns]']

        for col in tz_columns:
            if not isinstance(df[col].dtype, DatetimeTZDtype):
                df[col] = df[col].astype('datetime64[ns]')
            if df[col].dt.tz is None:
                df[col] = df[col].dt.tz_localize('utc')

        not_tz_cols = [col for col in self.output_dtypes.keys() if col not in tz_columns]

        df[not_tz_cols] = df[not_tz_cols].astype(
            {
                col: dtype for col, dtype in self.output_dtypes.items()
                if col not in tz_columns
            }
        )

    def _create_dataframes(self, rows: List[List[str]]) -> List[pd.DataFrame]:
        """
        Creates the dataframes from a list of rows.

        Parameters
        ----------
        rows: A List of rows that contain the raw, read. string values

        Returns
        -------
        a pandas dataframe
        -------

        """
        df = pd.DataFrame(rows, columns=self.input_columns)

        df = self.postprocess_dataframe(df)
        clean_na(df)

        df['source_file'] = self.file_url
        self.cast_to_target_types(df)
        return [df[list(self.output_dtypes.keys())]]

    def line_to_values(self, line: str) -> List[str]:
        """
        Takes a file line and returns a list of values for each column

        Parameters
        ----------
        line: read file line

        Returns
        -------
        List of values in the row

        """
        return split_and_strip(None, line)

    def _read_file(self, file_content: str) -> List[List[str]]:
        """
        Reads a file line by line, converts the file to a list of rows containing values.
        If a row's length is not the same as input_columns, an exception is raised

        Parameters
        ----------
        file_content: content of the file

        Returns
        -------
        List of row values


        """
        rows = []
        for idx, line in enumerate(file_content.splitlines()):

            if line == '' or not self.is_data_line(line):
                continue

            values = self.line_to_values(line)

            if len(values) != len(self.input_columns):
                raise ParserException(
                    f'Invalid line in file: {self.file_url}. expected {len(self.input_columns)} columns'
                    f', got {len(values)} columns. Line index: {idx}'
                )
            rows.append(values)

        return rows

    def to_dataframes(self, file_content: str) -> List[pd.DataFrame]:
        """
        Creates a dataframe from a file content

        Parameters
        ----------
        file_content: content of the file

        Returns
        -------
        List of length 1. The dataframe is the one that is read from the file.
        A list of length 1 to keep the interface of parsers that produce more dataframes

        """
        try:
            assert self.file_url is not None, ('This parser was not created with a filename'
                                               'which is necessary for parsing')

            rows = self._read_file(file_content)

            return self._create_dataframes(rows)
        except Exception as e:
            raise ParserException(f'{self.__class__.__name__}: {str(e)}')


def split_and_strip(splitter: Optional[str], string: str) -> List[str]:
    """
    Splits a string into a list of strings and strips the restulting items.
    >>> split_and_strip(None, '')
    []

    >>> split_and_strip(None, 'hello hi')
    ["hello", "hi"]

    >>> split_and_strip(None, 'hello SEPhi')
    ["hello", "hi"]


    Parameters
    ----------
    splitter a substring to split on
    string the string to splut

    Returns
    -------
    A list of resulting values

    """
    return list(map(str.strip, string.split(splitter)))


def clean_na(df: pd.DataFrame, inplace=True) -> Optional[pd.DataFrame]:
    """
    Takes a dataframe and sets all nullable values to None

    Parameters
    ----------
    df: pd.DataFrame
        The dataframe to normalize all nullable values.
    inplace: bool
        Whether to modify the dataframe inplace.

    Returns
    -------
    Optional[pd.DataFrame]
        The dataframe if inplace=False, otherwise None.

    Examples
    --------
    >>> import pandas as pd
    >>> import numpy as np
    >>> df = pd.DataFrame({'A': [1, 'NA', np.nan, ''], 'B': ['foo', '-0', 'bar', None]})
    >>> clean_na(df, inplace=False)
         A     B
    0    1   foo
    1  None  None
    2  None   bar
    3  None  None
    """
    res = df.replace([pd.NA, 'NA', '-0', np.nan, ''], None, inplace=inplace)
    if not inplace:
        return res


def mjd_to_datetime(mjd: Union[str, float]) -> datetime:
    """
    Converts Modified Julian Date (MJD) to a datetime object.

    Parameters
    ----------
    mjd: Union[str, float]
        A string or float representation of the Modified Julian Date.

    Returns
    -------
    datetime
        The corresponding datetime object.

    Examples
    --------
    >>> mjd_to_datetime(58000.0)
    datetime.datetime(2017, 9, 4, 0, 0, tzinfo=datetime.timezone.utc)
    >>> mjd_to_datetime("58000.5")
    datetime.datetime(2017, 9, 4, 12, 0, tzinfo=datetime.timezone.utc)
    """
    if isinstance(mjd, str):
        mjd = float(mjd)

    mjd_epoch = datetime(1858, 11, 17, tzinfo=timezone.utc)
    mjd_integer = int(mjd)
    mjd_fraction = mjd - mjd_integer

    seconds_in_day = 24 * 60 * 60
    seconds_fraction = mjd_fraction * seconds_in_day

    date_part = mjd_epoch + timedelta(days=mjd_integer)

    hours = int(seconds_fraction // 3600)
    minutes = int((seconds_fraction % 3600) // 60)
    seconds = seconds_fraction % 60

    return date_part + timedelta(hours=hours, minutes=minutes, seconds=seconds)
