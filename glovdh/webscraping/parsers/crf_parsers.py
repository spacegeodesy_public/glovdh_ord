import abc
from typing import Union

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import (
    TabularDataParser, split_and_strip, mjd_to_datetime, ParserFactory)


class CRFParserFactory(ParserFactory):
    """
    Factory responsible for CRF parser creation
    """

    def get_default_parser(self) -> 'TabularDataParser':
        return DefaultCRFParser(None)

    def get_file_parser(self, file_url:str, filecontent: str) -> 'TabularDataParser':
        filename = self.url_to_filename(file_url)
        if filename.startswith("bkg"): return BKGParserCRF(file_url)
        if filename.startswith("asi"): return ASIParserCRF(file_url)
        if filename.startswith("aus"): return AUSParserCRF(file_url)
        if filename.startswith('opa'): return OPAParserCRF(file_url)

        self.logger.warning(f'Could not determine parser for {file_url}. Returning DefaultCRFParser')
        return DefaultCRFParser(file_url)


class DefaultCRFParser(TabularDataParser, abc.ABC):
    """
    Default CRF parser that parses the most common format of CRF files
    """

    output_dtypes = {
        'source': 'string',
        'RA': 'Float64',
        'DEC': 'Float64',
        'sig_RA': 'Float64',
        'sig_DEC': 'Float64',
        'correlation': 'Float64',
        'w_mean_mjd': 'datetime64[ns]',
        'first_mjd': 'datetime64[ns]',
        'last_mjd': 'datetime64[ns]',
        'num_sessions': 'Int64',
        'num_delays': 'Int64',
        'num_rates': 'Int64',
        'analyst_center': 'string',
        'source_file': 'string'
    }

    input_columns = ['source', 'alt_name' ,
                     'RA_hour', 'RA_min', 'RA_sec',
                     'DEC_degree', 'DEC_arcmin', 'DEC_arcsec',
                     'sig_RA', 'sig_DEC', 'correlation',
                     'w_mean_mjd', 'first_mjd', 'last_mjd','num_sessions', 'num_delays', 'num_rates'
                     ]

    def is_data_line(self, line: str) -> bool:
        line = line.strip()
        return not line.startswith('%') and not line.startswith('#')

    def transform_columns(self, df: pd.DataFrame):
        df.columns = self.input_columns
        return df

    def hms_to_degrees(self, hour: Union[float, str], minute: Union[float, str], second: Union[float, str]) -> float:
        """
        Converts hours-minutes-seconds of angles to degrees

        Parameters
        ----------
        hour: angle hour
        minute: angle minute
        second: angle second

        Returns
        -------
        total angle in degrees

        """
        hour, minute, second = float(hour), float(minute), float(second)
        return hour * 15 + minute / 4 + second / 240

    def DEC_to_degrees(self, degree: Union[float, str], arcminute: Union[float, str], arcsecond: Union[float, str]) -> float:
        """
        converts declination of degrees, arcminutes and arcseconds to a total angle in degrees
        Parameters

        ----------
        degree: degree
        arcminute: declination arcminute
        arcsecond: declination in arcseconds

        Returns
        -------
        total angle in degrees

        """
        degree, arcminutes, arcseconds = float(degree), float(arcminute), float(arcsecond)
        return degree + arcminutes / 60 + arcseconds / 3600

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        df = self.transform_columns(df)


        df['RA'] = df.apply(lambda row: self.hms_to_degrees(
            row['RA_hour'], row['RA_min'], row['RA_sec']
        ), axis=1)

        df['DEC'] = df.apply(lambda row: self.DEC_to_degrees(
            row['DEC_degree'], row['DEC_arcmin'], row['DEC_arcsec']
        ), axis=1 )

        df.drop(columns=['RA_hour', 'RA_min', 'RA_sec',
                         'DEC_degree', 'DEC_arcmin', 'DEC_arcsec'], inplace=True)

        df['w_mean_mjd'] = df['w_mean_mjd'].apply(lambda d: mjd_to_datetime(d))
        df['first_mjd'] = df['first_mjd'].apply(lambda d: mjd_to_datetime(d))
        df['last_mjd'] = df['last_mjd'].apply(lambda d: mjd_to_datetime(d))
        df['source'] = df['source'].str.lower()
        df['analyst_center'] = self.file_url.split('/')[-1][:3]
        return df


class ASIParserCRF(DefaultCRFParser):
    """
    CRF implementation of (potentially older) CRF files from the asi analyst center
    """
    def line_to_values(self, line: str):
        values = split_and_strip(None, line)

        if len(values) != len(self.input_columns):
            values.insert(1, '') #If there was no alt name, insert ''

        return values

    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#')


class AUSParserCRF(DefaultCRFParser):
    """
    CRF implementation of (potentially older) CRF files from the aus analyst center
    """
    input_columns = DefaultCRFParser.input_columns + ['EstFlag']

    def is_data_line(self, line: str) -> bool:
        return True


class BKGParserCRF(DefaultCRFParser):
    """
    CRF implementation of (potentially older) CRF files from the bkg analyst center
    """

    input_columns = ['icrf', 'designation', 'source', 'inf',
                     'RA_hour', 'RA_min', 'RA_sec',
                     'DEC_degree', 'DEC_arcmin', 'DEC_arcsec',
                     'sig_RA', 'sig_DEC', 'correlation',
                     'w_mean_mjd', 'first_mjd', 'last_mjd', 'num_sessions', 'num_delays'
                     ]

    def __init__(self, filename: str):
        super().__init__(filename)
        self._found_separators = 0

    def transform_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        df.columns = self.input_columns
        df['num_rates'] = pd.NA
        return df


    def is_data_line(self, line: str) -> bool:
        if '-'*20 in line:
            self._found_separators += 1
            return False
        if self._found_separators < 2:
            return False
        return True


class OPAParserCRF(DefaultCRFParser):
    """
    CRF implementation of (potentially older) CRF files from the opa analyst center
    """

    input_columns = DefaultCRFParser.input_columns + ['est_flag']

    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#')
