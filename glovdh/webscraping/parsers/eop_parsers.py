import json
from abc import ABC
from typing import List, Optional

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import TabularDataParser, split_and_strip, clean_na, mjd_to_datetime, \
    ParserFactory, ParserException


class EopParserFactory(ParserFactory):
    """
    Factory responsible for EOP parser creation
    """

    def get_default_parser(self) -> 'TabularDataParser':
        return DefaultEOPParser(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'TabularDataParser':
        filename = self.url_to_filename(file_url)
        fileheader = ''.join(filecontent.splitlines()[:3])
        if 'eop format version 2.0' in fileheader.lower(): return EOPParser2_0(file_url)
        if 'eop format version 2.1' in fileheader.lower(): return EOPParser2_1(file_url)
        if 'eop format version 2.2' in fileheader.lower(): return EOPParser2_2(file_url)
        if 'eop 3.0' in fileheader.lower():
            return EOPParser3_0(file_url, allow_comment_column=filename.startswith('vie'))
        if 'eop 3.1' in fileheader.lower():
            return EOPParser3_0(file_url, allow_comment_column=True)
        if filename.startswith("bkg"): return BKGParserEOP(file_url)
        if filename.startswith("gsf"): return GSFParserEOP(file_url)
        if filename.startswith("iaa"): return IAAParserEOP(file_url)
        if filename.startswith('opa'): return OPAParserEOP(file_url)
        if filename.startswith('usn'): return USNParserEOP(file_url)

        self.logger.warning(f'Could not determine parser for {filename}. Returning DefaultParser')
        return DefaultEOPParser(file_url)



class DefaultEOPParser(TabularDataParser, ABC):
    """
    default EOP parser used for files that dont have a specific parser
    """


    output_dtypes = {
        'epoch': 'datetime64[ns]',
        'XPO': 'Float64',
        'YPO': 'Float64',
        'UT1UTC': 'Float64',
        'dPSI': 'Float64',
        'dX': 'Float64',
        'dEPS': 'Float64',
        'dY': 'Float64',
        'sig_XPO': 'Float64',
        'sig_YPO': 'Float64',
        'sig_UT1UTC': 'Float64',
        'sig_dPSI': 'Float64',
        'sig_dX': 'Float64',
        'sig_dEPS': 'Float64',
        'sig_dY': 'Float64',
        'wrms': 'Float64',
        'cor_XPO_YPO': 'Float64',
        'cor_XPO_UT1UTC': 'Float64',
        'cor_YPO_UT1UTC': 'Float64',
        'cor_dPDE': 'Float64',
        'nobs': 'Int64',
        'session_id': 'string',
        'XPO_r': 'Float64',
        'YPO_r': 'Float64',
        'lod': 'Float64',
        'dPSI_r': 'Float64',
        'dX_r': 'Float64',
        'dEPS_r': 'Float64',
        'dY_r': 'Float64',
        'sig_XPO_r': 'Float64',
        'sig_YPO_r': 'Float64',
        'sig_lod': 'Float64',
        'sig_dPSI_r': 'Float64',
        'sig_dX_r': 'Float64',
        'sig_dEPS_r': 'Float64',
        'sig_dY_r': 'Float64',
        'network': 'string',
        'analyst_center': 'string',
        'source_file': 'string'
    }

    input_columns = ['epoch', 'XPO', 'YPO', 'UT1UTC', 'dPSI_or_dX', 'dEPS_or_dY', 'sig_XPO', 'sig_YPO', 'sig_UT1UTC',
                     'sig_dPSI_or_dX', 'sig_dEPS_or_dY', 'wrms',
                     'cor_XPO_YPO', 'cor_XPO_UT1UTC', 'cor_YPO_UT1UTC', 'cor_dPDE',
                     'nobs', 'session_id', 'span', 'XPO_r', 'YPO_r',
                     'lod', 'dPSI_or_dX_r', 'dEPS_or_dY_r', 'sig_XPO_r', 'sig_YPO_r',
                     'sig_lod', 'sig_dPSI_or_dX_r', 'sig_dEPS_or_dY_r', 'network']
    
    def __init__(self, file_url: Optional[str]):
        super().__init__(file_url)
        if file_url is not None:
            self.analyst_center = file_url.split('/')[-1][:3].lower()
    
    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        if len(values) == len(self.input_columns):
            return values

        session_index = self.input_columns.index('session_id')
        if len(values) < session_index+1:
            return super().line_to_values(line)

        padded = values[:session_index+1] + [None]*(len(self.input_columns) - session_index - 1)
        return padded


    def is_data_line(self, line: str) -> bool:
        line = line.strip()
        return not line.startswith('%') and not line.startswith('#')

    def transform_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        A hook for subclasses to make transformation on the raw input dataframe

        Parameters
        ----------
        df: a read dataframe

        Returns
        -------
        the transformed dataframe

        """
        df.columns = self.input_columns
        return df

    def _fix_eops_eoxy_columns(self, df: pd.DataFrame):
        """
        Since this parser class is used for both eops/eoxy and eopi files,
        the input columns have to be different. In the resulting dataframe
        we add both eops/eoxy and eopi columns, but one of the groups are filled
        with null values

        Parameters
        ----------
        df: the dataframe that is being built

        Returns
        -------

        """
        # remark: by default we assume eopi files are like eoxy files regarding column types,
        # tho eopi xy columns are usually(?) not estimated
        filename = self.file_url.split('/')[-1]
        for col_pattern in ['{col}', 'sig_{col}', '{col}_r', 'sig_{col}_r']:
            if 'eops' in filename and 'eoxy' not in filename:
                col_name_x = col_pattern.format(col='dPSI')
                col_name_y = col_pattern.format(col='dEPS')
                col_name_not_used_x = col_pattern.format(col='dX')
                col_name_not_used_y = col_pattern.format(col='dY')
            elif (('eopi' in filename or 'eoxy' in filename)
                  and 'eops' not in filename):
                col_name_x = col_pattern.format(col='dX')
                col_name_y = col_pattern.format(col='dY')
                col_name_not_used_x = col_pattern.format(col='dPSI')
                col_name_not_used_y = col_pattern.format(col='dEPS')
            else:
                raise ValueError(f'Could not determine if its eoxy or eops type columns')

            df[col_name_x] = df[col_pattern.format(col=f'dPSI_or_dX')]
            df[col_name_y] = df[col_pattern.format(col=f'dEPS_or_dY')]
            df[col_name_not_used_x] = pd.NA
            df[col_name_not_used_y] = pd.NA

    def _network_to_json(self, network: Optional[str]) -> str:
        """
        Converts a network string to a list of string that is dumped as json
        >>>_network_to_json('WzHk')
        "[\"Wz\", \"Hk\"]"
        >>

        Parameters
        ----------
        network: optinal network string

        Returns
        -------
        json representation of the string

        """
        if pd.isna(network):
            return json.dumps([])
        for char in ['(', ')', '[', ']', '-', ' ','\t']:
            network = network.replace(char, '')
        network = network.strip()

        if len(network) % 2 != 0:
            raise ParserException(f'Found invalid network {network} in file: {self.file_url}')
        codes = []

        for i in range(0, len(network), 2):
            code = network[i:i + 2].capitalize()
            codes.append(code)

        return json.dumps(codes)

    def _create_dataframes(self, rows: List[List[str]]) -> List[pd.DataFrame]:
        session_results = pd.DataFrame(rows, columns=self.input_columns)

        session_results = self.postprocess_dataframe(session_results)
        clean_na(session_results)

        session_results['source_file'] = self.file_url
        self.cast_to_target_types(session_results)

        analyst_centers_df = pd.DataFrame(data=[(self.analyst_center, self.file_url)],
                                          columns=['code', 'source_file'])

        return [analyst_centers_df, session_results[list(self.output_dtypes.keys())]]

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        df = self.transform_columns(df)

        df['analyst_center'] = self.analyst_center
        clean_na(df)

        df['epoch'] = df['epoch'].apply(lambda d: mjd_to_datetime(d))
        df['nobs'] = df['nobs'].astype(float).astype(int)
        self._fix_eops_eoxy_columns(df)
        df['network'] = df['network'].apply(lambda network: self._network_to_json(network))
        df['session_id'] = df['session_id'].str.lower()

        return df.reset_index(drop=True)


class BKGParserEOP(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOP files from the bkg analyst center
    """
    def _parse_to_older_version(self, line: str) -> List:
        # like bkg2020a
        values = split_and_strip(None, line)
        ut_apriori_idx = 19
        del values[ut_apriori_idx]
        network = values[20 - 1]
        del values[20 - 1]
        values += [None] * 10
        values += [network]
        return values

    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        if len(values) != len(self.input_columns):
            values = self._parse_to_older_version(line)
        return values

    def is_data_line(self, line: str):
        return not line.startswith('  ')


class GSFParserEOP(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOP files from the gsf analyst center
    """

    def transform_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        df = super().transform_columns(df)
        df.replace('-0', pd.NA, inplace=True)
        return df

    def is_data_line(self, line: str):
        return line and not line.startswith('#')


class IAAParserEOP(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOP files from the iaa analyst center
    """
    def __init__(self, filename: str):
        super().__init__(filename)

    def transform_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        df = super().transform_columns(df)
        df.replace('-0', pd.NA, inplace=True)
        return df

    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        session_id_index = self.input_columns.index('session_id')
        network = values[-1]
        values = values[:session_id_index + 1]
        values += [None] * (len(self.input_columns) - session_id_index - 1)

        network_index = self.input_columns.index('network')
        values[network_index] = network

        return values

    def is_data_line(self, line: str):
        return not line.startswith('#')


class OPAParserEOP(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOP files from the opa analyst center
    """

    input_columns = ['epoch', 'XPO', 'YPO', 'UT1UTC', 'dPSI_or_dX', 'dEPS_or_dY', 'sig_XPO', 'sig_YPO', 'sig_UT1UTC',
     'sig_dPSI_or_dX', 'sig_dEPS_or_dY',
     'cor_XPO_YPO', 'cor_XPO_UT1UTC', 'cor_YPO_UT1UTC', 'cor_dPDE',
     'nobs', 'session_id', 'span', 'XPO_r', 'YPO_r',
     'lod', 'dPSI_or_dX_r', 'dEPS_or_dY_r', 'sig_XPO_r', 'sig_YPO_r',
     'sig_lod', 'sig_dPSI_or_dX_r', 'sig_dEPS_or_dY_r', 'wrms', 'network']

    def _parse_to_older_version(self, line: str):
        values = split_and_strip(None, line)
        older_version_database_col_idx = 11
        del values[older_version_database_col_idx]
        return values

    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        if len(values) != len(self.input_columns):
            values = self._parse_to_older_version(line)
        return values

    def transform_columns(self, df: pd.DataFrame):
        df = super().transform_columns(df)
        df.replace('-0', pd.NA, inplace=True)
        return df

    def is_data_line(self, line: str):
        return not line.startswith('#')


class USNParserEOP(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOP files from the usn analyst center
    """

    def transform_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        df = super().transform_columns(df)
        df.replace('-0', pd.NA, inplace=True)
        return df

    def is_data_line(self, line: str):
        return not line.startswith('#')


class EOPParser2_0(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOPv2.0 files
    """

    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        values = values + [None]  # No network column in version 2.0
        return values

    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#')


class EOPParser2_1(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOPv2.1 files
    """
    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#')


class EOPParser2_2(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOPv2.2 files
    """
    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#')


class EOPParser3_0(DefaultEOPParser):
    """
    EOP implementation of (potentially older) EOPv3.0 files
    """
    def __init__(self, filename: str, allow_comment_column=False):
        """

        Parameters
        ----------
        filename: filename
        allow_comment_column: weather to allow for comments at the end of the lines while parsing
        """
        super().__init__(filename)
        self.reading_data_block = False
        self.allow_comment_column = allow_comment_column

    def line_to_values(self, line: str):
        values = split_and_strip(None, line)
        if self.allow_comment_column:
            values = values[:len(self.input_columns)]
        return values

    def is_data_line(self, line: str) -> bool:
        if line.strip() == '+DATA':
            self.reading_data_block = True
            return False
        if line.strip() in ['-DATA', '+HEADER', '-HEADER'] or line.startswith('%'):
            self.reading_data_block = False

        return self.reading_data_block and not line.startswith('#')
