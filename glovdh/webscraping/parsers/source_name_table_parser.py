from typing import List

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import TabularDataParser, split_and_strip, ParserFactory

class SourceNameTableParserFactory(ParserFactory):
    """
    Factory responsible for Source Name table parser creation
    """

    def get_default_parser(self) -> 'TabularDataParser':
        return SourceNameTableParser(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'TabularDataParser':
        return SourceNameTableParser(file_url)


class SourceNameTableParser(TabularDataParser):
    output_dtypes = {
        'ivs_name': 'string',
        'j2000_name_long': 'string',
        'j2000_name_short': 'string',
        'iers_name': 'string',
        'source_file': 'string'
    }

    input_columns = ['ivs_name', 'j2000_name_long', 'j2000_name_short', 'iers_name']

    def line_to_values(self, line: str) -> List:
        values = split_and_strip(None, line)
        return values[:4]

    def is_data_line(self, line: str) -> bool:
        return not line.startswith('#') and not line.startswith('=')

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        df.columns=self.input_columns
        df.replace('-', pd.NA, inplace=True)
        return df
