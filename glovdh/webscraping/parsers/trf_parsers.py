import datetime
from typing import Optional

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import split_and_strip, TabularDataParser, ParserFactory
class TRFParserFactory(ParserFactory):
    """
    Factory responsible for Master parser creation
    """

    def get_default_parser(self) -> 'TabularDataParser':
        return TRFParser(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'TabularDataParser':
        return TRFParser(file_url)


class TRFParser(TabularDataParser):
    """
    default TRF parser
    """

    DATA_BLOCK_IDENTIFIER = 'SOLUTION/ESTIMATE'
    SITE_MAPPING_BLOCK_IDENTIFIER = 'SITE/ID'
    EPOCH_MAPPING_BLOCK_IDENTIFIER = 'SOLUTION/EPOCHS'

    input_columns = ['index', 'type', 'station_code', 'pt', 'solution_version', 'ref_epoch',
                     'unit', 's', 'estimate', 'error']

    output_dtypes = {
        'station': 'string',
        'ref_epoch': 'datetime64[ns]',
        'epoch_start': 'datetime64[ns]',
        'epoch_end': 'datetime64[ns]',
        'solution_version': 'int',
        'estimate_stax': 'Float64',
        'estimate_stay': 'Float64',
        'estimate_staz': 'Float64',
        'estimate_velx': 'Float64',
        'estimate_vely': 'Float64',
        'estimate_velz': 'Float64',
        'error_stax': 'Float64',
        'error_stay': 'Float64',
        'error_staz': 'Float64',
        'error_velx': 'Float64',
        'error_vely': 'Float64',
        'error_velz': 'Float64',
        'analyst_center': 'string',
        'source_file': 'string'
    }

    def __init__(self, filename: Optional[str]):
        super().__init__(filename)

        self.reading_data_block = False
        self.reading_site_mapping_block = False
        self.reading_epoch_mapping_block = False
        self.site_mapping_rows = []
        self.epoch_mapping_rows = []

    def is_data_line(self, line: str) -> bool:
        if line == '+' + self.SITE_MAPPING_BLOCK_IDENTIFIER:
            self.reading_site_mapping_block = True
            self.reading_data_block = False
            self.reading_epoch_mapping_block = False

        if line == '-' + self.SITE_MAPPING_BLOCK_IDENTIFIER:
            self.reading_data_block = False
            self.reading_site_mapping_block = False
            self.reading_epoch_mapping_block = False

        if self.reading_site_mapping_block:
            self.site_mapping_rows.append(line)

        if line == '+' + self.EPOCH_MAPPING_BLOCK_IDENTIFIER:
            self.reading_data_block = False
            self.reading_data_block = False
            self.reading_epoch_mapping_block = True

        if line == '-' + self.EPOCH_MAPPING_BLOCK_IDENTIFIER:
            self.reading_data_block = False
            self.reading_data_block = False
            self.reading_epoch_mapping_block = False

        if self.reading_epoch_mapping_block:
            self.epoch_mapping_rows.append(line)

        if line == '+' + self.DATA_BLOCK_IDENTIFIER:
            self.reading_data_block = True
            self.reading_site_mapping_block = False
            self.reading_epoch_mapping_block = False
            return False

        if line == '-' + self.DATA_BLOCK_IDENTIFIER:
            self.reading_data_block = False
            self.reading_data_block = False
            self.reading_epoch_mapping_block = False

        return self.reading_data_block and not line.startswith('*')

    def _epoch_to_date(self, epoch: str) -> Optional[datetime.datetime]:
        """
        converts string epochs to datetime object. example: 92:266:122
        Parameters
        ----------
        epoch. epoch string

        Returns
        -------
        datetime object
        """
        try:
            yy, doy, seconds = map(int, epoch.split(':'))

            century = 1900 if yy > 70 else 2000
            year = century + yy

            date = (datetime.datetime(year, 1, 1, tzinfo=datetime.timezone.utc)
                    + datetime.timedelta(days=(doy - 1), seconds=seconds))
            return date
        except ValueError:
            return None

    def _add_solution_version_windows(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Versions the trf solutions in the epoch section of the file, converts time-sensitive columns
        to datetime objects

        Parameters
        ----------
        df: dataframe that is being built

        Returns
        -------
        modified dataframe
        """
        self.epoch_mapping_rows = self.epoch_mapping_rows[1:]
        mapping = dict()
        for line in self.epoch_mapping_rows:
            station_code, _, version, _, start, end, _ = split_and_strip(None, line)
            mapping[(station_code, version)] = (start, end)
        try:
            df[['epoch_start', 'epoch_end']] = df.apply(lambda row:
                                                        mapping[(row['station_code'], row['solution_version'])],
                                                        axis=1, result_type="expand")
            df['epoch_start'] = df['epoch_start'].apply(lambda ep: self._epoch_to_date(ep))
            df['epoch_end'] = df['epoch_end'].apply(lambda ep: self._epoch_to_date(ep))
            df['ref_epoch'] = df['ref_epoch'].apply(lambda ep: self._epoch_to_date(ep))
            return df
        except KeyError as e:
            self.logger.info(f'Failed to Parse {self.file_url} - Could not match TRF Solution versions with epochs.'
                             f' Versions (sbin) between {self.EPOCH_MAPPING_BLOCK_IDENTIFIER} and {self.SITE_MAPPING_BLOCK_IDENTIFIER}'
                             f' MIGHT not match and therefore the file is invalid.')
            raise e

    def _translate_station_codes(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Translate station codes based on a file section in trf files

        Parameters
        ----------
        df: parsed dataframe

        Returns
        -------
        modified dataframe
        """
        self.site_mapping_rows = self.site_mapping_rows[1:]  # drop header

        name_mapping = dict()

        for line in self.site_mapping_rows:
            values = split_and_strip(None, line)
            station_code = values[0]
            station_name = values[4]
            name_mapping[station_code] = station_name

        df['station'] = df['station_code'].apply(lambda code: name_mapping[code])
        return df

    def postprocess_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        df.columns = self.input_columns

        df = df[df.type.isin(['STAX', 'STAY', 'STAZ', 'VELX', 'VELY', 'VELZ'])]

        df.drop(columns=['index', 'pt', 'unit', 's'], inplace=True)
        df = df \
            .pivot(index=['station_code', 'ref_epoch', 'solution_version'], columns='type',
                   values=['estimate', 'error']) \
            .reset_index()
        df.columns = ["_".join([c for c in tup if c]).lower() for tup in
                      df.columns.to_flat_index()]

        df = self._add_solution_version_windows(df)
        df = self._translate_station_codes(df)
        df['analyst_center'] = self.file_url.split('/')[-1][:3]
        for col in filter(lambda key: self.output_dtypes[key] == 'Float64', self.output_dtypes.keys()):
            df[col] = df[col].str.replace('D+', 'e+')
            df[col] = df[col].str.replace('D-', 'e-')

        return df
