import abc
import re
from datetime import datetime
from typing import Optional

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import FactoryException, TabularDataParser, split_and_strip, ParserFactory
from glovdh_ord.glovdh.webscraping.consts import INTENSIVE_SESSION, NORMAL_SESSION


class MasterFileParserFactory(ParserFactory):
    """
    Factory responsible for Master parser creation
    """

    def get_default_parser(self) -> 'TabularDataParser':
        return MasterFileParser1_0(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'TabularDataParser':
        filename = self.url_to_filename(file_url)
        #in 2024 feb, every master file has been uploaded with v2
        #if 'master file format version 1.0' in filecontent.lower():
            #return MasterFileParser1_0(file_url)
        if 'master file format version 2.0' in filecontent.lower():
            return MasterFileParser2_0(file_url)

        raise FactoryException(f'Could not identify master file version: {filename}')


class MasterFileParser(TabularDataParser, abc.ABC):
    output_dtypes = {
        'program': 'string',
        'time_start': 'datetime64[ns]',
        'time_end': 'datetime64[ns]',
        'session_id': 'string',
        'stations': 'string',
        'dbc': 'string',
        'operation_center': 'string',
        'corr': 'string',
        'subm': 'string',
        'session_type': 'string',
        'source_file': 'string'
    }

    SEPERATOR = '-' * 50

    def __init__(self, file_url: Optional[str]) -> None:
        TabularDataParser.__init__(self, file_url)
        if file_url is not None:
            self.year = self._year_from_filename(file_url.split('/')[-1])

        self.read_first_separator = False

    def _year_from_filename(self, filename: str)->int:
        """
        >>>_year_from_filename('master20')
        2000

        >>>_year_from_filename('master1999-in')
        1999


        Parameters
        ----------
        filename: a masterfile filename like master20 or master1999-int

        Returns
        -------
        The year of the master file
        """

        filename_regex = 'master(\d*)(-int)?.txt'
        year_start_idx, year_end_idx = re.match(filename_regex, filename).regs[1]
        year_str = filename[year_start_idx:year_end_idx]
        if len(year_str) == 2:
            if int(year_str) < 23:
                return int(year_str) + 2000
            return int(year_str) + 1900
        elif len(year_str) == 4:
            return int(year_str)
        raise ValueError('Could not determine year from filename')

    def is_data_line(self, line: str) -> bool:
        if self.SEPERATOR in line:
            self.read_first_separator = True
            return False

        return self.read_first_separator and not line.startswith('#')

    @abc.abstractmethod
    def _transform_time_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        A hook to transform all time related columns in a master file parser

        Parameters
        ----------
        df: The dataframe that is being built

        Returns
        -------
        The modified dataframe
        """
        raise NotImplementedError

    def _clean_stations(self, stations: str) -> str:
        """
        A function to preprocess station file list found in master files.
        Does also checks if the string looks okay

        >>>_process_station_list("HkWzHg")
        "HkWzHg"

        >>> _process_station_list("HkWzHg-Kk")
        "HkWzHg"

        >>>_process_station_list("HkWzHgO")
        Traceback (most recent call last):
            ...
        AssertionError: HkWzHgO must be even length

        >>>_process_station_list("hkWzHg")
        Traceback (most recent call last):
            ...
        AssertionError: hkWzHg first letters must be capital or numeric

        >>>_process_station_list("hkWzHg")
        Traceback (most recent call last):
            ...
        AssertionError:  hKWzHg second letters must be lower or numeric'


        Parameters
        ----------
        stations: station list string

        Returns
        -------
        The cleaned string
        """

        stations = stations.split('-')[0].strip()
        assert len(stations) % 2 == 0, f'{stations} must be even length'
        assert all([stations[idx].isupper() or stations[idx].isnumeric()
                    for idx in range(len(stations)) if idx % 2 == 0]), f'{stations} first letters must be capital or numeric'
        assert all([stations[idx].islower() or stations[idx].isnumeric()
                    for idx in range(len(stations)) if idx % 2 == 1]),  f'{stations} second letters must be lower or numeric'

        return stations

    def _process_stations(self, df: pd.DataFrame):
        """
        transforms stations column to a clean version

        Parameters
        ----------
        df: dataframe that is being built

        """
        df['stations'] = df['stations'].apply(lambda s: self._clean_stations(s))

    def _is_intensive(self) -> bool:
        filename = self.file_url.split('/')[-1]
        return bool(re.match('master(\d*)-int.txt', filename))

    def postprocess_dataframe(self, df: pd.DataFrame):

        self._transform_time_columns(df)
        self._process_stations(df)
        df['session_id'] = df['session_id'].str.lower()
        df['session_type'] = INTENSIVE_SESSION if self._is_intensive() else NORMAL_SESSION
        return df

    def line_to_values(self, line: str):
        if line.startswith('|'):
            line = line[1:]
        if line.endswith('|'):
            line = line[:-1]

        values = split_and_strip('|', line)

        return values


class MasterFileParser1_0(MasterFileParser):
    input_columns = ['program', 'session_id', 'date', 'doy', 'time', 'dur',
                     'stations', 'operation_center', 'corr', 'status', 'pf', 'dbc', 'subm', 'del',
                     'mk4']

    def _duration_to_timedelta(self, duration: str):
        """
        Converts durations to timedelta objects

        Parameters
        ----------
        duration: time duration in a string in hours

        Returns
        -------
        timedelta object

        """
        if duration.strip() == '':
            return pd.to_timedelta(f'{0} days {0}:{0}:00')
        days = int(float(duration)) // 24
        hours = int(float(duration)) - days * 24
        minutes = 0
        return pd.to_timedelta(f'{days} days {hours}:{minutes}:00')

    def _transform_time_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        df['time'] = df['time'].apply(lambda t: '00:00' if t.strip() == '' else t)
        df['time_start'] = df.apply(
            lambda row: datetime.strptime(f"{self.year} {row['date']} {row['time']}"
                                          , '%Y %b%d %H:%M'),
            axis=1
        )

        df['dur'] = df['dur'].apply(lambda d: self._duration_to_timedelta(d))

        df['time_end'] = df['time_start'] + df['dur']

        return df


class MasterFileParser2_0(MasterFileParser):
    input_columns = ['program', 'date', 'session_id', 'doy', 'time', 'dur', 'stations',
                     'operation_center', 'corr', 'status', 'dbc', 'subm', 'del']

    SEPERATOR_LENGTH = 140

    def _duration_to_timedelta(self, duration: str):
        """
        Converts durations to timedelta objects

        Parameters
        ----------
        duration: time duration in a string in hours

        Returns
        -------
        timedelta object
        """

        if duration == '':
            return pd.to_timedelta(f'{0} days {0}:{0}:00')
        days = int(duration.split(':')[0]) // 24
        hours = int(duration.split(':')[0]) - days * 24
        minutes = int(duration.split(':')[1])
        return pd.to_timedelta(f'{days} days {hours}:{minutes}:00')

    def _transform_time_columns(self, df: pd.DataFrame):
        df['date'] = pd.to_datetime(df.apply(lambda row: row['date'] + ' ' + row['time']
                                             , axis=1
                                             ),
                                    utc=True
                                    )

        df['dur'] = df['dur'].apply(lambda d: self._duration_to_timedelta(d))

        df['time_start'] = df['date']
        df['time_end'] = df['date'] + df['dur']

        return df
