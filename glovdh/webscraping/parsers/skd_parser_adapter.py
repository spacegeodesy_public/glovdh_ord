from typing import Tuple, Optional

import pandas as pd

from glovdh_ord.glovdh.webscraping.parsers.common import Parser, ParserFactory, clean_na, ParserException
from glovdh_ord.glovdh.webscraping.parsers.skd.skd import SKDParser


class SKDParserFactory(ParserFactory):
    """
    Factory responsible for SKD parser creation
    """

    def get_default_parser(self) -> 'SKDParserAdapter':
        return SKDParserAdapter(None)

    def get_file_parser(self, file_url: str, filecontent: str) -> 'SKDParserAdapter':
        return SKDParserAdapter(file_url)


class SKDParserAdapter(Parser):
    """
    An adapter to a third party skd parser
    """

    source_stats_output_dtypes = {
        'session_id': 'string',
        'ivs_name': 'string',
        'iers_name': 'string',
        'scans': 'int',
        'observations': 'int',
        'source_file': 'string'
    }

    station_stats_output_dtypes = {
        'session_id': 'string',
        'station': 'string',
        'scans': 'int',
        'observations': 'int',
        'source_file': 'string'
    }

    baseline_stats_output_dtypes = {
        'session_id': 'string',
        'station1': 'string',
        'station2': 'string',
        'observations': 'int',
        'source_file': 'string'
    }

    def __init__(self, file_url: Optional[str]):
        super().__init__()
        self.file_url = file_url
        if file_url is not None:
            self.session_name = file_url.split('/')[-1].replace('.skd', '')

    def get_empty(self) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        sources = pd.DataFrame(columns=list(self.source_stats_output_dtypes.keys()))
        stations = pd.DataFrame(columns=list(self.station_stats_output_dtypes.keys()))
        baselines = pd.DataFrame(columns=list(self.baseline_stats_output_dtypes.keys()))
        return sources, stations, baselines

    def to_dataframes(self, filecontent: str) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        assert self.session_name is not None
        try:
            skd = SKDParser()
            skd.parse(filecontent)

            scans_per_source = {s: s.nscans for s in skd.sources.sources if s.nscans > 0}
            obs_per_source = {s.name: s.nobs for s in skd.sources.sources if s.nscans > 0}
            scans_per_station = {s.name: s.nscans for s in skd.stations.stations if s.nscans > 0}
            obs_per_station = {s.name: s.nobs for s in skd.stations.stations if s.nscans > 0}
            obs_per_baseline = skd.baseline_obs

            source_table_values = [(self.session_name, source.name if source.name else None, source.altname, scans,
                                    obs_per_source[source.name])
                                   for source, scans in scans_per_source.items()]

            source_df = pd.DataFrame(source_table_values,
                                     columns=['session_id', 'iers_name', 'ivs_name', 'scans', 'observations'])
            source_df['source_file'] = self.file_url
            source_df['iers_name'] = source_df['iers_name'].apply(lambda n: None if n == '' else n)
            source_df['ivs_name'] = source_df['ivs_name'].apply(lambda n: None if n == '' else n)
            source_df = source_df.astype(self.source_stats_output_dtypes)

            station_table_values = [(self.session_name, station, scans, obs_per_station[station])
                                    for station, scans in scans_per_station.items()]

            station_df = pd.DataFrame(station_table_values, columns=['session_id', 'station', 'scans', 'observations'])
            station_df['source_file'] = self.file_url
            station_df = station_df.astype(self.station_stats_output_dtypes)

            baseline_table_values = []
            for baseline, nobs in obs_per_baseline.items():
                if '-VLBA' in baseline:
                    baseline = baseline.replace('-VLBA', '_VLBA')
                station1, station2 = baseline.split('-')
                station1 = station1.replace('_VLBA', '-VLBA')
                station2 = station2.replace('_VLBA', '-VLBA')
                values = (self.session_name, station1, station2, nobs)
                baseline_table_values.append(values)

            baseline_df = pd.DataFrame(baseline_table_values,
                                       columns=['session_id', 'station1', 'station2', 'observations'])
            baseline_df['source_file'] = self.file_url
            baseline_df = baseline_df.astype(self.baseline_stats_output_dtypes)

            clean_na(source_df)
            clean_na(station_df)
            clean_na(baseline_df)
            return source_df, station_df, baseline_df
        except Exception as e:
            raise ParserException(f'{self.__class__.__name__}: {str(e)}')

