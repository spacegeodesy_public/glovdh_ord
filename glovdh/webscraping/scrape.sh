#!/bin/bash

cd /home/wwwglovdh || exit

source private/venv/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/wwwglovdh/public
cd public || exit

python glovdh_ord/scrape.py
touch wwwglovdh.wsgi
