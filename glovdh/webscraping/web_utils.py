import dataclasses
import gzip
import io
import logging
from collections import defaultdict
from typing import List, Optional, Dict

import pandas as pd
import requests

from glovdh_ord.glovdh.webscraping.parsers.skd.jdutil import datetime

logger = logging.getLogger(__name__)


class FileDownloadException(Exception):
    pass


@dataclasses.dataclass(frozen=True)
class FileMetadata:
    """
    Utility class to store the last known modification date and file url
    of a file on the internet
    """
    file_url: str
    last_modified: datetime


class Downloader:
    """
    Downloader interface, mainly for ease of mocking in tests
    """
    def download_content(self, url: str) -> str:
        raise NotImplementedError


class InternetDownloader(Downloader):
    """
    This class is responsible for downloading content from the internet.
    """
    def __init__(self, max_retries: int = 3):
        self.max_retries = max_retries

    def _download(self, url: str, retries: int = 3) -> str:
        """
        Tries to download a file from the given url. Automatically decompresses
        a file if it detects that its a binary one. Throws FileDownloadException
        if retries are exhausted

        Parameters
        ----------
        url: url of the file
        retries: retries to download the file from the internet

        Returns
        -------
        read filecontent

        """
        try:
            logger.debug(f'Downloading content {url}...'.format(url=url))
            r = requests.get(url, timeout=20, stream=True)
            r.raise_for_status()
            logger.debug(f'Downloading {url} finished')
            if not is_compressed_file(url.split('/')[-1]):
                return r.content.decode('utf-8')

            with gzip.GzipFile(fileobj=io.BytesIO(r.content), mode='rb') as gz_file:
                try:
                    return gz_file.read().decode('utf-8', errors='ignore')
                except UnicodeDecodeError:
                    logger.warning(f'Could not decode {url}, decoding with errors ignored!')
                    gz_file.seek(0)
                    return gz_file.read().decode('utf-8', errors='ignore')
        except Exception as e:
            if retries > 0:
                logger.warning(f'Retrying download for {url}. ({retries} left.)')
                return self._download(url, retries=retries - 1)
            raise FileDownloadException from e

    def download_content(self, url: str) -> str:
        return self._download(url, self.max_retries)


def is_compressed_file(filename: str) -> bool:
    """
    Function to check if a file is compressed or not.
    Currently only .gz files are used that are compressed. File
    extensions can be for example .gz.crf that does not end with .gz


    Parameters
    ----------
    filename: name of the file

    Returns
    -------
    weather the file is compressed or not
    """
    return '.gz' in filename


@dataclasses.dataclass
class ParsingSummary:
    target_identifier: str
    received_models: List[str] = dataclasses.field(default_factory=list)
    received_urls: List[int] = dataclasses.field(default_factory=list)
    parsed_records: Dict[str, int] = dataclasses.field(default_factory=lambda: defaultdict(int))
    discarded_records: Dict[str, int] = dataclasses.field(default_factory=lambda: defaultdict(int))
    failed_file_loadings: List[str] = dataclasses.field(default_factory=list)
    run_successful = True

    def to_dict(self):
        return {
            'target_identifier': self.target_identifier,
            'received_models': self.received_models,
            'received_urls': self.received_urls,
            'parsed_records': dict(self.parsed_records),
            'discarded_records': dict(self.discarded_records),
            'failed_file_loadings': self.failed_file_loadings,
            'run_successful': self.run_successful,
        }


@dataclasses.dataclass()
class ScrapeTarget:
    """
    A utility class to describe a file that is going through the pipeline.
    It stores the metadata of the file, when it was scraped, resulting dataframes
    and an optional error that was encountered in the process.
    The first encountered error is stored, it is not overwritten
    if an other error is registered.
    """
    file_meta: FileMetadata
    scraping_date: datetime
    result_dfs: List[pd.DataFrame]
    parsing_error: Optional[Exception] = None

    def error_occurred(self, exc: Exception):
        self.parsing_error = self.parsing_error or exc
