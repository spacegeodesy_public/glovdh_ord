import logging
import traceback
from concurrent.futures import ThreadPoolExecutor
from typing import List, Type

from django.db import models, transaction
from django.db.models import Model

from glovdh_ord.glovdh.webscraping.db_handler import DBHandler
from glovdh_ord.glovdh.webscraping.parsers.common import ParserFactory
from glovdh_ord.glovdh.webscraping.scraping_collections import ScrapeTargetCollection
from glovdh_ord.glovdh.webscraping.web_utils import ParsingSummary, Downloader
from glovdh_ord.glovdh.webscraping.parsers.skd.jdutil import datetime
from glovdh_ord.glovdh.webscraping.web_utils import ScrapeTarget


class WebParserAgent:
    """
    An agent that takes is responsible to scrape a collection, orchestrate the
    scraping of files and loading them into the database.
    This class is stateful, meaning it accumulates summaries, so it should be
    newly instantiated for a new collection or a new run.
    """

    def __init__(self, target_coll: ScrapeTargetCollection,
                 parser_factory: ParserFactory,
                 django_models: List[Type[models.Model]],
                 db_handler: DBHandler,
                 downloader: Downloader,
                 num_workers: int = 8,
                 ):
        """
        Parameters
        ----------
        target_coll: target collection that gives file urls to download
        parser_factory: factory for parser creation
        django_models: django models that should be loaded with the resulting dataframes from the parsers.
        The resulting dataframes from the parsers should have the SAME ORDER as this list. That is,
        first dataframe will be loaded to the first model, second dataframe will be loaded to the second model etc..
        db_handler: object to communicate with the database
        downloader: used to download file content of urls
        num_workers: How many threads to spawn for parallel execution of processing the files.
        This number of course should be a function of the hardware, BUT A TIGHTER BOTTLENECK is usually
        the maximum number of requests the target urls/host can handle in a given time. If its too many, the used
        scraping IP/Account might get blocked by the overloaded target host.
        """
        self.target_coll = target_coll
        self.django_models = django_models
        self.parser_factory = parser_factory
        self.db_handler = db_handler
        self.downloader = downloader
        self.num_workers = num_workers

        self.logger = logging.getLogger(f'{target_coll.tag}_WebAgent')
        self.summary = ParsingSummary(
            target_identifier=self.target_coll.tag,
            received_models=list(map(lambda m: self._model_name(m), self.django_models))
        )

    def _model_name(self, model: Model) -> str:
        """
        Returns a django model's name

        Parameters
        ----------
        model: django model

        Returns
        -------
        String name of the model
        """
        return model.__name__

    def process_target(self, target: ScrapeTarget, target_idx: int, log_every_100=False) -> ScrapeTarget:
        """
        Takes a target(file), downloads it from the web, gets a parser, and stores the
        parsed dataframes in the ScrapeTarget object.

        Parameters
        ----------
        target: file target to process
        target_idx: target index in the collection
        log_every_100: whether to log every 100th processed file. Mainly for debugging or more verbosity

        Returns
        -------
        The parameter ScrapeTarget object, with resulting dataframes stored in it.
        These dataframes can be empty if an error occured, in this case the error is also
        registered in the object to store.
        """

        if log_every_100 and target_idx % 100 == 0:
            self.logger.info(f'Processing file #{target_idx}')
        file_url = target.file_meta.file_url
        try:
            filecontent = self.downloader.download_content(file_url)
            self.logger.debug(f'Parsing file {file_url}')
            parser = self.parser_factory.get_file_parser(file_url, filecontent)
            result_dfs = parser.to_dataframes(filecontent)
            if any([df.empty for df in result_dfs]):
                self.logger.warning(f'{file_url} was parsed to at least one empty empty dataframe')
            target.result_dfs = result_dfs
        except Exception as e:
            self.logger.warning(f'{type(e)}:{str(e)} for {file_url}')
            result_dfs = self.parser_factory.get_default_parser().get_empty()
            target.result_dfs = result_dfs
            target.error_occurred(e)
        return target

    def parallel_process_targets(self, targets: List[ScrapeTarget]) -> List[ScrapeTarget]:
        """
        Takes a list of target objects and processes them in a parallel fashion for efficiency
        It uses a max_job_time constant that can be maximum the time in seconds devoted for processing a file.


        Parameters
        ----------
        targets

        Returns
        -------

        """
        max_job_time = 30
        with ThreadPoolExecutor(max_workers=self.num_workers) as executor:
            target_results = executor.map(self.process_target, targets, range(len(targets)),
                                          timeout=max_job_time)
        return list(target_results)

    def _get_scrape_targets(self):
        """
        Creates scrape targets from the collection's urls
        Returns
        -------
        The ScrapeTarget lists
        """
        target_files = self.target_coll.get_target_files()
        self.summary.received_urls = [target_file.file_url for target_file in target_files]

        targets = [ScrapeTarget(file_meta=f, scraping_date=datetime.now(), result_dfs=[]) for f in target_files]
        return targets

    def _load_target_results(self, targets: List[ScrapeTarget]):
        """
        Takes targets and loads them into the database. The targets are expected to have
        as many result dataframes stored in them as self.django_models, so they can be
        matched (sensitive to order!).
        The loading uses transactions, so if there is an error while loading any of the dataframes of a target,
        the whole target is aborted, and the error is saved into the database instead under the file record.
        (=no records of the dataframes will be loaded into the database).

        Parameters
        ----------
        targets: scraped targets that are ready to be loaded


        """
        for target in targets:
            try:
                with transaction.atomic():
                    self.db_handler.upsert_sourcefile(target)
                    for result_df, django_model in zip(target.result_dfs, self.django_models):
                        self.summary.parsed_records[self._model_name(django_model)] += len(result_df)
                        records = result_df.to_dict(orient='records')
                        self.logger.info(f'Loading {len(records)} record(s) for '
                                         f'{target.file_meta.file_url} with model {django_model.__name__}')

                        self.db_handler.load_model(records, django_model)
            except Exception as e:
                target.error_occurred(e)
                self.summary.failed_file_loadings.append(target.file_meta.file_url)
                self.db_handler.upsert_sourcefile(target)

    def run(self) -> ParsingSummary:
        """
        Initiates the downloading, scraping, and loading to the database of the collection
        that this object was instantiated with.

        Returns
        -------
        A parsing summary, detailing information about successful and unsuccessful loadings
        """
        try:
            targets = self._get_scrape_targets()
            self.logger.info(f'Archive {self.target_coll.tag} gave {len(targets)} targets to download!')
            if len(targets) > 0:
                self.logger.info(f'Scraping {len(targets)} files with up to {self.num_workers} workers')
                targets = self.parallel_process_targets(targets)

                self.logger.info(f'Loading {len(targets)} files to database')
                self._load_target_results(targets)

        except Exception as e:
            self.logger.error(
                f'{self.__class__.__name__} with {self.target_coll.tag} failed! {e}-{traceback.format_exc()}')
            self.summary.run_successful = False

        self.logger.info('Agent run finished!')

        return self.summary
