import datetime
import itertools
import logging

from collections import defaultdict

from typing import List, T, Type, Dict, Set, Tuple, Optional, Any, Iterable

from django.db import models
from django.db.models import Q, UniqueConstraint

from glovdh_ord.glovdh.models import ScrapedFile, VLBISession, Source, Station, AnalystCenter, SourceStatistic, \
    StationStatistic, BaselineStatistic, SessionType
from glovdh_ord.glovdh.webscraping.web_utils import FileMetadata
from glovdh_ord.glovdh.webscraping.web_utils import ScrapeTarget
from glovdh_ord.glovdh.webscraping.consts import INTENSIVE_SESSION, NORMAL_SESSION, UNKNOWN_STATIONS_WHITELIST


def batched(iterable: List[T], n: int) -> Iterable[List[T]]:
    if n < 1:
        raise ValueError('n must be at least one')
    it = iter(iterable)
    while (batch := list(itertools.islice(it, n))):
        yield batch


class DBHandlerException(Exception):
    pass


class DBHandler:
    """
    A database handler to load and query django models
    """
    def __init__(self):
        self.logger = logging.getLogger('db_handler')
        self.BATCH_SIZE = 4096

    def get_sessions_with_start_dates(self) -> List[Tuple[str, datetime.datetime]]:
        """
        Gets all stations with their start dates

        Returns
        -------
        A list of tuples, consisting of (session_id, start_time_object)
        """
        sessions = (VLBISession.objects
                    .all()
                    .values_list('session_id', 'time_start'))
        return [(sess_id, time_start) for sess_id, time_start in sessions if time_start]

    def get_outdated_file_urls(self, filemetadatas: List[FileMetadata]) -> Set[str]:
        """
        Takes a list of metadatas and returns a set of file urls that are outdated in the database from the
        list.

        Parameters
        ----------
        filemetadatas: file metadata objects describing a file url and a modification date

        Returns
        -------
        File urls that have been outdated in the database compared to the parameter modification dates
        """
        outdated_file_queries = []
        fileurls = []
        for filedata in filemetadatas:
            fileurl = filedata.file_url
            fileurls.append(fileurl)
            outdated_file_queries.append(Q(file_url=fileurl, parsed_date__lt=filedata.last_modified))

        outdated_file_query = Q()
        for q_obj in outdated_file_queries:
            outdated_file_query = outdated_file_query | q_obj

        outdated_file_query = outdated_file_query & Q(file_url__in=fileurls)

        outdated_files = set((ScrapedFile.objects.filter(outdated_file_query)
                              .values_list('file_url', flat=True)))

        return outdated_files

    def get_missing_file_urls(self, files: List[FileMetadata]) -> Set[str]:
        """
        Gets a list of file metadatas, and returns urls that are missing from the database
        compared to the parameter list

        Parameters
        ----------
        files: filemetadata objects describing a url and a modification date

        Returns
        -------
        a set of file urls that are not in the parameter list

        """
        urls = [f.file_url for f in files]
        files_in_db = (ScrapedFile.objects.filter(file_url__in=urls)
                       .values_list('file_url', flat=True))
        missing_files = set(urls) - set(files_in_db)

        return missing_files

    def get_url_last_part(self, url: str) -> str:
        """
        Returns the last part of a url, typically a filename

        Parameters
        ----------
        url: file url like myhost/myfile.txt

        Returns
        -------
        the filename from the url, list myfile.txt

        """
        parts = url.split('/')
        if len(parts) > 1:
            return parts[-1]
        return url

    def upsert_sourcefile(self, file: ScrapeTarget) -> ScrapedFile:
        """
        Takes a file descriptor and upserts it into the database

        Parameters
        ----------
        file: an object describing a file from the scraping process

        Returns
        -------
        The django model ScrapedFile object that has been upserted into the database
        """
        parsed_date = datetime.datetime.now(datetime.timezone.utc)

        err_max_length = ScrapedFile._meta.get_field('error').max_length
        truncate_exc_msg = lambda ex: str(ex)[:err_max_length] if ex else None

        instance = ScrapedFile(
            filename=self.get_url_last_part(file.file_meta.file_url),
            file_url=file.file_meta.file_url,
            last_modified=file.file_meta.last_modified,
            parsed_date=parsed_date,
            error=truncate_exc_msg(file.parsing_error)
        )
        instance.save()
        return instance

    def source_file_field_to_scrapedfile_obj(self, records: List[dict]):
        """
        Takes in a list of dicts as records and changes every source_file field in the records
        to a looked-up ScrapedFile django model Object. The array is modified in-place.
        Parameters
        ----------
        records: A list of records that have source_file keys. If the first record does not
        have a source_file key, its assumed none of the records have it and nothing is modified
        """

        if len(records) == 0 or 'source_file' not in records[0].keys():
            return

        parsed_files = list({r['source_file'] for r in records})
        if not all(isinstance(file, str) for file in parsed_files):
            raise DBHandlerException('Filenames must be strings!')

        files = [ScrapedFile.objects.get(pk=self.get_url_last_part(f))
                 for f in parsed_files]

        lookup = {parsed_file.file_url: parsed_file for parsed_file in files}
        for idx, record in enumerate(records):
            record['source_file'] = lookup[record['source_file']]

    def _assert_source_name_is_valid(self, source_name: Any):
        """
        Asserts that a source name is valid. Raises DBHandlerException is invalid source_name is found

        Parameters
        ----------
        source_name: source name

        """
        if not isinstance(source_name, str):
            raise DBHandlerException(f'Invalid Source Name {source_name}')
        if '?' in source_name:
            raise DBHandlerException(f'Invalid Source Name {source_name}')

    def source_fields_to_source_obj(self, records: List[dict]):
        """
        Takes in a list of dicts as records and changes every field in the records that can be a source
        to a looked-up Source django model Object. The array is modified in-place.
        Parameters
        ----------
        records: A list of records that have any of ['ivs_name', 'iers_name', 'source', 'iau_name'] keys.
        If the first record does not have any of the keys, its assumed none of the records have it
         and nothing is modified
        """

        if not records:
            return
        possible_record_fields = ['ivs_name', 'iers_name', 'source', 'iau_name']
        if all(key not in records[0] for key in possible_record_fields):
            return

        for record in records:
            found_matching_source = False
            for db_field in ['ivs_name', 'iers_name', 'j2000_name_long', 'j2000_name_short']:
                name_matching_query = Q()
                for record_field in possible_record_fields:
                    if record.get(record_field) is None:
                        continue
                    kwargs = {
                        f'{db_field}__iexact': record[record_field]}
                    name_matching_query |= Q(**kwargs)

                match = Source.objects.filter(name_matching_query).all()
                if match:
                    found_matching_source = True
                    if len(match) > 1:
                        rec_projection = {name: record.get(name) for name in possible_record_fields}
                        self.logger.warning(
                            f'Multiple source found for record {rec_projection} in {record["source_file"]}')
                    record['source'] = match[0]
                    break
            if not found_matching_source:
                name = (record.get('ivs_name') or record.get('iers_name')
                        or record.get('source') or record.get('iau_name'))
                self._assert_source_name_is_valid(name)
                self.logger.warning(f'Creating new Source with name {name} from file {record["source_file"]}')
                s = Source(ivs_name=name, iers_name=record.get('iers_name'), source_file=record['source_file'])
                s.save()
                record['source'] = s

            for field in possible_record_fields:
                if field != 'source' and field in record:
                    del record[field]

    def station_field_to_station_obj(self, records: List[Dict]):
        """
        Takes in a list of dicts as records and changes every field in the records that can be a station
        to a looked-up Station django model Object. The array is modified in-place.

        !! If the station is in UNKNOWN_STATIONS_WHITELIST, the record is deleted from the list.
        See documentation for more details

        Parameters
        ----------
        records: A list of records that have any of ['station', 'station1', 'station2'] keys.
        If the first record does not have any of the keys, its assumed none of the records have it
        and nothing is modified.

        """

        if len(records) == 0:
            return

        STATION_KEYS = ['station', 'station1', 'station2']
        if all(key not in records[0].keys() for key in STATION_KEYS):
            return
        station_fields = [key for key in records[0].keys() if key in STATION_KEYS]

        discarded_indices = list()
        for idx, record in enumerate(records):
            for key in station_fields:
                station_name = record[key]
                try:
                    record[key] = Station.objects.get(Q(name__iexact=station_name) |
                                                      Q(code__iexact=station_name))
                except Station.DoesNotExist:
                    if station_name in UNKNOWN_STATIONS_WHITELIST:
                        self.logger.warning(f'Unknown station {station_name} is whitelisted')
                        discarded_indices.append(idx)
                    else:
                        raise DBHandlerException(f"{station_name} station does not exist! file: {record['source_file']}")
        if discarded_indices:
            self.logger.warning(f'Discarding {len(discarded_indices)} records with unknown stations')
            for index in sorted(discarded_indices, reverse=True):
                del records[index]

    def _try_to_find_session_by_date_format(self, session_id: str) -> Optional[VLBISession]:
        """
        Tries to match a session_id to a VLBISession object in the database. Since session_ids have many
        forms, its not guaranteed to find an already existing session for the session_id, in which case
        this function returns None

        Parameters
        ----------
        session_id: session_id string

        Returns
        -------
        A matched VLBISession or None

        """
        try:
            year = int(session_id[:2])
            if year >= 70:
                year += 1900
            else:
                year += 2000
            dt = datetime.datetime.strptime(session_id[2:7], '%b%d', )
            dt = dt.replace(tzinfo=datetime.timezone.utc)
            dt_with_year = dt.replace(year=year)
            dbc = session_id[7:]

            candidates = VLBISession.objects.filter(dbc=dbc, time_start__year=dt_with_year.year,
                                                    time_start__month=dt_with_year.month,
                                                    time_start__day=dt_with_year.day).all()
            if len(candidates) == 0:
                return None
            if len(candidates) == 1:
                return candidates[0]
            else:
                self.logger.info(f'{session_id} was matched with multiple existing sessions. '
                                 f'Assigning it with the longest network')
                candidates = list(sorted(list(candidates), key=lambda s: len(s.network)))

                return candidates[0]

        except:
            return None

    def session_id_field_to_session_obj(self, records: List[Dict]):
        """
        Takes in a list of dicts as records and changes every session_id field
        to a looked-up VLBISession django model Object. The array is modified in-place.

        !!If the session_id is not found in the database, a None object will be its replacement, instead
        of a VLBISession object. (indicating a null foreign key in the database)

        Parameters
        ----------
        records: A list of records that have a session_id key.
        If the first record does not have any of the keys, its assumed none of the records have it
        and nothing is modified.

        """

        if len(records) == 0 or 'session_id' not in records[0].keys():
            return
        assert isinstance(records[0]['source_file'], ScrapedFile)

        unmatched_session_id = 0
        for idx, record in enumerate(records):
            sess_id = record['session_id']
            try:
                record['session_id'] = VLBISession.objects.get(session_id__iexact=sess_id)
            except VLBISession.DoesNotExist:
                sess_id = self._try_to_find_session_by_date_format(sess_id)
                if sess_id is None:
                    unmatched_session_id += 1
                record['session_id'] = sess_id

        if unmatched_session_id:
            self.logger.warning(
                f'Could not identify {unmatched_session_id}/{len(records)} sessions from file: {records[0]["source_file"]}')

    def session_types_to_session_types_obj(self, records: List[dict]):
        """
        Takes in a list of dicts as records and changes every session_type field
        to a looked-up SessionType django model Object. The array is modified in-place.

        !!If a session_type is not found, an SessionType.DoesNotExist error is thrown

        Parameters
        ----------
        records: A list of records that have a session_type key.
        If the first record does not have any of the keys, its assumed none of the records have it
        and nothing is modified.
        """


        if len(records) == 0 or 'session_type' not in records[0]:
            return

        for record in records:
            found=False
            for sess_type in (INTENSIVE_SESSION, NORMAL_SESSION):
                if record['session_type'] == sess_type:
                    record['session_type'] = SessionType.objects.get(name=sess_type)
                    found=True
            if not found:
                raise SessionType.DoesNotExist(f'Session Type: {record["session_type"]} does not exist')
    def analyst_center_field_to_analyst_center_obj(self, records: List[dict]):
        """
        Takes in a list of dicts as records and changes every analyst_center field
        to a looked-up AnalystCenter django model Object. The array is modified in-place.

        !!If a analyst_center is not found, an AnalystCenter.DoesNotExist error is thrown

        Parameters
        ----------
        records: A list of records that have a analyst_center key.
        If the first record does not have any of the keys, its assumed none of the records have it
        and nothing is modified.
        """

        if not records or 'analyst_center' not in records[0]:
            return

        for record in records:
            try:
                record['analyst_center'] = AnalystCenter.objects.get(pk=record['analyst_center'])
            except AnalystCenter.DoesNotExist:
                raise DBHandlerException(f'Analyst Center: {record["analyst_center"]} does not exist')

    def add_foreign_keys(self, records: List[dict], django_model: Type[models.Model]):
        """
        Adds foreign keys to a list of records. Every key in the records that can refer
        to a foreign key in the database is replaced with the corresponding django objects
        as its value. Everything is modified in-place.

        Parameters
        ----------
        records: List of records
        django_model: The django model that is being loaded to the database
        """

        if len(records) == 0:
            return

        if django_model != ScrapedFile:
            self.source_file_field_to_scrapedfile_obj(records)

        if django_model != VLBISession:
            self.session_id_field_to_session_obj(records)

        if django_model != Station:
            self.station_field_to_station_obj(records)

        if django_model != Source:
            self.source_fields_to_source_obj(records)

        if django_model != AnalystCenter:
            self.analyst_center_field_to_analyst_center_obj(records)

        if django_model == VLBISession:
            self.session_types_to_session_types_obj(records)

    def _group_by_source_file(self, records: List[Dict]) -> Dict[ScrapedFile, List[Dict]]:
        """
        Takes a list of records and groups them by sourcefile

        Parameters
        ----------
        records: list of records

        Returns
        -------
        A mapping between ScrapedFile objects and their exclusive subset of records

        """
        source_files = defaultdict(list)
        for record in records:
            source_files[record['source_file']].append(record)

        return source_files

    def _get_update_fields(self, model: Type[models.Model]) -> List[str]:
        """
        Takes a django Model, and returns a list of fieldnames that
        are to be updated when there is an creation conflict in the database (
        record is already present at creation so fields are updated)

        Parameters
        ----------
        model: Django model that is being loaded

        Returns
        -------
        A list of fieldnames of that model that should be updated when the record is already
        in the database at creation

        """
        pk = model._meta.pk.name
        unique_constraints = [c.fields for c in model._meta.constraints if isinstance(c, UniqueConstraint)]

        not_updated_fields = set(itertools.chain.from_iterable(unique_constraints))
        not_updated_fields.update([pk])

        all_fields = [f.name for f in model._meta.fields]
        return list(set(all_fields) - not_updated_fields)

    def load_model(self, records: List[dict], django_model: Type[models.Model]):
        """
        Loads a list of records under a django model to the database

        Parameters
        ----------
        records: list of records
        django_model: django model that should be loaded
        """

        if len(records) == 0:
            return

        self.add_foreign_keys(records, django_model)

        instances = [django_model(**r) for r in records]
        update_fields = self._get_update_fields(django_model)
        django_model.objects.bulk_create(instances,
                                         update_conflicts=True,
                                         update_fields=update_fields)

    def get_sessions_with_missing_skd_file(self) -> List[str]:
        """
        Queries the database for VLBISession that do not have an skd file in the database

        Returns
        -------
        A list of session_ids that are missing their skd files from the database

        """
        sessions_with_skd_files = (BaselineStatistic.objects.values_list('session_id_id', flat=True)
                     .union(StationStatistic.objects.values_list('session_id_id', flat=True),
                            SourceStatistic.objects.values_list('session_id_id', flat=True)))

        sessions_without_skd_files = (VLBISession.objects.values_list('session_id', flat=True)
                                      .difference(sessions_with_skd_files))

        return list(sessions_without_skd_files.all())
