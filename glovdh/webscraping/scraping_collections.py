import abc
import logging
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone, timedelta
import re
from typing import List, Iterable, Tuple, Optional, Dict

from bs4 import BeautifulSoup


from glovdh_ord.glovdh.webscraping.consts import SKD_FILE_URL_TEMPLATE, SKD_FILE_REGEX
from glovdh_ord.glovdh.webscraping.db_handler import DBHandler
from glovdh_ord.glovdh.webscraping.web_utils import FileMetadata, Downloader


def filter_metadatas_with_regex(ls: Iterable[FileMetadata], regex: str) -> List[FileMetadata]:
    """
    Gets filemetadatas and filters their urls against a given regex

    Parameters
    ----------
    ls: list of file metadatas
    regex: regex pattern to filter for

    Returns
    -------
    the filtered list
    """
    return [f for f in ls if re.match(regex, f.file_url)]


class ScrapeTargetCollection(abc.ABC):
    """
    A class representing a collection for scraping.
    When get_target_files() is called, it gives back a list of file descriptors that
    should be scraped from the internet from this collection.
    To create a new scraping collection that is being scraped from the internet
    just implement the only abstract function of this class
    """
    def __init__(self, tag: str, db: DBHandler, downloader: Downloader):
        self.db_handler = db
        self.tag = tag
        self.logger = logging.getLogger(self.__class__.__name__)
        self.downloader = downloader

    @abc.abstractmethod
    def get_target_files(self) -> List[FileMetadata]:
        """
        Returns
        -------
        A list of files that should be scraped from this abstract file collection
        """
        raise NotImplementedError


class FileArchiveTarget(ScrapeTargetCollection):
    """
    A file archive modelling pages
    like https://cddis.nasa.gov/archive/vlbi/ivsproducts/eops/
    that is formatted as a table of filenames with modification dates.

    The HTML structure of the pages matters for getting and scraping the modification dates
    of the files.
    """
    def __init__(self, tag: str, db_handler: DBHandler
                 , file_url_pattern: str,
                 minimum_last_file_update: datetime,
                 archive_url: str,
                 file_url_template: str,
                 downloader: Downloader,
                 urls: Optional[List[str]] = None):
        super().__init__(tag, db_handler, downloader)
        self.file_url_pattern = file_url_pattern
        self.minimum_last_file_update = minimum_last_file_update
        self.archive_url = archive_url
        self.file_url_template = file_url_template
        self.urls=urls

    @staticmethod
    def parse_file_updates_table(html_content: str) -> Dict[str, datetime]:
        """
        Parses the file archive page for file updates

        Parameters
        ----------
        html_content: content of the page

        Returns
        -------
        a mapping between filenames and modification dates
        """
        soup = BeautifulSoup(html_content, features='html5lib')
        results = soup.findAll("div", {
            "class": "archiveItemTextContainer"})

        changes = dict()

        for result in results:
            filename = result.find('a', class_='archiveItemText')['id']

            file_info_text = result.find('span', class_='fileInfo').text.strip()
            file_info_date_match = re.search(r'(\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2})', file_info_text)

            modified_date = file_info_date_match.group(1)
            modified_date = (datetime.strptime(modified_date, '%Y:%m:%d %H:%M:%S')
                             .replace(tzinfo=timezone.utc))

            changes[filename] = modified_date

        return changes

    def get_target_files(self) -> List[FileMetadata]:
        html = self.downloader.download_content(self.archive_url)
        file_updates = self.parse_file_updates_table(html)

        file_metadatas = [FileMetadata(
            self.file_url_template.format(filename=filename),
            modified_date)
            for filename, modified_date in file_updates.items()
        ]

        if self.urls is not None:
            return [f for f in file_metadatas if f.file_url in self.urls]


        file_metadatas = [f for f in file_metadatas if f.last_modified >= self.minimum_last_file_update]
        file_metadatas = filter_metadatas_with_regex(file_metadatas, self.file_url_pattern)

        outdated_files = self.db_handler.get_outdated_file_urls(file_metadatas)
        missing_files = self.db_handler.get_missing_file_urls(file_metadatas)

        lookup = {f.file_url: f for f in file_metadatas}
        files_to_scrape = set()
        files_to_scrape.update([lookup[f] for f in outdated_files])
        files_to_scrape.update([lookup[f] for f in missing_files])

        return list(files_to_scrape)


class StationURLScrapeTarget(ScrapeTargetCollection):
    """
    A collection that represents the station page for scraping VLBI stations.
    A single record is returned as a collection, that represents the file url and
    its last modified date
    """

    CHANGE_LOG_START = '* Updates (most recent at the top): '

    def __init__(self, tag: str, db_handler: DBHandler
                 , file_url: str, downloader: Downloader):
        super().__init__(tag, db_handler, downloader)
        self.file_url = file_url

    def _parse_last_modified(self) -> datetime:
        content = self.downloader.download_content(self.file_url)
        lines = content.splitlines()
        for line_idx, line in enumerate(lines):
            if line.strip() == self.CHANGE_LOG_START.strip():
                most_recent_change_line = lines[line_idx + 1]
                date_pattern = r'\d{4}-\d{2}-\d{2}'
                match = re.findall(date_pattern, most_recent_change_line)[0]
                latest_change_date = datetime.strptime(match, "%Y-%m-%d")
                return latest_change_date.replace(tzinfo=timezone.utc)

        raise ValueError(f'Could not determine last modified date in {self.file_url}')

    def get_target_files(self) -> List[FileMetadata]:

        last_modified = self._parse_last_modified()

        return [FileMetadata(file_url=self.file_url, last_modified=last_modified)]


class SKDFilesTarget(ScrapeTargetCollection):
    """
    A collection that represents different skd files for scraping.
    """

    def __init__(self, tag: str, db_handler: DBHandler, downloader: Downloader,
                 num_workers=8,  urls: Optional[List] = None):
        super().__init__(tag, db_handler, downloader)
        self.file_url_pattern = SKD_FILE_REGEX
        self.file_url_template = SKD_FILE_URL_TEMPLATE
        self.num_workers = num_workers
        self.urls = urls

    def _generate_file_urls(self, session_start_dates: List[Tuple[str, datetime]]) -> List[str]:
        """
        Gets a list of tuples representing (session_ids and their start dates), and
        returns file urls that should be parsed from the list based on the session's start time
        Parameters
        ----------
        session_start_dates: sessions with their start time

        Returns
        -------
        skd file urls, that should be parsed (either recent sessions or near future sessions)
        """
        two_weeks_from_now = datetime.now(timezone.utc) + timedelta(days=14)
        a_year_ago = datetime.now(timezone.utc) - timedelta(days=365)
        urls = []
        for session, start_date in session_start_dates:
            if a_year_ago < start_date < two_weeks_from_now:
                url = self.file_url_template.format(year=start_date.year, session=session)
                urls.append(url)
        return urls

    def _scrape_modified_dates(self, urls: List[str]) -> List[FileMetadata]:
        """
        Takes a list of urls and looks up their modification dates

        Parameters
        ----------
        urls: List of skd file urls

        Returns
        -------
        File metadata objects describing skd files and their modification dates

        """
        self.logger.info(f'Checking modification dates of {len(urls)} schedule files.')
        with ThreadPoolExecutor(max_workers=self.num_workers) as executor:
            max_job_time = 30
            results = executor.map(self._get_last_modified_date, urls,
                                   timeout=max_job_time)
        results = list(results)

        metadatas = []

        for metadata in results:
            if metadata is not None:
                metadatas.append(metadata)
        return metadatas

    def _get_last_modified_date(self, skd_url: str) -> Optional[FileMetadata]:
        """
        Gets a modification dates of a single skd file
        Parameters
        ----------
        skd_url: skd url

        Returns
        -------
        File descriptor if succeeded, None otherwise
        """
        if skd_url.endswith('/'):
            skd_url = skd_url[:-1]
        skd_filename = skd_url.split('/')[-1]
        web_folder = '/'.join(skd_url.split('/')[:-1])

        try:
            html = self.downloader.download_content(web_folder)
            updates = FileArchiveTarget.parse_file_updates_table(html)
            update = [update for filename, update in updates.items() if
                      filename == skd_filename][0]
            return FileMetadata(skd_url, update)
        except IndexError:
            self.logger.warning(f'Error downloading {web_folder}. Schedule file might be missing!')
            return None
        except Exception as e:
            self.logger.warning(f'Error downloading {web_folder}: {str(e)}')
            return None


    def get_target_files(self) -> List[FileMetadata]:

        if self.urls is None:
            session_start_dates = self.db_handler.get_sessions_with_start_dates()

            sessions_without_skd_files = self.db_handler.get_sessions_with_missing_skd_file()

            sessions_to_check = list(filter(lambda sessions_start_dates: sessions_start_dates[0] in sessions_without_skd_files
                                            ,session_start_dates)
                                     )

            urls = self._generate_file_urls(sessions_to_check)
        else:
            urls = self.urls
        file_metadatas = self._scrape_modified_dates(urls)

        target_metadatas = filter_metadatas_with_regex(file_metadatas, self.file_url_pattern)

        return target_metadatas
