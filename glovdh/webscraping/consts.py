from datetime import timezone, datetime

SKD_FILE_URL_TEMPLATE = \
    'https://cddis.nasa.gov/archive/vlbi/ivsdata/aux/{year}/{session}/{session}.skd'

MASTERFILE_URL_TEMPLATE =\
    'https://cddis.nasa.gov/archive/vlbi/ivscontrol/{filename}'

TRF_FILES_URL_TEMPLATE = 'https://cddis.nasa.gov/archive/vlbi/ivsproducts/trf/{filename}'

CRF_FILES_URL_TEMPLATE = 'https://cddis.nasa.gov/archive/vlbi/ivsproducts/crf/{filename}'

NORMAL_SESSION_URL_TEMPLATE = \
    'https://cddis.nasa.gov/archive/vlbi/ivsproducts/eops/{filename}'

INTENSIVE_SESSION_URL_TEMPLATE = \
    'https://cddis.nasa.gov/archive/vlbi/ivsproducts/eopi/{filename}'

SOURCE_METADATA_TEMPLATE = \
    'https://cddis.nasa.gov/archive/vlbi/gsfc/ancillary/solve_apriori/{filename}'

STATION_LIST_URL = 'https://raw.githubusercontent.com/nvi-inc/sked_catalogs/main/position.cat'

PARSER_MASTER_FILES_MINIMUM_DATE = datetime(year=2021, month=1, day=1, tzinfo=timezone.utc)
PARSE_EOP_MINIMUM_DATE = datetime(year=2021, month=1, day=1, tzinfo=timezone.utc)
PARSE_CRF_MINIMUM_DATE = datetime(year=2021, month=1, day=1, tzinfo=timezone.utc)
PARSE_TRF_MINIMUM_DATE = datetime(year=2021, month=1, day=1, tzinfo=timezone.utc)

SOURCE_METADATA_FILE_REGEX = r'.*\/IVS_SrcNamesTable.txt$'
MASTER_FILE_REGEX = r".*\/master\d{4}(-int)?\.txt$"
EOPI_FILE_REGEX = r'.*\/[^.\s]*?.eopi(?:.gz)?(?:.gz)?$'
EOPS_FILE_REGEX = r'.*\/[^.\s]*?.(?:eops|eoxy)(?:.gz)?(?:.gz)?$'
CRF_FILE_REGEX = r'.*\/\w*.crf.gz$'
TRF_FILE_REGEX = '.*\/\w*.trf.gz$'
SKD_FILE_REGEX = '.*\/\w[^.]*?.skd$'

#has to match migrations/0002_populate_session_types.py values
INTENSIVE_SESSION = 'intensive'
NORMAL_SESSION = '24-hour'


#Some trf files list stations that are not in the official catalogue
#to be able to parse the file, we dont throw an error in case of these stations
#just discard them
UNKNOWN_STATIONS_WHITELIST = [
    "GGAO7108",
    "GORF7102",
    "BLKBUTTE",
    "YUMA",
    "PVERDES",
    "TIGOWTZL",
    "JPL_MV1",
    "PENTICTN",
    "YLOW7296",
    "SANPAULA",
    "GIFU11",
    "WIDE85_3",
    "PBLOSSOM",
    "FORT",
    "PRESIDIO",
    "FORTORDS",
    "MON_PEAK",
    "VERNAL",
    "KASHIMA",
    "VNDNBERG",
    "YAKATAGA",
    "PLATTVIL",
    "Penticton,",
    "KODIAK",
    "TRYSILNO",
    "TIGOCONC",
    "QUINCY",
    "SNDPOINT",
    "DSS65",
    "FLAGSTAF",
    "ELY",
    "YEBES",
    "RICHMOND",
    "FORT_ORD",
    "Algonquin",
    "NRAO85_1",
    "PINFLATS",
    "34-m",
    "CTVASTJ",
    "JPL",
    "HRAS_085",
    'PARKES64',
    'ARIES_9M',
    'TIGO',
    'ONSALDBC',
    'SYOWA10',
    'TSUKU3',
    'YEBESDBC',
    'METSHOVI',
    'CTVA',
    'GBT-VLBA',
    'ORION_5M',
    'WETTDBBC',
    'WARK30M'
]
