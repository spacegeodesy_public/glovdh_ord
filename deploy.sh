#!/usr/bin/bash

# Description: This script is used to deploy the application on the server.

# activate python venv
cd /home/wwwglovdh || exit
source private/venv/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/wwwglovdh/public

# update git repository
cd public || exit
cd glovdh_ord || exit
git fetch origin
git reset --hard origin/main

# run django tests
cd ..
python glovdh_ord/manage.py test --no-input #destroys existing test db by default after use

# rerunning the test with --keepdb option so the db exists between test runs
# (--keepdb looks for an existing db to use but we want to force a new db, hence the first run)
# (This is only for ETHZ deployment, can be removed)
python glovdh_ord/manage.py test --keepdb

# run django migrations if there is any new one
python glovdh_ord/manage.py migrate

# build the frontend code
source /home/wwwglovdh/private/.nodeenv/bin/activate
cd glovdh_ord/glovdh-fe
HOME=/home/wwwglovdh/private npm install
HOME=/home/wwwglovdh/private ng build

# refresh static files from potential angular build and other sources
source /home/wwwglovdh/private/venv/bin/activate #reactivate python venv, .nodeenv deactivates it before
cd /home/wwwglovdh/public || exit
python glovdh_ord/manage.py collectstatic --noinput --clear

# restart the running wsgi server
touch wwwglovdh.wsgi
