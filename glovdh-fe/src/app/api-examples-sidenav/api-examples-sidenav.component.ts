import { Component, OnInit } from '@angular/core';
import { ApiExamplesContentService, Chapter } from '../api-examples-content.service';

@Component({
  selector: 'app-api-examples-sidenav',
  templateUrl: './api-examples-sidenav.component.html',
  styleUrl: './api-examples-sidenav.component.css'
})
export class ApiExamplesSidenavComponent {
  chapters: Chapter[] =[];

  constructor(private examplesService: ApiExamplesContentService) {
  }

  async ngOnInit() {
    await this.examplesService.init();
    this.chapters = this.examplesService.getChapters();
  }

  getFragmentUrl(fragment: string): string {
    return `/examples#${fragment}`;
  }

}
