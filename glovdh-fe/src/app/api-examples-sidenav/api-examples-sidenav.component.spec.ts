import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiExamplesSidenavComponent } from './api-examples-sidenav.component';

describe('ApiExamplesSidenavComponent', () => {
  let component: ApiExamplesSidenavComponent;
  let fixture: ComponentFixture<ApiExamplesSidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ApiExamplesSidenavComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ApiExamplesSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
