import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'urlLastPart'
})
export class UrlLastPartPipe implements PipeTransform {
  transform(url: string): string {
    let parts = url.split('/')
    if(parts){
      return parts.pop()!!
    }
    return url
  }
}
