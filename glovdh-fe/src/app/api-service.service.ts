import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstants } from './consts';

const API_PREFIX = 'api/v1/';

@Injectable({
  providedIn: 'root',
})
export class ApiServiceService {
  API_ENDPOINT = AppConstants.API_ENDPOINT;
  constructor(private httpClient: HttpClient) {}

  queryParamsToHttpParams(queryParams: { [key: string]: any }): HttpParams {
    let params = new HttpParams();

    for (let key in queryParams) {
      if (queryParams.hasOwnProperty(key)) {
        params = params.set(key, queryParams[key]);
      }
    }
    return params;
  }

  getEopQueryOptions(queryParams: { [key: string]: any }): Observable<any> {
    let url = this.API_ENDPOINT + API_PREFIX + 'query-eop-options';

    let params = this.queryParamsToHttpParams(queryParams);
    return this.httpClient.get(url, { params: params });
  }

  getCrfQueryOptions(queryParams: { [key: string]: any }): Observable<any> {
    let url = this.API_ENDPOINT + API_PREFIX + 'query-crf-options';

    let params = this.queryParamsToHttpParams(queryParams);
    return this.httpClient.get(url, { params: params });
  }

  getAgencies(queryParams: { [key: string]: any }) {
    let url = this.API_ENDPOINT + API_PREFIX + 'list-agencies';
    let params = this.queryParamsToHttpParams(queryParams);
    return this.httpClient.get(url, { params: params });
  }

  downloadEop(queryParams: { [key: string]: any }): Observable<any> {
    let url = this.API_ENDPOINT + API_PREFIX + 'download-eop';
    let params = this.queryParamsToHttpParams(queryParams);
    return this.httpClient.get(url, { params: params });
  }

  getEOPSourceFiles(agencyCode: string): Observable<any> {
    let url = this.API_ENDPOINT + API_PREFIX + 'eop-sourcefiles';
    let params = this.queryParamsToHttpParams({
      center: agencyCode,
    });
    return this.httpClient.get(url, { params: params });
  }

  getCRFSourceFiles(agencyCode: string): Observable<any> {
    let url = this.API_ENDPOINT + API_PREFIX + 'crf-sourcefiles';
    let params = this.queryParamsToHttpParams({
      center: agencyCode,
    });
    return this.httpClient.get(url, { params: params });
  }

  queryEOP(requestBody: { [key: string]: any }) {
    let url = this.API_ENDPOINT + API_PREFIX + 'query-eop';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }

  queryCRF(requestBody: { [key: string]: any }) {
    let url = this.API_ENDPOINT + API_PREFIX + 'query-crf';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }

  freeSearch(requestBody: { [key: string]: any }) {
    let url = this.API_ENDPOINT + API_PREFIX + 'search';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }

  stationView(station_code: string) {
    let url = this.API_ENDPOINT + API_PREFIX + 'station/' + station_code;
    return this.httpClient.get(url);
  }

  stationStatsView(station_code: string) {
    let url = this.API_ENDPOINT + API_PREFIX + 'station-stats/' + station_code;
    return this.httpClient.get(url);
  }


  sourceStatsView(source_name: string) {
    let url = this.API_ENDPOINT + API_PREFIX + 'source-stats/' + source_name;
    return this.httpClient.get(url);
  }


  sourceView(station_code: string) {
    let url = this.API_ENDPOINT + API_PREFIX + 'source/' + station_code;
    return this.httpClient.get(url);
  }

  sessionStats(session_id: string) {
    let url = this.API_ENDPOINT + API_PREFIX + 'session/' + session_id;
    return this.httpClient.get(url);
  }

  getPrograms() {
    let url = this.API_ENDPOINT + API_PREFIX + 'list-programs';
    return this.httpClient.get(url);
  }

  querySessions(requestBody: any) {
    let url = this.API_ENDPOINT + API_PREFIX + 'query-sessions';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }

  getAllStations(requestBody: { [key: string]: any }){
    let url = this.API_ENDPOINT + API_PREFIX + 'list-stations';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }
  
  getAllSources(){
    let url = this.API_ENDPOINT + API_PREFIX + 'list-sources';
    return this.httpClient.get(url);
  }

  queryFiles(requestBody: { [key: string]: any }){
    let url = this.API_ENDPOINT + API_PREFIX + 'query-files';
    let params = this.queryParamsToHttpParams(requestBody);
    return this.httpClient.get(url, { params: params });
  }

  getFileTypeCounts(){
    let url = this.API_ENDPOINT + API_PREFIX + 'filetype-counts';
    return this.httpClient.get(url);
  }

  getAPIExamples(){
    let url = this.API_ENDPOINT + 'api-examples';
    return this.httpClient.get(url);
  }

}
