import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiExamplesContentComponent } from './api-examples-content.component';

describe('ApiExamplesContentComponent', () => {
  let component: ApiExamplesContentComponent;
  let fixture: ComponentFixture<ApiExamplesContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ApiExamplesContentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ApiExamplesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
