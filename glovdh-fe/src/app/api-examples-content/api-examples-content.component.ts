import { AfterViewInit, ChangeDetectorRef, Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ApiExamplesContentService, Chapter } from '../api-examples-content.service';
import { CodeHighlightComponent } from '../code-highlight/code-highlight.component';

@Component({
  selector: 'app-api-examples-content',
  templateUrl: './api-examples-content.component.html',
  styleUrl: './api-examples-content.component.css'
})
export class ApiExamplesContentComponent implements AfterViewInit {
  @ViewChildren(CodeHighlightComponent) codeHighlights!: QueryList<CodeHighlightComponent>;
  chapters: Chapter[] =[];
  public language = 'python'
  public code = '';

  constructor(private examplesService: ApiExamplesContentService,
    private cdr: ChangeDetectorRef) {}


  async ngAfterViewInit() {
    // Wait for chapters to be initialized
    if (!this.chapters.length) {
      await this.examplesService.init();
      this.chapters = this.examplesService.getChapters();
      // Manually trigger change detection
      this.cdr.detectChanges();
    }

    // Now the view should be updated, and we can safely call highlight
    if (this.codeHighlights) {
      this.codeHighlights.get(0)?.highlightAll();
    }
  }



}
