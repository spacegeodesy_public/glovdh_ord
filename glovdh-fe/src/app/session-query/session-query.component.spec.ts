import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionQueryComponent } from './session-query.component';

describe('SessionQueryComponent', () => {
  let component: SessionQueryComponent;
  let fixture: ComponentFixture<SessionQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SessionQueryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SessionQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
