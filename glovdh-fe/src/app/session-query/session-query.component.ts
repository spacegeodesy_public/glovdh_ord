import { ChangeDetectorRef, Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';
import { QueryResult } from '../models';

declare let Plotly: any;
@Component({
  selector: 'app-session-query',
  templateUrl: './session-query.component.html',
  styleUrl: './session-query.component.css',
})
export class SessionQueryComponent {

  public ALLPROGRAMS: string = 'All Programs';
  public selectedProgramCtrl = new FormControl([] as any[]);
  public selectedSessionType = 'all'

  public programs: string[] = [];
  public filteredPrograms: string[] = [];

  public startDate = new FormControl(new Date('1950-01-01'));
  public endDate = new FormControl();

  public queryResult? : QueryResult = undefined;
  public showQueryResult = false;
  private firstQueryRan = false;
  private lastQuery: any = {};
  public runningQuery = false;
  public currentQueryName : string= 'session_query';

  constructor(
    private apiService: ApiServiceService,
    private dialog: MatDialog
  ) {}


  ngOnInit() {
    this.apiService.getPrograms().subscribe({
      next: (response: any) => {
        this.programs = [this.ALLPROGRAMS, ...response.data];
        this.filteredPrograms = this.programs;
        this.selectAllPrograms(true);
      },
      error: (error: any) => {
        this.showErrorDialog('Error occured when fetching programs');
      }
    });
  }

  programSelectionChanged(event: any, program: string) {
    if (event.isUserInput && program === this.ALLPROGRAMS && event.source.selected){
      this.selectAllPrograms();
    }
    if (event.isUserInput && program === this.ALLPROGRAMS && !event.source.selected){
      this.deselectAllPrograms();
    }
  }


  filterItems(filter: any): void {
    let searchValue = filter.value.toLowerCase(); 
    if (!searchValue) {
      this.filteredPrograms = this.programs;
    }
    else{
      this.filteredPrograms = this.programs.filter((option) =>
        option.toLowerCase().includes(searchValue)
      );
    }
  }


  selectAllPrograms(selectAllProgramsButton=false) {
    if(selectAllProgramsButton){
      this.selectedProgramCtrl.setValue(this.programs);
    }
    else{
      this.selectedProgramCtrl.setValue(this.programs
        .filter(program => program !== this.ALLPROGRAMS)
      );
    }
  }

  deselectAllPrograms() {
    this.selectedProgramCtrl.setValue([]);
  }


  runQuery() {
    this.runningQuery = true;
    try {
      let requestBody: any = {
        programs: this.selectedProgramCtrl.value,
        type: this.selectedSessionType
      };
      
      if (this.startDate.value) {
        requestBody.min_date = this.startDate.value.toISOString().replace('.000Z', 'Z');
      }
      
      if (this.endDate.value) {
        requestBody.max_date = this.endDate.value.toISOString().replace('.000Z', 'Z');
      }
      
      this.lastQuery = requestBody;
      
      this.apiService.querySessions(requestBody).subscribe({
        next: (response: any) => {
          try {
            this.queryResult = response;

            this.currentQueryName = this.getDescriptionName();
            this.showQueryResult = true;
            this.plot_charts(response.rows);
            this.firstQueryRan = true;
            this.runningQuery = false;
          } catch (error) {
            this.runningQuery = false;
            console.log(error)

            this.showErrorDialog('Error occured when running query');
          }
        },
        error: (error: any) => {
          this.runningQuery = false;
          console.log(error)

          this.showErrorDialog('Error occured when running query');
        },
      });
    } catch (error) {
      this.runningQuery = false;
      this.showErrorDialog('Error occured when running query');
    }
  }
  showErrorDialog(errorMessage: string): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { message: errorMessage },
    });
  }

  extractSessionInfo(session: any[]): { program: string; year: number } {
    const program: string = session[1];
    const startDate: Date = new Date(session[2]);
    const year: number = startDate.getFullYear();
    return { program, year };
  }

  countProgramsPerYear(sessions: any[]): {
    [key: number]: { [key: string]: number };
  } {
    const sessionCounts: { [key: number]: { [key: string]: number } } = {};
    sessions.forEach((session) => {
      const { program, year } = this.extractSessionInfo(session);
      if (!sessionCounts[year]) {
        sessionCounts[year] = {};
      }
      if (!sessionCounts[year][program]) {
        sessionCounts[year][program] = 0;
      }
      sessionCounts[year][program]++;
    });
    return sessionCounts;
  }
  prepareDataForPlotting(sessionCounts: {
    [key: number]: { [key: string]: number };
  }): any[] {
    const years: number[] = Object.keys(sessionCounts).map(Number);
    const minYear: number = Math.min(...years);
    const maxYear: number = Math.max(...years);

    const yearRange: number[] = [];
    for (let year = minYear; year <= maxYear; year++) {
      yearRange.push(year);
    }

    const traces: any[] = [];
    for (const program of this.programs) {
      const yearlyProgramCounts = [];
      let foundValidValue = false;
      for (let year = minYear; year <= maxYear; year++) {
        let yearData = sessionCounts[year];
        if (!yearData) {
          yearlyProgramCounts.push(0);
          continue;
        }
        let sessionsForProgram = sessionCounts[year][program] ?? 0;
        if (sessionsForProgram != 0) {
          foundValidValue = true;
        }
        yearlyProgramCounts.push(sessionsForProgram);
      }
      if (foundValidValue) {
        traces.push({
          x: yearRange,
          y: yearlyProgramCounts,
          name: `${program}`,
          type: 'bar',
        });
      }
    }
    return traces;
  }

  plot_charts(data: { [key: number]: { [key: string]: number } }) {
    let aggregated = this.countProgramsPerYear(data as any[]);
    let traces = this.prepareDataForPlotting(aggregated);

    var layout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Yearly Number of Sessions for Programmes',
    };
    const barplotDiv = 'barPlotDiv';
    if (!this.firstQueryRan) {
      Plotly.newPlot(barplotDiv, traces, layout);
    } else {
      Plotly.react(barplotDiv, traces, layout);
    }
  }

  getDescriptionName() {
    const minTime = this.lastQuery.min_date
      .substring(0, 10)
      .replace(/-/g, '');
    const maxTime = this.lastQuery.max_date?.substring(0, 10).replace(/-/g, '');
    const programmes = this.lastQuery.programs.slice(0, 2).join('-');
    const leftOver =
      this.lastQuery.programs.length <= 2
        ? ''
        : `+${this.lastQuery.programs.length - 2}`;

    return `${programmes}${leftOver}_${minTime}_${maxTime}`;
  }

  
}
