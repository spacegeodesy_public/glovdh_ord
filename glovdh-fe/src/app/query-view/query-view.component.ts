import { Component, Input, ViewChild } from '@angular/core';
import { Query } from '../eop-query/eop-query.component';
import { ChartHandlerComponent } from '../chart-handler/chart-handler.component';
import { AppConstants } from '../consts';

@Component({
  selector: 'app-query-view',
  templateUrl: './query-view.component.html',
  styleUrl: './query-view.component.css',
})
export class QueryViewComponent {
  queries: Query[] = [];

  @ViewChild(ChartHandlerComponent) chartsHandler!: ChartHandlerComponent;
  @Input() public page: string = '';

  public showingCharts: boolean = true;
  public shownQuery: Query | undefined;
  public runningQuery = false;

  public selectedQueryToPlot = this.queries[0];

  columns: any = [];
  displayedColumns: any = [];

  generateColumns() {
    let colNames = this.shownQuery?.columns!;

    this.columns = colNames.map((colName, idx) => {
      return {
        columnDef: colName,
        header: colName,
        cell: (element: any) => `${element[idx]}`,
      };
    });
    this.displayedColumns = colNames;
  }

  notifyQueryStarted() {
    this.runningQuery = true;
  }

  notifyQueryEnded() {
    this.runningQuery = false;
  }



  showCharts() {
    this.showingCharts = true;
    this.shownQuery = undefined;

}



  addQuery(query: Query) {
    this.notifyQueryEnded();
    this.queries.push(query);
    this.selectedQueryToPlot = query;
    if (this.page == AppConstants.EOP) {
      this.addConfidenceChart('epoch', 'XPO', 'sig_XPO', 'XPO');
      this.addConfidenceChart('epoch', 'YPO', 'sig_YPO', 'YPO');
      this.addConfidenceChart('epoch', 'UT1UTC', 'sig_UT1UTC', 'UT1UTC');
      this.addConfidenceChart('epoch', 'dY', 'sig_dY', 'dY');
      this.addConfidenceChart('epoch', 'dX', 'sig_dX', 'dX');
    }
    if (this.page == AppConstants.CRF) {
      this.addMollweideProjection();
      this.addHistPlotMAS();
    }
    this.showCharts();
  }

  removeQuery(query: Query){
    this.queries = this.queries.filter(q => q !== query);
    for(let key in this.chartsHandler.registeredCharts){
      let chart = {...this.chartsHandler.registeredCharts[key]};

      let filteredTraces = chart.data.filter((trace: any) => trace.name !== query.title);

      if (filteredTraces.length === 0){
        delete this.chartsHandler.registeredCharts[key];
      } else {
        chart.data = filteredTraces;
        this.chartsHandler.registeredCharts[key] = chart;
      }
    }
    this.showingCharts = true;
    this.shownQuery = undefined;
  }

  notifyChartRemoved() {}

  handleQuerySelection(query: Query) {
    this.shownQuery = query;
    this.generateColumns();
    this.showingCharts = false;
  }


  addMollweideProjection() {
    let asc: number[] = [];
    let dec: number[] = [];
    let asc_uncertainty: number[] = [];
    let dec_uncertainty: number[] = [];
    let names: string[] = [];
    this.selectedQueryToPlot!.data.forEach((dataPoint) => {
      let xp =
        dataPoint[this.selectedQueryToPlot.columns.indexOf('RA')];
      asc.push(xp as number);

      let yp =
        dataPoint[this.selectedQueryToPlot.columns.indexOf('DEC')];
      dec.push(yp as number);

      let unc =
        dataPoint[
          this.selectedQueryToPlot.columns.indexOf(
            'sig_RA'
          )
        ];
      asc_uncertainty.push(unc as number);

      let dec_unc =
        dataPoint[
          this.selectedQueryToPlot.columns.indexOf('sig_DEC')
        ];
      dec_uncertainty.push(dec_unc as number);
      names.push(
        'IVS:' +
          dataPoint[this.selectedQueryToPlot.columns.indexOf('source_id')]
      );
    });
    this.chartsHandler.addMollweideProjection(
      asc,
      dec,
      asc_uncertainty,
      dec_uncertainty,
      names,
      this.selectedQueryToPlot.title
    );

  }

  addHistPlotMAS() {
    let asc: number[] = [];
    let dec: number[] = [];
    let asc_uncertainty: number[] = [];
    let dec_uncertainty: number[] = [];
    let names: string[] = [];
    this.selectedQueryToPlot!.data.forEach((dataPoint) => {
      let xp =
        dataPoint[this.selectedQueryToPlot.columns.indexOf('RA')];
      asc.push(xp as number);

      let yp =
        dataPoint[this.selectedQueryToPlot.columns.indexOf('DEC')];
      dec.push(yp as number);

      let unc =
        dataPoint[
          this.selectedQueryToPlot.columns.indexOf(
            'sig_RA'
          )
        ];
      asc_uncertainty.push(unc as number);

      let dec_unc =
        dataPoint[
          this.selectedQueryToPlot.columns.indexOf('sig_DEC')
        ];
      dec_uncertainty.push(dec_unc as number);
      names.push(
        'IVS:' +
          dataPoint[this.selectedQueryToPlot.columns.indexOf('source_id')]
      );
    });
    this.chartsHandler.addHistPlotMAS(
      asc_uncertainty,
      dec_uncertainty,
      'Uncertaininty - ' + this.selectedQueryToPlot.title,
      'Uncertaininty - ' + this.selectedQueryToPlot.title,
    );

  }
  addConfidenceChart(xColName: string, yColName: string, confColName: string, chartName: string) {
    let xDataIndex = this.selectedQueryToPlot.columns.indexOf(
      xColName
    );
    let yDataIndex = this.selectedQueryToPlot.columns.indexOf(
      yColName
    );
    let confDataIndex = this.selectedQueryToPlot.columns.indexOf(
      confColName
    );

    let x = this.selectedQueryToPlot.data.map(
      (dataPoint) => dataPoint[xDataIndex!]
    );
    let y = this.selectedQueryToPlot.data.map(
      (dataPoint) => dataPoint[yDataIndex!]
    );
    let confidence = this.selectedQueryToPlot.data.map(
      (dataPoint) => dataPoint[confDataIndex!]
    );

    this.chartsHandler.addConfidenceChart(
      x as string[],
      y as number[],
      confidence as number[],
      this.selectedQueryToPlot.title,
      chartName
    );

  }

  download() {
    if (!this.shownQuery) {
      return;
    }

    const header = this.displayedColumns;
    let stationIndex = this.displayedColumns.indexOf('stations');

    const csv = this.shownQuery.data.map((row: any) => {
      let rowCopy = [...row];
      if (stationIndex != -1) {
        rowCopy[stationIndex] = rowCopy[stationIndex].join('');
      }
      rowCopy = rowCopy.map((value) =>
        value == null || value == undefined ? 'null' : value
      );
      return rowCopy.join(',');
    });
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const a = document.createElement('a');
    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);

    a.href = url;

    a.download = this.shownQuery.title + '.csv';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }



}
