import {
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { QueryViewComponent } from '../query-view/query-view.component';
import { AppConstants } from '../consts';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { FormControl } from '@angular/forms';

export interface Query {
  title: string;
  data: number[][] | string[][];
  columns: string[];
  units?: string[];
  query: { [key: string]: string };
}

@Component({
  selector: 'app-eop-query',
  templateUrl: './eop-query.component.html',
  styleUrl: './eop-query.component.css',
})
export class EopQueryComponent implements OnInit {
  private ALL = 'all';

  public analystCenters: string[] = [];
  public selectedAnalystCenter: string | null = null;
  public sourceFiles: string[] = [];
  public selectedSourceFile: string | null = null;
  
  public ALLPROGRAMS: string = 'All Programs';
  public selectedProgramCtrl = new FormControl([] as any[]);
  public programs: string[] = [];
  public filteredPrograms: string[] = [];
  private _originalProgramResponse: string[] = [];


  public queryTag: string = ''

  public plotOptions: string[] = [];
  private earliestDate = new Date('1950-01-01')
  private currentDate = new Date();
  public startDate = new FormControl(this.earliestDate);
  public endDate = new FormControl(this.currentDate);

  public loading = false;

  private queryPrefix = 'Q#';
  private userChangedQueryTag = false;

  constructor(
    private apiService: ApiServiceService,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef
  ) {}

  @ViewChild(QueryViewComponent) queryHandler!: QueryViewComponent;

  ngOnInit(): void {
    this.apiService.getAgencies({ project: 'eop' }).subscribe({
      next: (response: any) => {
        this.analystCenters = response.data;
        this.selectedAnalystCenter = this.analystCenters[0];

        this.updateQueryOptions(true);
      },
      error: (error: any) => {
        this.analystCenters = ['Error Getting Agencies'];
        this.loading = false;
        this.showErrorDialog('An Error occured when fetching agencies:(');
      },
    });
  }

  updateQueryOptions(selectAllPrograms=false, updateSourceFiles=true) {
    this.loading = true;

    let eopQueryOptionsPayload : any= {
      analyst_center: this.selectedAnalystCenter
    };
    
    if (this.selectedSourceFile !== null) {
      eopQueryOptionsPayload.filename = this.selectedSourceFile;
    }
  
    this.apiService
      .getEopQueryOptions(eopQueryOptionsPayload)
      .subscribe({
        next: (response: any) => {
          this._originalProgramResponse = response.programs;
          this.programs = [this.ALLPROGRAMS, ...response.programs];
          this.filteredPrograms = this.programs;
          if(updateSourceFiles){
            this.sourceFiles = response.source_files;
            this.selectedSourceFile = this.sourceFiles[0];
          }
          this.generateNextQueryTag();
          if(selectAllPrograms){
            this.selectAllPrograms(true);
          }
          else{
            this.deselectAllPrograms();
          }
          this.loading = false;
        },
        error: (error: any) => {
          this.programs = ['Error Getting Programmes'];
          this.sourceFiles = ['Error Getting Source Files'];
          this.loading = false;
        },
      }),
      (error: any) => {
        this.analystCenters = ['Error Getting Agencies'];
        this.loading = false;
      };
  }

  ngAfterViewInit() {
    this.queryChanged();
    this.cdr.detectChanges();
  }

  onAnalystAgencyChange(ac: string) {
    this.selectedAnalystCenter = ac;
    this.selectedSourceFile = null;
    this.updateQueryOptions(true);
  }

  onSourceFileChanges(chosenFile: string){
    this.selectedSourceFile = chosenFile;
    this.updateQueryOptions(true, false);
  }


  allProgramsAreSelected(): boolean {
    return this.selectedProgramCtrl.value?.length === this.programs.length;
  }

  runQuery() {
    this.loading=true;
    try {
      this.queryHandler.notifyQueryStarted();
      let requestBody: any = {
        ...(!this.allProgramsAreSelected() && {
          programs: this.selectedProgramCtrl.value,
        }),
        ...(this.selectedAnalystCenter !== this.ALL && {
          analyst_center: this.selectedAnalystCenter,
        }),
        ...(this.selectedSourceFile !== this.ALL && {
          filename: this.selectedSourceFile,
        }),
      };
      if (this.startDate.value != this.earliestDate && this.startDate.value) {
        requestBody.min_date = this.startDate.value.toISOString().replace('.000Z', 'Z');
      }
      
      if (this.endDate.value != this.currentDate && this.endDate.value) {
        requestBody.max_date = this.endDate.value.toISOString().replace('.000Z', 'Z');
      }
      
      this.apiService.queryEOP(requestBody).subscribe({
        next: (response: any) => {
          try {

            let queryMeta = {
              analystAgency: this.selectedAnalystCenter!,
              sourceFile: this.selectedSourceFile!,
              programs: this.getSelectedProgramsQueryTagPart(),
            };

            let query = {
              title: this.queryTag,
              data: response.rows,
              query: queryMeta,
              columns: response.columns,
              units: response.units,
            };
            this.queryHandler.addQuery(query);

            this.userChangedQueryTag = false;
            this.queryTag = this.generateNextQueryTag();
          } catch (error) {
            console.log(error)
            this.showErrorDialog('An Error occured when running the query:(');
            this.queryHandler.notifyQueryEnded();
          }
          this.loading=false;
        },
        error: (error: any) => {
          this.loading=false;
          console.log(error)
          this.showErrorDialog('An Error occured when running the query:(');
          this.queryHandler.notifyQueryEnded();
        },
      });
    } catch (error) {
      this.loading=false;
      this.showErrorDialog('An Error occured when running the query:(');
      this.queryHandler.notifyQueryEnded();
    }
  }

  showErrorDialog(errorMessage: string): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { message: errorMessage },
    });
  }


  
  selectAllPrograms(selectAllProgramsButton=false) {
    if(selectAllProgramsButton){
      this.selectedProgramCtrl.setValue(this.programs);
    }
    else{
      this.selectedProgramCtrl.setValue(this.programs
        .filter(program => program !== this.ALLPROGRAMS)
      );
    }
  }

  deselectAllPrograms() {
    this.selectedProgramCtrl.setValue([]);
  }


  programSelectionChanged(event: any, program: string) {
    if (event.isUserInput && program === this.ALLPROGRAMS && event.source.selected){
      this.selectAllPrograms();
    }
    if (event.isUserInput && program === this.ALLPROGRAMS && !event.source.selected){
      this.deselectAllPrograms();
    }
  }


  filterItems(filter: any): void {
    let searchValue = filter.value.toLowerCase(); 
    if (!searchValue) {
      this.filteredPrograms = this.programs;
    }
    else{
      this.filteredPrograms = this.programs.filter((option) =>
        option.toLowerCase().includes(searchValue)
      );
    }
  }


  queryChanged() {
    this.queryTag = this.generateNextQueryTag();
  }

  queryTagChanged() {
    this.userChangedQueryTag = true;
  }

  generateNextQueryTag(): string {
    
    let candidateTag = this.queryTag;
    if (!this.userChangedQueryTag) {
      let fnamePart = this.selectedSourceFile;
      if (fnamePart !== null) {
        let fnameParts = this.selectedSourceFile!.split('/');
        fnamePart = fnameParts[fnameParts.length - 1].split('.')[0];
      }
      candidateTag = this.queryPrefix + fnamePart;
      candidateTag = candidateTag + this.getSelectedProgramsQueryTagPart()
    }
    let v = 1;
    let versionedTag = candidateTag;
    while (this.queryHandler.queries.map((q) => q.title).includes(versionedTag)) {
      versionedTag = candidateTag + '#' + v;
      v += 1;
    }
    this.queryTag = versionedTag;
    return this.queryTag;
  }

  getSelectedProgramsQueryTagPart(): string {

    if(!this.selectedProgramCtrl.value || this.selectedProgramCtrl.value.length === 0){
      return '';  
    }

    if(this.arraysAreEqual(this.selectedProgramCtrl.value!, this._originalProgramResponse)){
      return '';
    }

    if(this.selectedProgramCtrl.value!.length === 1){
      return this.selectedProgramCtrl.value![0];
    }

    return this.selectedProgramCtrl.value![1] + '..'

  }


  arraysAreEqual(arr1: any[], arr2: any[]): boolean {
    if (arr1.length !== arr2.length) {
      return false;
    }
  
    const set1 = new Set(arr1);
    const set2 = new Set(arr2);
  
    if (set1.size !== set2.size) {
      return false;
    }
  
    for (const item of set1) {
      if (!set2.has(item)) {
        return false;
      }
    }
  
    return true;
  }
  

}
