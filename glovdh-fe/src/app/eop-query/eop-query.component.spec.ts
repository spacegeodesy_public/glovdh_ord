import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EopQueryComponent } from './eop-query.component';

describe('EopQueryComponent', () => {
  let component: EopQueryComponent;
  let fixture: ComponentFixture<EopQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EopQueryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EopQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
