import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-button-navigation',
  templateUrl: './button-navigation.component.html',
  styleUrls: ['./button-navigation.component.css'],
})
export class ButtonNavigationComponent {
  constructor(private router: Router) {}

  setTab(tab: string): void {
    this.router.navigate(['/' + tab]);
  }

  getTab(): string {
    return this.router.url;
  }
}
