import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StationViewComponent } from './station-view/station-view.component';
import { CrfQueryComponent } from './crf-query/crf-query.component';
import { EopQueryComponent } from './eop-query/eop-query.component';
import { SourceViewComponent } from './source-view/source-view.component';
import { SessionViewComponent } from './session-view/session-view.component';
import { SessionQueryComponent } from './session-query/session-query.component';
import { GeneralTabComponent } from './general-tab/general-tab.component';
import { ApiExamplesComponent } from './api-examples/api-examples.component';

const routes: Routes = [
  { path: '', redirectTo: 'general', pathMatch: 'full' },
  { path: 'station/:id', component: StationViewComponent },
  { path: 'source/:id', component: SourceViewComponent },
  { path: 'session/:id', component: SessionViewComponent },
  { path: 'crf', component: CrfQueryComponent },
  { path: 'eop', component: EopQueryComponent },
  { path: 'sessions', component: SessionQueryComponent },
  { path: 'general', component: GeneralTabComponent },
  { path: 'examples', component: ApiExamplesComponent },
  { path: '**', redirectTo: 'general', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
