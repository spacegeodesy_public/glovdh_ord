import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { Session, QueryResult } from '../models';
import { FormControl } from '@angular/forms';


declare let Plotly: any;
@Component({
  selector: 'app-station-view',
  templateUrl: './station-view.component.html',
  styleUrl: './station-view.component.css',
  animations: [
    trigger('detailExpand', [
      state('collapsed,void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class StationViewComponent {
  
  public sessionData: any[] = [];
  public sessionDataColumnNames : string[] = [];

  public id = 'station';
  public fullName = '';
  public stationCode = '';
  public sessions: Session[] = [];
  private stationStatsQuery?: QueryResult = undefined
  public now = new Date(Date());
  public loadingPage = true;
  public errorGettingStation = false;
  private earliestDate = new Date('1950-01-01')
  private latestDate = new Date();
  public timeFilterStart = new FormControl(this.earliestDate);
  public timeFilterEnd = new FormControl(this.latestDate);
  
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService,
    public changeDetectorRef: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}



  ngOnInit() {
    this.latestDate.setFullYear(this.latestDate.getFullYear() + 2);



    this.route.params.subscribe((params) => {
      this.stationCode = params['id'];
      let data = this.apiService.stationView(params['id']).subscribe({
        next: (data: any) => {
          try {
            this.fullName = data.info.name
            this.sessions = this.getSessions(data);
            this.apiService.stationStatsView(params['id']).subscribe(
              {
                next: (data: any) => {
                  this.stationStatsQuery = data
                  this.plotScansObsBarChart(data, this.timeFilterStart.value!, this.timeFilterEnd.value!);
                },
                error: (error: string) => {
                  console.log(error)
                  this.showErrorDialog('An Error Occured Loading when the Page:(');
                  this.loadingPage = false;
                  this.errorGettingStation = true;
                },
              }
            )

            if (this.sessions.length == 0) {
              this.loadingPage = false;
              return;
            }

            this.sessionData = this.sessions.map((session: Session) => {
                return [session.session, session.program, session.start]

            })
            this.sessionDataColumnNames = data.sessions.columns.slice(0, 3);
            

            this.plotSessionPieChart(this.sessions);
            this.plotSessionCountBarChart(this.sessions);
            
            this.plotTRF(data.position);
            this.plotPosition(data.info)
            this.loadingPage = false;
          } catch (error) {
            console.log(error)
            this.showErrorDialog('An Error Occured Loading when the Page:(');
            this.loadingPage = false;
          }
        },
        error: (error: string) => {
          console.log(error)
          this.showErrorDialog('An Error Occured Loading when the Page:(');
          this.loadingPage = false;
          this.errorGettingStation = true;
        },
      });
      
     


    });
  }

  updateTimeFilters(){
    if(this.timeFilterStart.value! >= this.earliestDate && this.timeFilterEnd.value! <= this.latestDate )
      {
        this.plotScansObsBarChart(this.stationStatsQuery!,
         this.timeFilterStart.value!, 
         this.timeFilterEnd.value!,
          true
         );

        let filteredSessions = this.sessions.filter((session: Session) =>
          session.start >= this.timeFilterStart.value! && session.start <= this.timeFilterEnd.value!
        )

         this.plotSessionPieChart(filteredSessions, true);
         this.plotSessionCountBarChart(filteredSessions, true);
      }
  }
  

  statusFromTime(start: string, end: string) {
    let startDate = new Date(start);
    let endDate = new Date(end);

    if (endDate < this.now) {
      return 'Completed';
    } else if (endDate > this.now && startDate < this.now) {
      return 'In Progress';
    }
    return 'Planned';
  }

  getStatusColor(status: string) {
    if (status == 'Completed') {
      return '#2ed144';
    } else if (status == 'In Progress') {
      return '#cfcc36';
    }
    return '#9ba4a8';
  }

  networkStringToList(network: string) {
    const twoLetterCodes = [];

    for (let i = 1; i < network.length; i += 2) {
      const code = network[i - 1] + network[i];
      twoLetterCodes.push(code);
    }
    return twoLetterCodes;
  }


  getSessions(data: any): Session[] {

    return data.sessions.rows.map((row: any[]) => ({
      session: row[data.sessions.columns.indexOf('session_id')],
      program: row[data.sessions.columns.indexOf('program')],
      start: new Date(row[data.sessions.columns.indexOf('time_start')]),
      status: this.statusFromTime(
        row[data.sessions.columns.indexOf('time_start')],
        row[data.sessions.columns.indexOf('time_end')]
      ),
      network: this.networkStringToList(row[data.sessions.columns.indexOf('stations')]),
      sessionType: row[data.sessions.columns.indexOf('session_type')],
    }));
  }



  showErrorDialog(errorMessage: string): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { message: errorMessage },
    });
  }

  plotPosition(info: {[key: string]: string}){
    let lat = info['latitude']
    let long = info['longitude']


    let posTraces = [{
        type: 'scattergeo',
        mode: 'markers',
        lat: [lat],
        lon: [long],
        marker: {
            size: 8,
            color: 'red'
        },
        name: 'Position'
    }];

    let layout = {
      geo: {
          scope: 'world',
          projection: {
              type: 'equirectangular'
          },
          showland: true,
          landcolor: 'rgb(217, 217, 217)',
          subunitwidth: 1,
          countrywidth: 1,
          subunitcolor: 'rgb(255,255,255)',
          countrycolor: 'rgb(255,255,255)'
      },
      title: 'Position on Map',
      margin: {
          l: 50, 
          r: 50,
          t: 50,
          b: 50 
      }
  };

    Plotly.newPlot('positionDiagram', posTraces, layout);
  }

  plotTRF(stationPositionQuery: QueryResult) {

    if (stationPositionQuery.rows.length == 0) {
      return;
    }

    let posTraces = stationPositionQuery.rows.map((row: any[]) => {
      const posX = row[stationPositionQuery.columns.indexOf('estimate_stax')];
      const posY = row[stationPositionQuery.columns.indexOf('estimate_stay')];
      const posZ = row[stationPositionQuery.columns.indexOf('estimate_staz')];
      const posXError = row[stationPositionQuery.columns.indexOf('error_stax')];
      const posYError = row[stationPositionQuery.columns.indexOf('error_stay')];
      const posZError = row[stationPositionQuery.columns.indexOf('error_staz')];
      const filename = row[stationPositionQuery.columns.indexOf('source_file')];

      return {
        x: ['X', 'Y', 'Z'],
        y: [posX, posY, posZ],
        name: filename,
        error_y: {
          type: 'data',
          array: [posXError, posYError, posZError],
          visible: true,
        },
        type: 'bar',
      };
    });

    let velTraces = stationPositionQuery.rows.map((row: any[]) => {
      const velX = row[stationPositionQuery.columns.indexOf('estimate_velx')];
      const velY = row[stationPositionQuery.columns.indexOf('estimate_vely')];
      const velZ = row[stationPositionQuery.columns.indexOf('estimate_velz')];
      const velXError = row[stationPositionQuery.columns.indexOf('error_velx')];
      const velYError = row[stationPositionQuery.columns.indexOf('error_vely')];
      const velZError = row[stationPositionQuery.columns.indexOf('error_velz')];
      const filename = row[stationPositionQuery.columns.indexOf('source_file')];


      return {
        x: ['X', 'Y', 'Z'],
        y: [velX, velY, velZ],
        name: filename,
        error_y: {
          type: 'data',
          array: [velXError, velYError, velZError],
          visible: true,
        },
        type: 'bar',
      };
    });

    posTraces = posTraces.filter((trace: any) => trace.y[0] != null);
    velTraces = velTraces.filter((trace: any) => trace.y[0] != null);

    var layout = { barmode: 'group', showlegend: true };
    Plotly.newPlot('trfPosChart', posTraces, {
      ...layout,
      title: 'Position Estimates',
    });
    Plotly.newPlot('trfVelChart', velTraces, {
      ...layout,
      title: 'Velocity Estimates',
    });
  }


  plotSessionPieChart(sessions: Session[], update=false) {
    const programCounts: { [key: string]: number } = {};

    sessions.forEach((row: Session) => {
      let program = row.program
      programCounts[program] =
        (programCounts[program] || 0) + 1;
    });

    const sortedSessionTypes = Object.keys(programCounts).sort(
      (a, b) => programCounts[b] - programCounts[a]
    );

    const topSessionTypes = sortedSessionTypes.slice(0, 10);

    const values: number[] = [];
    const labels: string[] = [];

    topSessionTypes.forEach((sessionType) => {
      labels.push(sessionType);
      values.push(programCounts[sessionType]);
    });

    const plotData: Plotly.Data[] = [
      {
        values: values,
        labels: labels,
        type: 'pie',
        automargin: true,
      },
    ];

    const layout: Partial<Plotly.Layout> = {
      title: 'Programme Frequency',
    };

    if(update){
      Plotly.react('sessionTypeChart', plotData, layout);

    }
    else{
      Plotly.newPlot('sessionTypeChart', plotData, layout);
    }
  }




  countSessionsPerYear(sessions: any[]): {
    sessionCounts: { [key: number]: { [key: string]: number } }
  } {
    const sessionCounts: { [key: number]: { [key: string]: number } } = {};
  
    sessions.forEach((session) => {
      const program = session.program
      const year = session.start.getFullYear()

  
      if (!sessionCounts[year]) {
        sessionCounts[year] = {};
      }
      if (!sessionCounts[year][program]) {
        sessionCounts[year][program] = 0;
      }
      sessionCounts[year][program]++;
    });
  
  
    return {
      sessionCounts
    };
  }
  

  countObsScansPerYear(sessions: QueryResult, min_date: Date, max_date: Date): {
    obsCounts: { [key: number]: { [key: string]: number } },
    scanCounts: { [key: number]: { [key: string]: number } }
  } {
    const obsCounts: { [key: number]: { [key: string]: number } } = {};
    const scanCounts: { [key: number]: { [key: string]: number } } = {};
    
    let sessionMap : {[key: string]: Session}= {}
    this.sessions.forEach((session) => {
    
      sessionMap[session.session] = session ;
    })

    sessions.rows.forEach((queryRow: any) => {
      
      const program = sessionMap[queryRow[sessions.columns.indexOf('session_id')]]?.program;
      if(!program){
        return
      }
      const scans = queryRow[sessions.columns.indexOf('scans')];
      const obs = queryRow[sessions.columns.indexOf('observations')];
      let date = sessionMap[queryRow[sessions.columns.indexOf('session_id')]]?.start
      
      if(date < min_date || date > max_date){
        return

      }

      const year = date.getFullYear();

      if (!obsCounts[year]) {
        obsCounts[year] = {};
      }
      if (!scanCounts[year]) {
        scanCounts[year] = {};
      }

      if (!obsCounts[year][program]) {
        obsCounts[year][program] = 0;
      }
      if (!scanCounts[year][program]) {
        scanCounts[year][program] = 0;
      }

      obsCounts[year][program] = obsCounts[year][program] + obs;
      scanCounts[year][program] = scanCounts[year][program] + scans;
    });
  
  
    return {
      obsCounts,
      scanCounts
    };
  }
  

  getStackBarPlotTraces(sessionCounts: {
    [key: number]: { [key: string]: number };
  }): any[] {
    const years: number[] = Object.keys(sessionCounts).map(Number);

    const minYear: number = Math.min(...years);
    const maxYear: number = Math.max(...years);

    const yearRange: number[] = [];
    for (let year = minYear; year <= maxYear; year++) {
      yearRange.push(year);
    }

    const traces: any[] = [];
    const programs = Array.from(new Set(this.sessions.map((session: Session) => session.program))).sort();

    for (const program of programs.sort()) {
      const yearlyProgramCounts = [];
      let foundValidValue = false;
      for (let year = minYear; year <= maxYear; year++) {
        let yearData = sessionCounts[year];
        if (!yearData) {
          yearlyProgramCounts.push(0);
          continue;
        }
        let sessionsForProgram = sessionCounts[year][program] ?? 0;
        if (sessionsForProgram != 0) {
          foundValidValue = true;
        }
        yearlyProgramCounts.push(sessionsForProgram);
      }

      if (foundValidValue) {
        traces.push({
          x: yearRange,
          y: yearlyProgramCounts,
          name: `${program}`,
          type: 'bar',
        });
      }
    }
    return traces;
  }

  plotSessionCountBarChart(sessions: Session[], update=false){
    let sessionCounts = this.countSessionsPerYear(sessions);
    let aggregatedCounts = sessionCounts.sessionCounts;
    let traces = this.getStackBarPlotTraces(aggregatedCounts);

    var layout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Number of Sessions per Year',
      xaxis: {
        tickvals: Object.keys(aggregatedCounts).map(Number).sort(),        
        title: 'Years'               
      },

    };
    if(update){
      Plotly.react('sessionCountBarPlot', traces, layout);
    }
    else{
      Plotly.newPlot('sessionCountBarPlot', traces, layout);

    }
  }


  plotScansObsBarChart(sessions: QueryResult, min_date: Date , max_date: Date, update=false){

    //min_date =  new Date('2016-09-11')//min_date || this.earliestDate
    //max_date =  new Date('2018-02-02') //max_date || this.latestDate
    let aggData = this.countObsScansPerYear(sessions, min_date, max_date);
    let scanTraces = this.getStackBarPlotTraces(aggData.scanCounts);
    let obsTraces = this.getStackBarPlotTraces(aggData.obsCounts);

    var scansLayout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Number of Scans Per Year',
      xaxis: {
        tickvals: Object.keys(aggData.scanCounts).map(Number).sort(),        
        title: 'Years'               
      },
    };

    var obsLayout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Number of Observations Per Year',
      xaxis: {
        tickvals: Object.keys(aggData.obsCounts).map(Number).sort(),        
        title: 'Years'               
      },
    };
    if(update){
      Plotly.react('scansBarPlot', scanTraces, scansLayout);
      Plotly.react('obsBarPlot', obsTraces, obsLayout);
    }
    Plotly.newPlot('scansBarPlot', scanTraces, scansLayout);
    Plotly.newPlot('obsBarPlot', obsTraces, obsLayout);
  }

  
}
