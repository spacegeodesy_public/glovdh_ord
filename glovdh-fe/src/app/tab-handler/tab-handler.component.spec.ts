import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHandlerComponent } from './tab-handler.component';

describe('TabHandlerComponent', () => {
  let component: TabHandlerComponent;
  let fixture: ComponentFixture<TabHandlerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TabHandlerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TabHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
