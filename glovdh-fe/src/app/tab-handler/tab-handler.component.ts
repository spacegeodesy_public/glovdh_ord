import { Component, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-tab-handler',
  templateUrl: './tab-handler.component.html',
  styleUrl: './tab-handler.component.css',
})
export class TabHandlerComponent {
  tabs: any[] = [];
  @ViewChild(MatTabGroup) tabGroup!: MatTabGroup;
  constructor() {}

  addTab(label: string, columnDefs: any[], tableRows: any[]) {
    this.tabs.push({
      label: label,
      columnDefs: columnDefs,
      tableRows: tableRows,
    });
    if (this.tabGroup) {
      this.tabGroup.selectedIndex = this.tabs.length - 1;
    }
  }

  removeTab(label: string) {
    this.tabs = this.tabs.filter((tab) => tab.label !== label);
  }
}
