import { Component, ElementRef, Input, ViewChild } from '@angular/core';

import { Data, Layout } from 'plotly.js-dist-min';

declare let Plotly: any;

@Component({
  selector: 'app-chart-wrapper',
  templateUrl: './chart-wrapper.component.html',
  styleUrls: ['./chart-wrapper.component.css'],
})
export class ChartWrapperComponent {
  @Input() data!: any[];
  @Input() layout!: any[];
  @ViewChild('Chart') chartContainer!: ElementRef;

  ngAfterViewInit() {
    const chartElement = this.chartContainer.nativeElement;
    Plotly.newPlot(
      chartElement,
      this.data as Data[],
      this.layout as Partial<Layout>
    );
  }

}
