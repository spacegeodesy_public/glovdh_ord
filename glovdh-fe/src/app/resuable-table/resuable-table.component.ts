import { ChangeDetectorRef, Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-resuable-table',
  templateUrl: './resuable-table.component.html',
  styleUrl: './resuable-table.component.css'
})
export class ResuableTableComponent {

  @Input() data: any[][] = [];
  @Input() columnNames: string[] = [];
  @Input() showSubsetColumns?: string[] = undefined
  @Input() columnUnits?: string[] = undefined;
  @Input() name: string = 'query';
  @Input() pageSizes: number[] = [10, 25, 50];
  

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable) sourceTable!: MatTable<any>;
  @ViewChild(MatSort) sort!: MatSort;

  public dataSource = new MatTableDataSource<any>([]);
  public columnObjects : any[]= [];
  loaded = false;

  public StationListShortener = '...'
  public StatusOK = 'OK'
  public StatusError = 'Error'
  public viewColumnNames: any = {
    'stations': 'Stations',
    'station': 'Station',
    'session_id': 'Session',
    'session': 'Session',
    'epoch': 'Epoch',
    'time_start': 'Start',
    'time_end': 'End',
    'dbc': 'DBC',
    'operation_center': 'Op. Center',
    'corr': 'CORR',
    'subm': 'SUBM',
    'session_type': 'Type',
    'source': 'Source',
    'scans': 'Scans',
    'observations': 'Observations',
    'source_file': 'Source File',
    'file_url': 'Source File',
    'parsed_date': 'Parsed Date',
    'last_modified': 'Last Modified',
    'correlation': 'Corr.',
    'w_mean_mjd': 'Mean MJD',
    'first_mjd': 'First MJD',
    'last_mjd': 'Last MJD',
    'num_sessions': '#Sessions',
    'num_delays': '#Delays',
    'num_rates': '#Rates',
    'analyst_center': 'Analyst Center',
    'program': 'Program',
    'error': 'Status',
    'UT1UTC': 'UT1-UTC',
    'sig_UT1UTC': 'sig_UT1-UTC',
  }  
  constructor(private cdr: ChangeDetectorRef,
    private apiService: ApiServiceService
  ) {}

  ngOnInit(): void {


    let stationIndex = this.columnNames.indexOf('stations');

    if (stationIndex !== -1) {
      this.data = this.data.map((row) => {

      let newRow = row.slice();
      const pairs: string[] = [];
      let stations = row[stationIndex];
      for (let i = 0; i < stations.length; i += 2) {
          const pair = stations.slice(i, i + 2);
          pairs.push(pair);
        }
        newRow[stationIndex] = pairs;
        return newRow;
      });
    }

    if(this.showSubsetColumns){
      this.data = this.data.map((row) => {
        return this.showSubsetColumns!.map((colName) => {
          return row[this.columnNames.indexOf(colName)];
        })
      }
      )
    }

    let cols = this.showSubsetColumns || this.columnNames;

    this.columnObjects = cols.map((colName) => {
      const idx = this.columnNames.indexOf(colName);
      return {
        columnDef: colName,
        header: colName,
        cell: (element: any) => {
          const value = element[idx];
          return typeof value === 'number' ? value.toFixed(4) : '-';
        },
        rawValue: (element: any) => element[idx]
      };
    });

  }
  
  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator!;
    this.dataSource.sort = this.sort!;
    this.dataSource.data = this.toKeyValueData(this.data, this.showSubsetColumns || this.columnNames);
    this.loaded = true;

    this.cdr.detectChanges();
  }


  errorToStatus(error? : string){
    if(!error){
      return this.StatusOK
   }
    return  this.StatusError
  }

  getStatusClass(status: string){
    return status === this.StatusOK ? 'status-ok' : 'status-error';
  }

  format(value: any){

    if (value === null || value === undefined) {
      return '-';
    }
    if (typeof value === 'number') {
      if (Number.isInteger(value)) {
          return value;
      } else {
          return value.toFixed(4);
      }
  }  
    return value
  }


  toKeyValueData(data: any[], columnNames: string[]): any[] {
    let keyValuedata: any[] = data.map(row => {
      let obj: any = {};
      columnNames.forEach((colName, index) => {
        obj[colName] = row[index];
      });
      return obj;
    });
    return keyValuedata;
  }
  


  ngOnChanges(changes: SimpleChanges) {
    if (changes['data']) {
      let kvData = this.toKeyValueData(changes['data'].currentValue, this.showSubsetColumns || this.columnNames);
      this.dataSource.data = kvData; 
    }
    if (changes['name']) {
      this.name = changes['name'].currentValue;
    }
  }
  

  applySourceTableFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    this.sourceTable.renderRows();
  }

  onFilenameClick(filename: string): void {
    this.apiService.queryFiles({ filename: filename, limit: 1 })
    .subscribe((response:any) => {
      if (response.rows && response.rows.length > 0) {
        const fileUrl = response.rows[0][response.columns.indexOf('file_url')];
        if (fileUrl) {
          window.open(fileUrl, '_blank');
        } else {
          console.error('File URL not found in the response');
        }
      } else {
        console.error('No rows found in the response');
      }
    }, error => {
      console.error('Error fetching file URL', error);
    });
  }

  getColumnHeader(columnName: string, index: number): string {
    const columnUnit = this.columnUnits ? this.columnUnits[index] : '';
    const displayName = this.viewColumnNames[columnName] || columnName;
    return columnUnit ? `${displayName}${' ('+columnUnit+')'}` : displayName;
  }

  download() {
    const replacer = (value: any) => (value === null ? '' : value);

    const arrayToStringOrValue = (value: any): any => {
      if (Array.isArray(value)) {
        return value.join('');
      }
      return value;
    };
    let cols =  this.showSubsetColumns || this.columnNames
    const header = cols.map((colName, idx) => this.getColumnHeader(colName, idx));

    const csv = this.data.map((row: any) => {
      let rowCopy = [...row];
      rowCopy = rowCopy.map((value) => arrayToStringOrValue(replacer(value)));
      return rowCopy.join(',');
    });
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const a = document.createElement('a');
    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);

    a.href = url;

    a.download = this.name+'.csv';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }

}
