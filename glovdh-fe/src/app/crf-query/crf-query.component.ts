import { Component, ElementRef, ViewChild } from '@angular/core';
import { TabHandlerComponent } from '../tab-handler/tab-handler.component';
import { ChartHandlerComponent } from '../chart-handler/chart-handler.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiServiceService } from '../api-service.service';
import { AppConstants } from '../consts';
import { QueryViewComponent } from '../query-view/query-view.component';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { Query } from '../eop-query/eop-query.component';


@Component({
  selector: 'app-crf-query',
  templateUrl: './crf-query.component.html',
  styleUrl: './crf-query.component.css',
})
export class CrfQueryComponent {
  private ALL = 'all';
  public analystCenters: string[] = [];
  public selectedAnalystCenter: string | null = null;
  public sourceFiles: string[] = [];
  public selectedSourceFile: string | null = null;

  private queryPrefix = 'Q#';

  public selectedQuery: Query | undefined | null;
  public queryTag: string = ''
  private userChangedQueryTag = false;

  public loading = false;


  @ViewChild(QueryViewComponent) queryHandler!: QueryViewComponent;

  public availableCharTypes = ['Uncertainty Histogram', 'Mollweide Projection'];
  public chartTag: string = this.generateNextChartTag();
  private chartPrefix = 'C#';
  private userChangedChartTag = false;

  public selectedChartType: string = this.availableCharTypes[0];

  @ViewChild('Graph') Graph!: ElementRef;
  @ViewChild(TabHandlerComponent) tabsHandler!: TabHandlerComponent;

  constructor(
    private apiService: ApiServiceService,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.apiService
      .getAgencies({ project: 'crf' })
      .subscribe({next: (response: any) => {
        this.analystCenters = response.data;
        this.selectedAnalystCenter = this.analystCenters[0];

        this.updateQueryOptions();
      },
      error: (error: any) => {
        this.showErrorDialog('Error occured when fetching agencies');
        this.loading = false;
      }
    });
  }

  generateNextChartTag(): string {
    let candidateTag = this.chartTag;
    if (!this.userChangedChartTag) {
      candidateTag =
        this.chartPrefix +
        this.selectedQuery?.title.replace(this.queryPrefix, '');
    }

    return this.chartTag;
  }

  showErrorDialog(errorMessage: string): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { message: errorMessage },
    });
  }

  updateQueryOptions() {
    this.loading = true;

    this.apiService
      .getCrfQueryOptions({
        analyst_center: this.selectedAnalystCenter
      })
      .subscribe({
        next: (response: any) => {
          this.sourceFiles = response.data;
          this.selectedSourceFile = this.sourceFiles[0];
          this.generateNextQueryTag();
          this.loading = false;
        },
        error: (error: any) => {
          this.sourceFiles = ['Error Getting Source Files'];
          this.loading = false;
          this.showErrorDialog('An Error occured when fetching source files:(');
        },
      }),
      (error: any) => {
        this.analystCenters = ['Error Getting Agencies'];
        this.loading = false;
      };
  }

  notifyChartRemoved() {}

  onAnalystAgencyChange(ac: string) {
    this.selectedAnalystCenter = ac;
    this.selectedSourceFile = null;
    this.updateQueryOptions();
  }

  runQuery() {
    this.loading = true
    try {
      this.queryHandler.notifyQueryStarted();
      let requestBody = {
        ...(this.selectedAnalystCenter !== 'all' && {
          analyst_center: this.selectedAnalystCenter,
        }),
        ...(this.selectedSourceFile !== 'all' && {
          filename: this.selectedSourceFile,
        }),
        all_fields: true
      };
      this.apiService.queryCRF(requestBody).subscribe({
        next: (response: any) => {
          try {

            let queryMeta = {
              analystAgency: this.selectedAnalystCenter!,
              sourceFile: this.selectedSourceFile!,
            };

            let query = {
              title: this.queryTag,
              units: response.units,
              data: response.rows,
              query: queryMeta,
              columns: response.columns,
            };
            this.queryHandler.addQuery(query);

            this.userChangedQueryTag = false;
            this.queryTag = this.generateNextQueryTag();
            this.loading = false
          } catch (error) {
            this.loading = false
            this.queryHandler.notifyQueryEnded();
            this.showErrorDialog('Error during running query :(');
          }
        },
        error: (error: any) => {
          this.loading = false
          this.queryHandler.notifyQueryEnded();
          this.showErrorDialog('Error during running query :(');
        },
      }),
        (error: any) => {
          this.loading = false
          this.queryHandler.notifyQueryEnded();
          this.showErrorDialog('Error during running query :(');
          this.sourceFiles = [this.ALL];
        };
    } catch (error) {
      this.loading = false
      this.queryHandler.notifyQueryEnded();
      this.showErrorDialog('Error during running query :(');
    }
  }


  chartMakerChanged() {}
  queryTagChanged() {
    this.userChangedQueryTag = true;
  }

  queryChanged() {
    this.queryTag = this.generateNextQueryTag();
  }

  generateNextQueryTag(): string {
    let candidateTag = this.queryTag;
    if (!this.userChangedQueryTag) {
      let fnamePart = this.selectedSourceFile;
      if (fnamePart != null) {
        let fnameParts = this.selectedSourceFile!.split('/');
        fnamePart = fnameParts[fnameParts.length - 1].split('.')[0];
      }
      candidateTag =
        this.queryPrefix + fnamePart;
    }
    let v = 1;
    let versionedTag = candidateTag;
    while (this.queryHandler.queries.map((q) => q.title).includes(versionedTag)) {
      versionedTag = candidateTag + '#' + v;
      v += 1;
    }
    this.queryTag = versionedTag;
    return this.queryTag;
  }
}
