import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrfQueryComponent } from './crf-query.component';

describe('CrfQueryComponent', () => {
  let component: CrfQueryComponent;
  let fixture: ComponentFixture<CrfQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrfQueryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CrfQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
