import { Injectable } from '@angular/core';
import { ApiServiceService } from './api-service.service';
import { firstValueFrom } from 'rxjs';


export interface Examples {
  title: string;
  description: string;
  code: string;
}

export interface Chapter {
  title: string;
  subchapters: Examples[];
}

@Injectable({
  providedIn: 'root'
})
export class ApiExamplesContentService {
  private chapters: Chapter[] = [];
  private initializationPromise: Promise<void> | null = null;
  private initialized: boolean = false;

  constructor(private apiService: ApiServiceService) {}

  async init() {
    if (!this.initialized) {
      if (!this.initializationPromise) {
        this.initializationPromise = this.setup();
      }
      await this.initializationPromise;
      this.initialized = true;
    }
    return this;
  }

  private async setup() {
    this.chapters = [];
    try {
      const response : any = await firstValueFrom(this.apiService.getAPIExamples());
      let chapterLookup: { [key: string]: Chapter } = {};

      for (let example of response.examples) {
        let chapter = chapterLookup[example.chapter];
        if (!chapter) {
          chapter = {
            title: example.chapter,
            subchapters: []
          };
          chapterLookup[example.chapter] = chapter;
          this.chapters.push(chapter);
        }
        let newExample: Examples = {
          title: example.title,
          description: example.description,
          code: example.code
        }
        chapter.subchapters.push(newExample);
      }
    } catch (error) {
      console.error(error);
      this.chapters = [
        {
          title: "Error Getting Examples",
          subchapters: []
        }
      ];
    }
  }

  getChapters(): Chapter[] {
    if (!this.initialized) {
      throw new Error('Service not initialized. Call init() first.');
    }
    return this.chapters;
  }
}