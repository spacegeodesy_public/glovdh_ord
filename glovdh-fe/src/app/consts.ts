export class AppConstants {
  public static API_ENDPOINT = 'https://glovdh.ethz.ch/';

  public static EOP = 'eop';
  public static CRF = 'crf';
}
