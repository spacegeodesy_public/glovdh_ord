import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonNavigationComponent } from './button-navigation/button-navigation.component';
import { EopQueryComponent } from './eop-query/eop-query.component';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { UrlLastPartPipe } from './url-last.pipe';
import { provideNativeDateAdapter } from '@angular/material/core';
import { TabHandlerComponent } from './tab-handler/tab-handler.component';
import { ChartHandlerComponent } from './chart-handler/chart-handler.component';
import { CrfQueryComponent } from './crf-query/crf-query.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { StationViewComponent } from './station-view/station-view.component';
import {MatSortModule} from '@angular/material/sort';
import { SourceViewComponent } from './source-view/source-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QueryViewComponent } from './query-view/query-view.component';
import { ChartWrapperComponent } from './chart-wrapper/chart-wrapper.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SessionViewComponent } from './session-view/session-view.component';
import { SessionQueryComponent } from './session-query/session-query.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import {MatCardModule} from '@angular/material/card';
import { ResuableTableComponent } from './resuable-table/resuable-table.component';
import { GeneralTabComponent } from './general-tab/general-tab.component';
import { ApiExamplesComponent } from './api-examples/api-examples.component';
import { ApiExamplesContentComponent } from './api-examples-content/api-examples-content.component';
import { ApiExamplesSidenavComponent } from './api-examples-sidenav/api-examples-sidenav.component';
import { CodeHighlightComponent } from './code-highlight/code-highlight.component';

@NgModule({
  declarations: [
    ButtonNavigationComponent,
    AppComponent,
    EopQueryComponent,
    UrlLastPartPipe,
    TabHandlerComponent,
    ChartHandlerComponent,
    CrfQueryComponent,
    ChartWrapperComponent,
    StationViewComponent,
    SourceViewComponent,
    QueryViewComponent,
    SessionViewComponent,
    SessionQueryComponent,
    ErrorDialogComponent,
    ResuableTableComponent,
    GeneralTabComponent,
    ApiExamplesComponent,
    ApiExamplesContentComponent,
    ApiExamplesSidenavComponent,
    CodeHighlightComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    HttpClientModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatCardModule,
    MatTabsModule,
    MatExpansionModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSidenavModule,
    NgxMatSelectSearchModule,
    FormsModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken', // Adjust if your cookie name is different
      headerName: 'X-CSRFToken' // Adjust if your header name is different
    })
  ],
  providers: [provideNativeDateAdapter()],
  bootstrap: [AppComponent]
})
export class AppModule { }
