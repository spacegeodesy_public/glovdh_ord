import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Data } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { QueryResult, Session } from '../models';
import { FormControl } from '@angular/forms';



declare let Plotly: any;

@Component({
  selector: 'app-source-view',
  templateUrl: './source-view.component.html',
  styleUrl: './source-view.component.css',
  animations: [
    trigger('detailExpand', [
      state('collapsed,void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class SourceViewComponent {

  public sessionData: any[] = [];
  public sessionDataColumnNames : string[] = [];

  public expandedElement: any = null;
  public sourceName = '';
  public iersName = '';
  public IVSName = '';
  public J2000Long = '';
  public J2000Short = '';
  public sessions: Session[] = [];
  public rawSessions: any[] = [];
  public now = new Date(Date());
  public loadingPage = true;
  public errorGettingSource = false;
  private earliestDate = new Date('1950-01-01')
  private latestDate = new Date();
  public timeFilterStart = new FormControl(this.earliestDate);
  public timeFilterEnd = new FormControl(this.latestDate);
  private stationStatsQuery?: QueryResult = undefined
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService,
    public changeDetectorRef: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}



  ngOnInit() {
    this.latestDate.setFullYear(this.latestDate.getFullYear() + 2);


    this.route.params.subscribe((params) => {
      {
        this.sourceName = params['id'];
        let data = this.apiService.sourceView(this.sourceName).subscribe({
          next: (data: any) => {
            try {
              this.rawSessions = data.sessions;

              this.iersName = data.info.iers_name || data.info.ivs_name;
              this.J2000Long = data.info.j2000_name_long;
              this.J2000Short = data.info.j2000_name_short;
              this.IVSName = data.info.ivs_name;
              

              let sessionIndex = data.sessions.columns.indexOf('session_id');
              let programIndex = data.sessions.columns.indexOf('program');
              let startIndex = data.sessions.columns.indexOf('time_start');

              this.sessionDataColumnNames = ['session_id', 'program', 'time_start'];


              this.sessions = data.sessions.rows.map((row: any[]) => ({
                session: row[data.sessions.columns.indexOf('session_id')],
                program: row[data.sessions.columns.indexOf('program')],
                start: new Date(row[data.sessions.columns.indexOf('time_start')]),
                status: this.statusFromTime(
                  row[data.sessions.columns.indexOf('time_start')],
                  row[data.sessions.columns.indexOf('time_end')]
                ),
                network: this.networkStringToList(row[data.sessions.columns.indexOf('stations')]),
                sessionType: row[data.sessions.columns.indexOf('session_type')],
              }));
              
              this.sessionData = this.sessions.map((session: Session) => (
                [session.session, session.program, session.start]
              ));
              this.apiService.sourceStatsView(params['id']).subscribe(
                {
                  next: (data: any) => {
                    this.stationStatsQuery = data
                    this.plotScansObsBarChart(data, this.timeFilterStart.value!, this.timeFilterEnd.value!);
                  },
                  error: (error: string) => {
                    console.log(error)
                    this.showErrorDialog('An Error Occured Loading when the Page:(');
                    this.loadingPage = false;
                    this.errorGettingSource = true;
                  },
                }
              )
  
              if (data.sessions.rows.length > 0) {
                this.plotSessionPieChart(this.sessions);
                this.plotSessionCountBarChart(this.sessions)
              }
              if (data.position.rows.length > 0) {
                this.plotCRF(data.position);
              }
              this.loadingPage = false;
            } catch (error) {
              console.log(error)
              this.showErrorDialog('An error occured when loading the page:(');
              this.loadingPage = false;
            }
          },
          error: (error: string) => {
            
            console.log(error)
            this.showErrorDialog('An error occured when loading the page:(');
            this.loadingPage = false;
            this.errorGettingSource = true;
          },
        });
      }
    });
  }

  
  statusFromTime(start: string, end: string) {
    let startDate = new Date(start);
    let endDate = new Date(end);

    if (endDate < this.now) {
      return 'Completed';
    } else if (endDate > this.now && startDate < this.now) {
      return 'In Progress';
    }
    return 'Planned';
  }

  networkStringToList(network: string) {
    const twoLetterCodes = [];

    for (let i = 1; i < network.length; i += 2) {
      const code = network[i - 1] + network[i];
      twoLetterCodes.push(code);
    }
    return twoLetterCodes;
  }

  showErrorDialog(errorMessage: string): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { message: errorMessage },
    });
  }


  plotCRF(positionData: any) {
    if (positionData.rows.length <= 0) {
      return;
    }

    let positionTraces = positionData.rows.map((row: any[]) => {
      const ascension = row[positionData.columns.indexOf('RA')];
      const declination = row[positionData.columns.indexOf('DEC')];
      const filename = row[positionData.columns.indexOf('source_file_id')];

      return {
        x: [ascension],
        y: [declination],
        mode: 'markers',
        type: 'scatter',
        name: filename,
        marker: {
          symbol: 'cross',
        },
      };
    });

    let errorTraces = positionData.rows.map((row: any[]) => {
      const ascensionError = row[positionData.columns.indexOf('sig_RA')];
      const declinationError = row[positionData.columns.indexOf('sig_DEC')];
      const filename = row[positionData.columns.indexOf('source_file_id')];

      return {
        x: [ascensionError],
        y: [declinationError],
        mode: 'markers',
        type: 'scatter',
        name: filename,
        marker: {
          symbol: 'cross',
        },
      };
    });

    var posLayout = {
      title: 'Position Estimates',
      showlegend: true,
      automargin: true,
      xaxis: {
        title: 'Ascension',
        tickformat: '.5f',
      },
      yaxis: {
        title: 'Declination',
        tickformat: '.5f',
      },
    };

    let errLayout = {
      title: 'Error Estimates',
      showlegend: true,
      automargin: true,
      xaxis: {
        title: 'Right Ascension',
      },
      yaxis: {
        title: 'Declination',
      },
    };

    Plotly.newPlot('crfPosChart', positionTraces, posLayout);
    Plotly.newPlot('crfErrChart', errorTraces, errLayout);
  }

  plotSessionPieChart(sessions: Session[], update=false) {
    const programCounts: { [key: string]: number } = {};

    sessions.forEach((session: Session) => {
      let program = session.program;
      programCounts[program] =
        (programCounts[program] || 0) + 1;
    });

    const sortedProgramCounts = Object.keys(programCounts).sort(
      (a, b) => programCounts[b] - programCounts[a]
    );

    const topPrograms = sortedProgramCounts.slice(0, 10);

    const values: number[] = [];
    const labels: string[] = [];

    topPrograms.forEach((program) => {
      labels.push(program);
      values.push(programCounts[program]);
    });

    const plotData: Plotly.Data[] = [
      {
        values: values,
        labels: labels,
        type: 'pie',
        automargin: true,
      },
    ];

    const layout: Partial<Plotly.Layout> = {
      title: 'Program Frequency'
    };
    if(update){
      Plotly.react('sessionTypeChart', plotData, layout);

    }
    else{
      Plotly.newPlot('sessionTypeChart', plotData, layout);
    }
  }


  countProgramsPerYear(sessions: Session[]): {
    sessionCounts: { [key: number]: { [key: string]: number } }
  } {
    const sessionCounts: { [key: number]: { [key: string]: number } } = {};
  
    sessions.forEach((session) => {
      const program = session.program;
      const year = session.start.getFullYear();

  
      if (!sessionCounts[year]) {
        sessionCounts[year] = {};
      }
      if (!sessionCounts[year][program]) {
        sessionCounts[year][program] = 0;
      }
      sessionCounts[year][program]++;
    });
    
    return {
      sessionCounts
    };
  }
  

  getStackBarPlotTraces(sessionCounts: {
    [key: number]: { [key: string]: number };
  }): any[] {
    const years: number[] = Object.keys(sessionCounts).map(Number);
    const minYear: number = Math.min(...years);
    const maxYear: number = Math.max(...years);

    const yearRange: number[] = [];
    for (let year = minYear; year <= maxYear; year++) {
      yearRange.push(year);
    }

    const traces: any[] = [];
    const programs = Array.from(new Set(this.sessions.map((session: Session) => session.program))).sort();
    for (const program of programs) {
      const yearlyProgramCounts = [];
      let foundValidValue = false;
      for (let year = minYear; year <= maxYear; year++) {
        let yearData = sessionCounts[year];
        if (!yearData) {
          yearlyProgramCounts.push(0);
          continue;
        }
        let sessionsForProgram = sessionCounts[year][program] ?? 0;
        if (sessionsForProgram != 0) {
          foundValidValue = true;
        }
        yearlyProgramCounts.push(sessionsForProgram);
      }
      if (foundValidValue) {
        traces.push({
          x: yearRange,
          y: yearlyProgramCounts,
          name: `${program}`,
          type: 'bar',
        });
      }
    }
    return traces;
  }

  plotSessionCountBarChart(sessions: Session[], update=false){
    let results = this.countProgramsPerYear(sessions);
    let aggregatedCounts = results.sessionCounts;


    let traces = this.getStackBarPlotTraces(aggregatedCounts);

    var layout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Sessions per Year',
      xaxis: {
        tickvals: Object.keys(aggregatedCounts).map(Number).sort(),        
        title: 'Years'               
      },
    };
    if(update){
      Plotly.react('activityChart', traces, layout);
    }else{
      Plotly.newPlot('activityChart', traces, layout);

    }

  }

  
  updateTimeFilters(){
    if(this.timeFilterStart.value! >= this.earliestDate && this.timeFilterEnd.value! <= this.latestDate )
      {
        this.plotScansObsBarChart(this.stationStatsQuery!,
         this.timeFilterStart.value!, 
         this.timeFilterEnd.value!,
          true
         );

        let filteredSessions = this.sessions.filter((session: Session) =>
          session.start >= this.timeFilterStart.value! && session.start <= this.timeFilterEnd.value!
        )

         this.plotSessionPieChart(filteredSessions, true);
         this.plotSessionCountBarChart(filteredSessions, true);
      }
  }

  countObsScansPerYear(sessions: QueryResult, minDate: Date, maxDate: Date): {
    obsCounts: { [key: number]: { [key: string]: number } },
    scanCounts: { [key: number]: { [key: string]: number } }
  } {
    const obsCounts: { [key: number]: { [key: string]: number } } = {};
    const scanCounts: { [key: number]: { [key: string]: number } } = {};
    
    let sessionMap : {[key: string]: Session}= {}
    this.sessions.forEach((session) => {
    
      sessionMap[session.session] = session ;
    })

    sessions.rows.forEach((queryRow: any) => {
      
      const program = sessionMap[queryRow[sessions.columns.indexOf('session_id')]]?.program;
      if(!program){
        return
      }
      const scans = queryRow[sessions.columns.indexOf('scans')];
      const obs = queryRow[sessions.columns.indexOf('observations')];
      let date = sessionMap[queryRow[sessions.columns.indexOf('session_id')]]?.start
      if(date < minDate || date > maxDate){
        return
      }
      const year = date.getFullYear();

      if (!obsCounts[year]) {
        obsCounts[year] = {};
      }
      if (!scanCounts[year]) {
        scanCounts[year] = {};
      }

      if (!obsCounts[year][program]) {
        obsCounts[year][program] = 0;
      }
      if (!scanCounts[year][program]) {
        scanCounts[year][program] = 0;
      }

      obsCounts[year][program] = obsCounts[year][program] + obs;
      scanCounts[year][program] = scanCounts[year][program] + scans;
    });
  
  
    return {
      obsCounts,
      scanCounts
    };
  }

  plotScansObsBarChart(scansAndObs: QueryResult, minDate: Date, maxDate: Date, update=false){
    let aggData = this.countObsScansPerYear(scansAndObs, minDate, maxDate);
    let scanTraces = this.getStackBarPlotTraces(aggData.scanCounts);
    let obsTraces = this.getStackBarPlotTraces(aggData.obsCounts);

    var scansLayout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Number of Scans Per Year',
      xaxis: {
        tickvals: Object.keys(aggData.scanCounts).map(Number).sort(),        
        title: 'Years'               
      },
    };

    var obsLayout = {
      barmode: 'stack',
      showlegend: true,
      title: 'Number of Observations Per Year',
      xaxis: {
        tickvals: Object.keys(aggData.obsCounts).map(Number).sort(),        
        title: 'Years'               
      },
    };

    Plotly.newPlot('scansBarPlot', scanTraces, scansLayout);
    Plotly.newPlot('obsBarPlot', obsTraces, obsLayout);
  }

}
