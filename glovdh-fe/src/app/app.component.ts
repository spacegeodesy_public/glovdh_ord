import { Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ApiServiceService } from './api-service.service';
import {
  BehaviorSubject,
  ReplaySubject,
  Subject,
  debounceTime,
  delay,
  filter,
  map,
  takeUntil,
  tap,
} from 'rxjs';
import { Router } from '@angular/router';

interface SearchResult {
  type: string;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  protected results: SearchResult[] = [];

  selectedItem: SearchResult | undefined;

  /** control for filter for server side. */
  public searchFilterCtrl: FormControl = new FormControl();

  /** indicate search operation is in progress */
  public searching = false;

  /** list of banks filtered after simulating server side search */
  public filteredResults: ReplaySubject<SearchResult[]> = new ReplaySubject<
    SearchResult[]
  >(1);

  @ViewChild('singleSelect', { static: true }) singleSelect:
    | MatSelect
    | undefined;

  title = 'glovdh-fe';
  constructor(private apiService: ApiServiceService, private router: Router) {}

  protected _onDestroy = new Subject<void>();

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit() {
    // listen for search field value changes
    this.searchFilterCtrl.valueChanges
      .pipe(
        filter((search) => !!search),
        tap((search) => {
          this.searching = true;
        }),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(async (search) => {
          if (!this.searchFilterCtrl.value) {
            return [];
          }

          let data: any = await this.apiService
            .freeSearch({ searchword: search })
            .toPromise();

          this.results = data.stations
            .map((station: string) => ({ type: 'station', name: station }))
            .concat(
              data.analystCenters.map((center: string) => ({
                type: 'analystCenter',
                name: center,
              }))
            )
            .concat(
              data.programmes.map((programme: string) => ({
                type: 'programme',
                name: programme,
              }))
            )
            .concat(
              data.sessions.map((session: string) => ({
                type: 'session',
                name: session,
              }))
            )
            .concat(
              data.sources.map((source: string) => ({
                type: 'source',
                name: source,
              }))
            );

          return this.results;
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(
        async (results) => {
          this.searching = false;

          this.filteredResults.next(await results); // Cast 'results' to 'SearchResult[]'
        },
        (error) => {
          // no errors in our simulated example
          this.searching = false;
          // handle error...
        }
      );
  }

  onOptionClick(item: SearchResult) {
    this.router.navigate(['/' + item.type + '/' + item.name]);
  }
}
1;
