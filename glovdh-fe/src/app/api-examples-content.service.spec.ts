import { TestBed } from '@angular/core/testing';

import { ApiExamplesContentService } from './api-examples-content.service';

describe('ApiExamplesContentService', () => {
  let service: ApiExamplesContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiExamplesContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
