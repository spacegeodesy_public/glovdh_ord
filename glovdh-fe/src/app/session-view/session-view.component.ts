import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { QueryResult } from '../models';
import { UntypedFormArray } from '@angular/forms';

interface StationData {
  scans: number;
  observations: number;
  session_id: string;
  station: string;
}
declare let Plotly: any;
@Component({
  selector: 'app-session-view',
  templateUrl: './session-view.component.html',
  styleUrl: './session-view.component.css',
})
export class SessionViewComponent {

  public session_id: string = '';
  public loadingPage = true;
  public session : {[key: string]: string} = {};
  public stations : {[key: string]: string}[] = [];
  public sources : {[key: string]: string}[]= [];
  public baselines : {[key: string]: string}[]= [];

  public sourceTableData: any[][] = []
  public sourceTableColumnNames = ['source', 'scans', 'observations'];


  public eopQuery?: QueryResult = undefined
  public eopTableColumnNames = ['XPO', 'YPO', 'UT1UTC', 'dX', 'dY', 'source_file']


  constructor(
    private activeRouter: ActivatedRoute,
    private router: Router,
    private apiService: ApiServiceService,
    private dialog: MatDialog
  ) {}


  ngOnInit(): void {
    this.activeRouter.params.subscribe((params) => {
      this.session_id = params['id'];
      this.getAndSetupSessionStats();
      this.getAndSetupEopTable();
    });
  }

  getAndSetupEopTable(){
    let data = this.apiService.queryEOP({
      session_id: this.session_id,
      all_fields: true
    }).subscribe({
      next: (data: any) => {

        this.eopQuery = data;
      },
      error: (message: any) => {
        console.log(message)
        this.dialog.open(ErrorDialogComponent, {
          data: { message: 'An error occured when loading the page:(' },
        });
      },
    })
  }


  getAndSetupSessionStats(){
    let data = this.apiService.sessionStats(this.session_id).subscribe({
      next: (data: any) => {
        try {
          this.session = data.session;
          this.sources = data.sources;
          this.setupSourceTable(this.sources)
          this.stations = data.stations;
          this.baselines = data.baselines;

          this.createObservationsHeatmapAndGeoMap(data.baselines);
          this.plotBarPlots(data.stations);
        } catch (error) {
          console.log(error)
          this.dialog.open(ErrorDialogComponent, {
            data: { message: 'An error occured when loading the page:(' },
          });
        }
        this.loadingPage = false;
      },
      error: (message: any) => {
        this.loadingPage = false;
        console.log(message)
        this.dialog.open(ErrorDialogComponent, {
          data: { message: 'An error occured when loading the page:(' },
        });
      },
    });

  }


  setupSourceTable(sources: any[]){

    let filteredData = sources.map(data_obj =>
      {
        return [data_obj.source,  data_obj.scans, data_obj.observations]
        }
      )

    filteredData = filteredData.sort((a,b) => b[1]-a[1])
    this.sourceTableData = filteredData;


  }

  plotBarPlots(data: StationData[]) {
    if (data.length == 0) {
      return;
    }
    // Extract station names, observations, and scans
    const stations = data.map((d) => d.station);
    const uniqueStations = Array.from(new Set(stations));
    const observations = uniqueStations.map((station) => {
      return data
        .filter((d) => d.station === station)
        .reduce((sum, d) => sum + d.observations, 0);
    });
    const scans = uniqueStations.map((station) => {
      return data
        .filter((d) => d.station === station)
        .reduce((sum, d) => sum + d.scans, 0);
    });

    const traceObservations = {
      x: uniqueStations,
      y: observations,
      name: 'Observations',
      type: 'bar',
    };

    const traceScans = {
      x: uniqueStations,
      y: scans,
      name: 'Scans',
      type: 'bar',
    };

    const plotData = [traceObservations, traceScans];

    const layout = {
      barmode: 'group',

      title: 'Observations and Scans per Station',
      xaxis: {
        title: 'Stations',
      },
      yaxis: {
        title: 'Count',
      },
    };

    // Render the plot
    Plotly.newPlot('barplots', plotData, layout);
  }


  createObservationsHeatmapAndGeoMap(data: any) {
    if (data.length == 0) {
      return;
    }

    // Extract unique stations
    const stations = Array.from(
      new Set(
        data
          .map((d: any) => d.station1)
          .concat(data.map((d: any) => d.station2))
      )
    );

    // Create matrix to hold observations
    const observationsMatrix_ = stations.map((station1) =>
      stations.map((station2) => {
        if(station1 == station2){
          return '-'
        }
        const observation = data.find(
          (d: any) => d.station1 === station1 && d.station2 === station2
        );
        return observation ? observation.observations : '';
      })
    );
    let observationsMatrix: number[][] = [];
    for (let i = 0; i < stations.length; i++) {
      observationsMatrix[i] = new Array<number>(stations.length).fill(0);
    }

    for (let i = 0; i < stations.length; i++) {
      for (let j = i; j < stations.length; j++) {
        observationsMatrix[i][j] = observationsMatrix_[j][i];
        observationsMatrix[j][i] = observationsMatrix_[i][j];
      }
    }



    const trace = {
      z: observationsMatrix,
      x: stations,
      y: stations,
      type: 'heatmap',
      colorscale: 'magma',
      hoverinfo: 'none',
    };

    var layout: Partial<Plotly.Layout> = {
      title: 'Observations',
      annotations: [],
      xaxis: {
        ticks: '',
        side: 'top',
        tickmode: 'array',
        tickvals: stations,
        ticktext: stations.map(
          (station) => `<a href="/station/${station}">${station}</a>`
        ),
        showgrid: false, // Remove grid lines from x-axis
      },
      yaxis: {
        ticks: '',
        ticksuffix: ' ',
        tickmode: 'array',
        tickvals: stations,
        ticktext: stations.map(
          (station) => `<a href="/station/${station}">${station}</a>`
        ),
        showgrid: false, // Remove grid lines from y-axis
      },
    };


    for (var i = 0; i < stations.length; i++) {
      for (var j = 0; j < stations.length; j++) {

        const result: any = {
          xref: 'x1',
          yref: 'y1',
          x: stations[j],
          y: stations[i],
          text: observationsMatrix[i][j],
          font: {
            family: 'Arial',
            size: 12,
            color: 'black',
          },
          showarrow: false,
        };
        layout.annotations!.push(result);
      }
    }

    // Plot
    Plotly.newPlot('observationsHeatmap', [trace], layout);

    this.plotGeoMap(stations as string[], observationsMatrix)
  }

  plotGeoMap(stations: string[], observationMatrix: number[][]) {

    // Call the API to get all stations with all fields
    this.apiService.getAllStations({ all_fields: true }).subscribe({
        next: (data: any) => {

            const codeIndex = data.columns.indexOf("code");

            let stationLookup: any = {};
            data.rows.forEach((row:any) => {
              const code = row[codeIndex];
              stationLookup[code] = row;
            });

            // Map the provided station codes to their corresponding station data using the lookup object
            const filteredStations = stations.map(code => stationLookup[code]);

            // Prepare the data for Plotly

            let latIndex = data.columns.indexOf('latitude');
            let longIndex = data.columns.indexOf('longitude');
            const latitudes = filteredStations.map((row:any) => row[latIndex]);
            const longitudes = filteredStations.map((row:any) => row[longIndex]);

            const names = filteredStations.map(station => station[0]);
            // Create the scatter trace for stations
            const scatterTrace = {
              type: 'scattergeo',
              mode: 'markers',
              lat: latitudes,
              lon: longitudes,
              text: names,
              marker: {
                  size: 8,
                  color: 'red'
              },
              showlegend: false,
              hoverinfo: 'text'
            };


            const layout = {

                geo: {
                    scope: 'world',
                    projection: {
                        type: 'equirectangular'
                    },
                    showland: true,
                    landcolor: 'rgb(217, 217, 217)',
                    subunitwidth: 1,
                    countrywidth: 1,
                    subunitcolor: 'rgb(255,255,255)',
                    countrycolor: 'rgb(255,255,255)'
                },
                title: {
                  text: 'Participating Stations',
              },
                margin: {
                    l: 0,
                    r: 0,
                    t: 50,
                    b: 0
                }
            };

            // Combine scatter trace and line traces
            const allTraces = [scatterTrace];

            Plotly.newPlot('geoMap', allTraces, layout);
        },
        error: (err: any) => {
            console.error('Error fetching station data:', err);
        }
    });
  }
}
