export interface Session {
    session: string; 
    program: string; 
    start: Date; 
    status: string; 
    network: string[];
    sessionType: string; 
  }



export interface QueryResult{
    columns: string[];
    units: string[];
    rows: any[];
}