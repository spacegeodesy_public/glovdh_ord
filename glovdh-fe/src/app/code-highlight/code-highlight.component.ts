// src/app/code-highlight/code-highlight.component.ts
import { Component, Input, AfterViewInit, ElementRef, AfterViewChecked } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var hljs: any;
@Component({
  selector: 'app-code-highlight',
  templateUrl: './code-highlight.component.html',
  styleUrls: ['./code-highlight.component.css']
})
export class CodeHighlightComponent{
  @Input() code: string = '';
  @Input() language: string = 'python';
  private calledHighlight = false;
  constructor(private el: ElementRef,
    private snackBar: MatSnackBar
  ) {
  }

  highlightAll() {
    if(this.calledHighlight){
      return;
    }
    hljs.highlightAll();
    this.calledHighlight = true;
  }
  

  copyCode(codeElement: HTMLElement) {
    const range = document.createRange();
    range.selectNode(codeElement);
    window.getSelection()?.removeAllRanges();
    window.getSelection()?.addRange(range);
    try {
      const successful = document.execCommand('copy');
      if (successful) {
        this.snackBar.open('Code copied to clipboard', 'Close', {
          duration: 1000, 
          horizontalPosition: 'center', 
          verticalPosition: 'bottom', 
        });
      }
    } catch (err) {
      console.log('Unable to copy');
    }
    window.getSelection()?.removeAllRanges();
  }
}
