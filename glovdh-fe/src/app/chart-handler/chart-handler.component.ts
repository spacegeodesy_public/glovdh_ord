import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  Query,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { CrfQueryComponent } from '../crf-query/crf-query.component';
import { QueryViewComponent } from '../query-view/query-view.component';
import { AppConstants } from '../consts';

@Component({
  selector: 'app-chart-handler',
  templateUrl: './chart-handler.component.html',
  styleUrl: './chart-handler.component.css',
})
export class ChartHandlerComponent {
  private colorPalette = [
    '#a6cee3',
    '#fb9a99',
    '#b2df8a',
    '#fdbf6f',
    '#cab2d6',
    '#1f78b4',
    '#e31a1c',
    '#33a02c',
    '#ff7f00',
    '#6a3d9a',
  ];

  public registeredCharts: { [key: string]: any } = {}

  private opacity: number = 0.7;

  @Input() parentContainer!: CrfQueryComponent | QueryViewComponent;

  constructor(private cdr: ChangeDetectorRef) {}

  getColor(idx: number, dark = false) {
    let color = this.colorPalette[idx];
    if (dark) {
      // Convert the color to RGB components
      const rgb = parseInt(color.substring(1), 16);
      const r = (rgb >> 16) & 0xff;
      const g = (rgb >> 8) & 0xff;
      const b = (rgb >> 0) & 0xff;

      // Darken the color by reducing each RGB component
      const darkR = Math.max(r - 30, 0); // Adjust this value for the darkness level
      const darkG = Math.max(g - 30, 0); // Adjust this value for the darkness level
      const darkB = Math.max(b - 30, 0); // Adjust this value for the darkness level

      // Convert the darker RGB components back to hexadecimal
      color = `#${((darkR << 16) | (darkG << 8) | darkB).toString(16)}`;
    }
    return `${color}${Math.round(this.opacity * 255).toString(16)}`;
  }

  addConfidenceChart(
    xData: string[],
    yData: number[],
    confidence: number[],
    title: string,
    chartName: string
  ) {
    const concatenatedX = xData.concat(xData.slice().reverse());

    const upperBound = yData.map((y, i) => y + confidence[i]);
    const lowerBound = yData.map((y, i) => y - confidence[i]);
    const concatenatedBounds = upperBound.concat(lowerBound.slice().reverse());

    var confTrace = {
      x: concatenatedX,
      y: concatenatedBounds,
      fill: 'tozerox',
      fillcolor: this.getColor(0),
      line: { color: 'transparent', width: 20 },
      name: title,
      showlegend: false,
      type: 'scatter',
    };

    var meanTrace = {
      x: xData,
      y: yData,
      line: { color: this.getColor(0, true) },
      mode: 'lines',
      name: title,
      type: 'scatter',
    };

    var layout = {
      title: title,
      paper_bgcolor: 'rgb(240,240,240)',
      xaxis: {
        gridcolor: 'rgb(255,255,255)',
        showgrid: true,
        showline: false,
        showticklabels: true,
        tickcolor: 'rgb(127,127,127)',
        ticks: 'outside',
        zeroline: false,
        type: 'date',
        tickformat: '%m-%d-%Y'
      },
      yaxis: {
        gridcolor: 'rgb(255,255,255)',
        showgrid: true,
        showline: false,
        showticklabels: true,
        tickcolor: 'rgb(127,127,127)',
        ticks: 'outside',
        zeroline: false,
      },
    };

    if (this.registeredCharts[chartName]) {
      let target = {... this.registeredCharts[chartName]};

      let nextColorIdx = target.data.length / 2;
      let confColor = this.getColor(nextColorIdx);
      let darkColor = this.getColor(nextColorIdx, true);
      confTrace.fillcolor = confColor;
      meanTrace.line.color = darkColor;
      target.data.push(confTrace);
      target.data.push(meanTrace);

      target.layout.title = chartName;
      target.title = chartName;
      this.registeredCharts[chartName] = target;
    } else {

      layout.title= chartName;
      let chart = {
        data: [confTrace, meanTrace],
        layout: layout,
        title: chartName,
      };
      this.registeredCharts[chartName] = chart;
    }

    this.cdr.detectChanges();
  }

  to_rad(degrees: number) {
    return (degrees * Math.PI) / 180;
  }

  getRACosDecTrace(declinations: number[], ascencions: number[], RAUncertainties: number[], hovers: string[]){
    const uncertainty_value_cos = RAUncertainties.map(
      (value, index) => value * Math.cos(this.to_rad(declinations[index]))
    );


    let trace = {
      type: 'scattergeo',
      lon: ascencions,
      lat: declinations,
      mode: 'markers',
      text: hovers,
      hoverinfo: 'text', 
      marker: {
        size: 5,
        color: uncertainty_value_cos,
        colorscale: 'magma',
        cmin: 0,
        cmax: 0.00001,
        colorbar: {
          title: 'sig_RA*COS(DEC)',
        },
      },
      showlegend: false,
    };
    return trace
  }

  
  getDecErrorTrace(declinations: number[], ascencions: number[], DECUncertainties: number[], hovers: string[]){


    let trace = {
      type: 'scattergeo',
      lon: ascencions,
      lat: declinations,
      mode: 'markers',
      text: hovers,
      hoverinfo: 'text', 
      marker: {
        size: 5,
        color: DECUncertainties,
        colorscale: 'magma',
        cmin: 0,
        cmax: 0.0005,
        colorbar: {
          title: 'sig_DEC',
        },
      },
      showlegend: false
    };
    return trace
  }

  
  get2DErrorTrace(declinations: number[], ascencions: number[], DECUncertainties: number[], RAUncertainties: number[], hovers: string[]){

    let error2D = DECUncertainties.map(
      (value, index) => Math.sqrt(value ** 2 + RAUncertainties[index] ** 2)
    );


    let trace = {
      type: 'scattergeo',
      lon: ascencions,
      lat: declinations,
      mode: 'markers',
      text: hovers,
      hoverinfo: 'text', 
      marker: {
        size: 5,
        color: error2D,
        colorscale: 'magma',
        cmin: 0,
        cmax: 0.0005,
        colorbar: {
          title: 'SQRT(sig_RA^2 + sig_DEC^2)',
        },
      },
      showlegend: false
    };
    return trace
  }

  addMollweideProjection(
    ascencions: number[],
    declination: number[],
    asc_uncertanity: number[],
    dec_uncertanity: number[],
    hovers: string[],
    title: string
  ) {
    var grid_ra_x = [180, 150, 120, 90, 60, 30];
    var grid_ra_y = [0, 0, 0, 0, 0];
    var grid_ra_hovtext = ['180°', '150°', '120°', '90°', '60°', '30°'];
    var grid_ra_textposition = [
      'middle center',
      'middle center',
      'middle center',
      'middle center',
      'middle center',
      'middle center',
    ];

    var grid_dec_x = [0, 0, 0, 0, 0, 0, 0];
    var grid_dec_y = [30, 60, 90];
    var grid_dec_hovtext = ['+30°', '+60°', '+90°'];
    var grid_dec_textposition = [
      'middle center',
      'middle center',
      'bottom center',
    ];

   
    let traces = [];

    let cosErrorTrace: any = this.getRACosDecTrace(declination, ascencions, asc_uncertanity, hovers);
    let decErrorTrace: any = this.getDecErrorTrace(declination, ascencions, dec_uncertanity, hovers);
    let error2DTrace: any= this.get2DErrorTrace(declination, ascencions, dec_uncertanity, asc_uncertanity, hovers);

    let arGridTrace : any= {
      // Grid RA
      type: 'scattergeo',
      mode: 'text',
      lon: grid_ra_x,
      lat: grid_ra_y,
      text: grid_ra_hovtext,
      hoverinfo: 'none',
      textfont: {
        size: 8,
        color: '#696969',
      },
      textposition: grid_ra_textposition,
      showlegend: false,
      visible: true
    }

    let decGridTrace: any = {
      // Grid Dec
      type: 'scattergeo',
      mode: 'text',
      lon: grid_dec_x,
      lat: grid_dec_y,
      text: grid_dec_hovtext,
      hoverinfo: 'none',
      textfont: {
        size: 8,
        color: '#696969',
      },
      textposition: grid_dec_textposition,
      showlegend: false,
      visible: true
    }

    cosErrorTrace.visible = true;
    decErrorTrace.visible = false;
    error2DTrace.visible = false;


    traces.push(cosErrorTrace);
    traces.push(decErrorTrace);
    traces.push(error2DTrace);
    traces.push(arGridTrace);
    traces.push(decGridTrace);

    let layout = {
      title: title,
      autosize: true,
      geo: {
        projection: {
          type: 'mollweide',
        },
        lonaxis: {
          showgrid: true,
          tick0: 0,
          dtick: 45,
          gridcolor: '#aaa',
          gridwidth: 1,
        },
        lataxis: {
          showgrid: true,
          tick0: 90,
          dtick: 45,
          gridcolor: '#aaa',
          gridwidth: 1,
        },
        showcoastlines: false,
        showland: false,
        showrivers: false,
        showlakes: false,
        showocean: false,
        showcountries: false,
        showsubunits: false,
      },
      updatemenus: [{
        buttons: [
          {
            method: 'update',
            args: [{'visible': [true, false, false, true, true]}],
            label: 'sig_RA*COS(DEC)'
          },
          {
            method: 'update',
            args: [{'visible': [false, true, false, true, true]}],
            label: 'sig_DEC'
          },
          {
            method: 'update',
            args: [{'visible': [false, false, true, true, true]}],
            label: 'sig_2D'
          }
        ],
        direction: 'down',
        showactive: true,
        x: 0.1,
        xanchor: 'left',
        y: 1.1,
        yanchor: 'top'
      }],
      dragmode: false
    };

    let chart = { data: traces, layout: layout, title: title };
    this.registeredCharts[title] = chart;
  }

  addHistPlotMAS(
    asc_uncertainty: number[],
    declination_uncertainty: number[],
    title: string,
    chartName: string
  ) {
    let cut = 0.00005;
    const data = [
      {
        x: asc_uncertainty,
        type: 'histogram',
        name: 'Right Ascension Uncertainty',
        histfunc: 'density',
        opacity: 0.7,
        marker: {
          color: this.getColor(1),
        },
        xbins: {
          end: cut,
          size: 0.000001,
          start: 0,
        },
      },
      {
        x: declination_uncertainty,
        type: 'histogram',
        name: 'Declination Uncertainty',
        histfunc: 'density',
        opacity: 0.7,
        marker: {
          color: this.getColor(3),
        },
        xbins: {
          end: cut,
          size: 0.000001,
          start: 0,
        },
      },
    ];

    const layout = {
      title: title,
      xaxis: {
        title: 'Uncertainty',
      },
      yaxis: {
        title: 'Count',
      },
      barmode: 'overlay',
      legend: {
        title: 'Uncertainties (capped at ' + String(cut) + ')',
      },
    };

    const chart = {
      data: data,
      layout: layout,
      title: title,
      id: chartName
    };
    this.registeredCharts[chartName] = chart;
  }

  removeChart(chartName: string) {
    delete this.registeredCharts[chartName];
    this.parentContainer.notifyChartRemoved();
  }

  getCharts() {
    return this.registeredCharts
  }
}
