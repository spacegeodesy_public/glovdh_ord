import { ChangeDetectorRef, Component } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

declare let Plotly: any;
@Component({
  selector: 'app-general-tab',
  templateUrl: './general-tab.component.html',
  styleUrl: './general-tab.component.css'
})
export class GeneralTabComponent {

  public stationsData: any[] = [];
  public sourcesData: any[] = [];

  public filesData: any[] = [];
  public filesColumns: string[] = []
  private hideFileColumns : string[] = ['parsed_date']
  public showNFiles = 5;

  constructor(private api: ApiServiceService, private cdr: ChangeDetectorRef) {}

  ngOnInit(){

    this.api.getAllStations({all_fields: true}).subscribe({
      next: (data: any) => {
          const codeIndex = data.columns.indexOf('code');
          this.stationsData = data.rows.map((row: any) => {
            return [row[codeIndex]]
          });

          this.plotMap(data)

      },
      error: (err: any) => { 
        console.log(err)
      }
    }
    )

    this.api.getAllSources().subscribe({
      next: (data: any) => {
          this.sourcesData = data.data.map((source: any) => {
            return [source]
          });
      },
      error: (err: any) => { 
        console.log(err)
      }
    }
    )

    this.api.queryFiles({limit: this.showNFiles, sort_by: '-last_modified'}).subscribe(
      {
        next: (resp: any) => {
          
            this.filesColumns = resp.columns.filter((column: string) => !this.hideFileColumns.includes(column));

            this.filesData = resp.rows.map((row: any[]) => {
              return row.filter((_, index) => !this.hideFileColumns.includes(resp.columns[index]))
            });

        },
        error: (err: any) => { 
          console.log(err)
        }
      }
    )

    this.api.getFileTypeCounts().subscribe(
      {
        next: (data: any) => {
            this.plotFiletypePieChart(data)
        },
        error: (err: any) => { 
          console.log(err)
        }
      }
    )
  }


  plotFiletypePieChart(data: any) {
    let fileTypeIndex = data.columns.indexOf('file_type');
    let countIndex = data.columns.indexOf('count');
    let labels = data.rows.map((row:any) => row[fileTypeIndex]);
    let values = data.rows.map((row:any) => row[countIndex]);

    let skd_index = labels.indexOf('skd');

    if (skd_index !== -1) {
        var labels_wo_skd = labels.slice(0, skd_index).concat(labels.slice(skd_index + 1));
        var values_wo_skd = values.slice(0, skd_index).concat(values.slice(skd_index + 1));
    } else {
        var labels_wo_skd = labels;
        var values_wo_skd = values;
    }

    
    let wo_skd_traces = [{
      labels: labels_wo_skd,
      values: values_wo_skd,
      type: 'pie',
      name: 'File Types',
      textinfo: 'value', 
  }];

    let layout = {
        title: 'Parsed Files In the Database',
        showlegend: true,
        updatemenus: [
            {
                buttons: [
                    {
                        label: 'Exclude skd',
                        method: 'update',
                        args: [{
                            values: [values_wo_skd],
                            labels: [labels_wo_skd],
                        }]
                    },
                    {
                      label: 'Show All',
                      method: 'update',
                      args: [{
                          values: [values],
                          labels: [labels],
                      }]
                  }
                ],
                direction: 'down', // Stack buttons vertically
                showactive: true,
            },

        ]
    };

    // Initialize the pie chart using Plotly
    Plotly.newPlot('filetype-piechart', wo_skd_traces, layout);
}

  
  

  plotMap(data: any) {
    // Extract latitudes, longitudes, and names from stationData


    let latIndex = data.columns.indexOf('latitude');
    let longIndex = data.columns.indexOf('longitude');
    let nameIndex = data.columns.indexOf('name');
    let codeIndex = data.columns.indexOf('code');
    const latitudes = data.rows.map((row:any) => row[latIndex]);
    const longitudes = data.rows.map((row:any) => row[longIndex]);
    const names = data.rows.map((row:any) => `${row[codeIndex]}: ${row[nameIndex]}`);

    // Create the scatter trace for stations
    const scatterTrace = {
        type: 'scattergeo',
        mode: 'markers',
        lat: latitudes,
        lon: longitudes,
        text: names, // Set names as text (hoverinfo will control its display)
        hoverinfo: 'text', // Show only text on hover
        marker: {
            size: 8,
            color: 'purple'
        },
        showlegend: false
    };

    const layout = {
        geo: {
            scope: 'world',
            projection: {
                type: 'equirectangular'
            },
            showland: true,
            landcolor: 'rgb(217, 217, 217)',
            subunitwidth: 1,
            countrywidth: 1,
            subunitcolor: 'rgb(255,255,255)',
            countrycolor: 'rgb(255,255,255)'
        },
        margin: {
            l: 0,
            r: 0,
            t: 0,
            b: 0
        }
    };

    Plotly.newPlot('geoMap', [scatterTrace], layout);
  }


}
