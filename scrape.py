import argparse
import dataclasses
import logging
import re
import time
import os
import sys
from datetime import datetime, timezone
from typing import Optional, Iterable, List

import django
import pymysql

cwd = os.getcwd()
if cwd not in sys.path:
    sys.path.append(cwd)
pymysql.install_as_MySQLdb()

from glovdh_ord.glovdh.logging_tools.utils import setup_logging
from glovdh_ord.glovdh.webscraping.web_utils import InternetDownloader
from glovdh_ord.glovdh.webscraping.parsers.crf_parsers import CRFParserFactory
from glovdh_ord.glovdh.webscraping.parsers.eop_parsers import EopParserFactory
from glovdh_ord.glovdh.webscraping.parsers.master_parsers import MasterFileParserFactory

from glovdh_ord.glovdh.webscraping.consts import MASTERFILE_URL_TEMPLATE, NORMAL_SESSION_URL_TEMPLATE, \
    INTENSIVE_SESSION_URL_TEMPLATE, CRF_FILES_URL_TEMPLATE, TRF_FILES_URL_TEMPLATE, PARSER_MASTER_FILES_MINIMUM_DATE, \
    PARSE_EOP_MINIMUM_DATE, \
    PARSE_TRF_MINIMUM_DATE, PARSE_CRF_MINIMUM_DATE, SOURCE_METADATA_TEMPLATE, STATION_LIST_URL, \
    SOURCE_METADATA_FILE_REGEX, MASTER_FILE_REGEX, EOPI_FILE_REGEX, EOPS_FILE_REGEX, CRF_FILE_REGEX, TRF_FILE_REGEX, \
    SKD_FILE_REGEX
from glovdh_ord.glovdh.webscraping.parsers.skd_parser_adapter import SKDParserFactory
from glovdh_ord.glovdh.webscraping.parsers.source_name_table_parser import SourceNameTableParserFactory
from glovdh_ord.glovdh.webscraping.parsers.stations_name_table_parser import StationParserFactory

from glovdh_ord.glovdh.webscraping.parsers.trf_parsers import TRFParserFactory

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'glovdh_ord.glovdh.settings')
django.setup()
from glovdh_ord.glovdh.webscraping.db_handler import DBHandler
from glovdh_ord.glovdh.models import VLBISession, EOPSolution, CRFSolution, TRFSolution, ScrapedFile, Source, Station, \
    StationStatistic, SourceStatistic, BaselineStatistic, AnalystCenter
from glovdh_ord.glovdh.webscraping.scraping_collections import FileArchiveTarget, StationURLScrapeTarget, \
    SKDFilesTarget
from glovdh_ord.glovdh.webscraping.webagent import WebParserAgent

logger = logging.getLogger(__name__)


def filter_urls_with_regex(urls: Optional[Iterable[str]], regex: str) -> Optional[List[str]]:
    if urls is None:
        return None

    return [url for url in urls if re.match(regex, url)]


def run_scrape(argument_urls: Optional[List] = None):
    if argument_urls is None:
        logger.info('Did not receive argument urls. Starting full scrape...')
    else:
        logger.info(f'Received {len(argument_urls)} urls. Starting scrape only for these urls...')

    db = DBHandler()
    downloader = InternetDownloader()
    agents = list()

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='SourcesTable',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=SOURCE_METADATA_FILE_REGEX,
                minimum_last_file_update=datetime(year=2020, month=1, day=1, tzinfo=timezone.utc),
                archive_url=SOURCE_METADATA_TEMPLATE.format(filename=''),
                file_url_template=SOURCE_METADATA_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, SOURCE_METADATA_FILE_REGEX)
            ),
            parser_factory=SourceNameTableParserFactory(),
            django_models=[Source],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, SOURCE_METADATA_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=StationURLScrapeTarget(
                tag='StationsTable',
                db_handler=db,
                downloader=downloader,
                file_url=STATION_LIST_URL
            ),
            parser_factory=StationParserFactory(),
            django_models=[Station],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or STATION_LIST_URL in argument_urls else ...

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='MasterFileArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=MASTER_FILE_REGEX,
                minimum_last_file_update=PARSER_MASTER_FILES_MINIMUM_DATE,
                archive_url=MASTERFILE_URL_TEMPLATE.format(filename=''),
                file_url_template=MASTERFILE_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, MASTER_FILE_REGEX)
            ),
            parser_factory=MasterFileParserFactory(),
            django_models=[VLBISession],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, MASTER_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='EOPIFileArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=EOPI_FILE_REGEX,
                minimum_last_file_update=PARSE_EOP_MINIMUM_DATE,
                archive_url=INTENSIVE_SESSION_URL_TEMPLATE.format(filename=''),
                file_url_template=INTENSIVE_SESSION_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, EOPI_FILE_REGEX)
            ),
            parser_factory=EopParserFactory(),
            django_models=[AnalystCenter, EOPSolution],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, EOPI_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='EOXYFileArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=EOPS_FILE_REGEX,
                minimum_last_file_update=PARSE_EOP_MINIMUM_DATE,
                archive_url=NORMAL_SESSION_URL_TEMPLATE.format(filename=''),
                file_url_template=NORMAL_SESSION_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, EOPS_FILE_REGEX)
            ),
            parser_factory=EopParserFactory(),
            django_models=[AnalystCenter, EOPSolution],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, EOPS_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='CRFArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=CRF_FILE_REGEX,
                minimum_last_file_update=PARSE_CRF_MINIMUM_DATE,
                archive_url=CRF_FILES_URL_TEMPLATE.format(filename=''),
                file_url_template=CRF_FILES_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, CRF_FILE_REGEX)
            ),
            parser_factory=CRFParserFactory(),
            django_models=[CRFSolution],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, CRF_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=FileArchiveTarget(
                tag='TRFArchive',
                db_handler=db,
                downloader=downloader,
                file_url_pattern=TRF_FILE_REGEX,
                minimum_last_file_update=PARSE_TRF_MINIMUM_DATE,
                archive_url=TRF_FILES_URL_TEMPLATE.format(filename=''),
                file_url_template=TRF_FILES_URL_TEMPLATE,
                urls=filter_urls_with_regex(argument_urls, TRF_FILE_REGEX)
            ),
            parser_factory=TRFParserFactory(),
            django_models=[TRFSolution],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, TRF_FILE_REGEX) else ...

    agents.append(
        WebParserAgent(
            target_coll=SKDFilesTarget(
                tag='SKDFileCollection',
                db_handler=db,
                downloader=downloader,
                urls=filter_urls_with_regex(argument_urls, SKD_FILE_REGEX)
            ),
            parser_factory=SKDParserFactory(),
            django_models=[SourceStatistic, StationStatistic, BaselineStatistic],
            db_handler=db,
            downloader=downloader
        )
    ) if argument_urls is None or filter_urls_with_regex(argument_urls, SKD_FILE_REGEX) else ...

    summaries = []
    for agent in agents:
        summary = agent.run()
        summaries.append(summary)

    logger.info('----SUMMARIES----')
    for summary in summaries:
        logger.info(summary.to_dict())



def __reset():
    warning_sec = 5
    logger.warning(f'RESETTING DB IN {warning_sec} SECONDS...')
    time.sleep(warning_sec)
    logger.warning(f'RESETTING DB!')
    StationStatistic.objects.all().delete()
    SourceStatistic.objects.all().delete()
    BaselineStatistic.objects.all().delete()
    Station.objects.all().delete()
    Source.objects.all().delete()
    TRFSolution.objects.all().delete()
    EOPSolution.objects.all().delete()
    VLBISession.objects.all().delete()
    Station.objects.all().delete()
    Source.objects.all().delete()
    ScrapedFile.objects.all().delete()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--run_id', type=str, help='run_id for logging', default=None, required=False)
    parser.add_argument('--urls', type=str, nargs='*', help='urls to parse', default=None, required=False)
    args = parser.parse_args()
    try:
        setup_logging(args.run_id)
    except ValueError as e:
        logger.error(f'Could not setup logging: "{str(e)}". Exiting...')
        exit()
    run_scrape(args.urls)


if __name__ == '__main__':
    #__reset()
    main()
